/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package config

import (
	"path"

	"flag"

	"os"

	"io/ioutil"

	"log"

	"gitlab.com/silentteacup/congo"
	congoflag "gitlab.com/silentteacup/congo/sources/flag"
	"gitlab.com/silentteacup/congo/sources/ini"
)

// Configuration contains the configuration for the system.
// nolint
type Configuration struct {
	BasePath string `name:"basepath" usage:"Defines the folder where STP searches its resources. \nIf this is not explicitly given STP will try the directory it's executed in."`
	Port     int    `name:"port" usage:"Defines the port number STP will listen to."`
	Host     string `name:"host" usage:"Defines the host STP will use.\nDefault is the empty string which means STP will listen to all interfaces."`
	DataDir  string `name:"data-dir" usage:"Where transient data should be stored. This is were STP will store projects, testcases, users, ... and so on."`
	Debug    bool   `name:"debug" usage:"Enable this to get more detailed log output. Default is false."`
}

// nonWindowsDefaultPath is the path used to search for the configuration file on
// non windows machines.
var nonWindowsDefaultPath = path.Join("/", "etc", "stp", "config.ini")
var configuration = getDefaults()

// Load loads the configuration or returns an error if something went wrong.
func Load() error {
	iniSource := getIniSource()
	cfg := congo.New(
		"main",
		congoflag.New(),
		iniSource,
	)
	cfg.Using(&configuration)
	if err := cfg.Init(); err != nil {
		return err
	}
	if err := cfg.Load(); err != nil {
		return err
	}

	initBasePath(Get().BasePath)
	return nil
}

// getDefaults returns the Configuration struct containing all defaults.
func getDefaults() Configuration {
	return Configuration{
		getWorkingDir(),
		8080,
		"",
		defaultDataDir,
		false,
	}
}

// getIniSource gets the source that loads from the ini configuration file
func getIniSource() ini.Source {
	sourcePath := defaultPath
	// Use different flag set that continues on error so we can parse the
	// setting folder flag before parsing the settings.
	s := flag.NewFlagSet("pre-flags", flag.ContinueOnError)

	name := "config-path"
	usage := "Can be " +
		"used to set the source were STP loads it's configuration from.\n Default is os depended. " +
		"On windows it's the working directory (.\\config.ini). On all other platforms it's " +
		nonWindowsDefaultPath + "."

	// Hide output of the first parsing. The second one will do it anyway.
	s.SetOutput(ioutil.Discard)
	s.StringVar(&sourcePath, name, sourcePath, usage)
	// Still add the flag to our main flag set so it appears in the help.
	flag.StringVar(&sourcePath, name, sourcePath, usage)
	// Ignore error since it will be picked up by second flagset
	s.Parse(os.Args[1:]) // nolint: errcheck

	if _, err := os.Stat(sourcePath); os.IsNotExist(err) && sourcePath != defaultPath {
		log.Printf("The configuration file specified by the %s argument "+
			"doesn't exist. STP will use the defaults instead.", name)
	}

	return ini.FromFile(sourcePath)
}

// Get returns a copy of the configuration.
func Get() Configuration {
	return configuration
}
