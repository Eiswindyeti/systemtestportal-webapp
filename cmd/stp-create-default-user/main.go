/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
)

func main() {
	if err := config.Load(); err != nil {
		panic(err)
	}
	store.InitializeDatabase()

	pu := user.PasswordUser{
		User: user.User{
			Name:             "default",
			DisplayName:      "DeFault",
			EMail:            "default@example.org",
			RegistrationDate: time.Now(),
		},
		Password: "default",
	}

	if err := store.GetUserStore().Add(&pu); err != nil {
		panic(err)
	}
}
