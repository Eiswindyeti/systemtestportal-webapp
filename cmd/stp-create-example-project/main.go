/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
)

func main() {
	if err := config.Load(); err != nil {
		panic(err)
	}
	store.InitializeDatabase()

	exp := exampleProject(id.ActorID("default"))
	extc := exampleTestCases("default")
	exts := exampleTestSequences("default")

	if err := store.GetProjectStore().Add(exp); err != nil {
		panic(err)
	}

	for _, tcs := range extc {
		for _, tc := range tcs {
			if err := store.GetCaseStore().Add(tc); err != nil {
				panic(err)
			}
		}
	}

	for _, tss := range exts {
		for _, ts := range tss {
			if err := store.GetSequenceStore().Add(ts); err != nil {
				panic(err)
			}
		}
	}

}

func exampleProject(owner id.ActorID) *project.Project {
	return &project.Project{
		Name:        "DuckDuckGo.com",
		Owner:       owner,
		Description: "Test the online search engine DuckDuckGo.com",
		Visibility:  visibility.Public,
		Variants: map[string]*project.Variant{
			"Chrome": {
				Name:     "Chrome",
				Versions: chromeVersions,
			},
			"Firefox": {
				Name:     "Firefox",
				Versions: firefoxVersions,
			},
			"Microsoft Edge": {
				Name:     "Microsoft Edge",
				Versions: microsoftEdgeVersions,
			},
		},
		Labels: []project.Label{
			{
				Name:        "High Priority",
				Description: "This test has a high priority",
			},
			{
				Name:        "Low Priority",
				Description: "This test has a low priority",
			},
			{
				Name:        "Elepant",
				Description: "This test has an elepant priority",
			},
			{
				Name:        "Verschiedene",
				Description: "Huehuehuehue",
			},
			{
				Name:        "Obstsalat",
				Description: "Schlupp",
			},
		},
		CreationDate: time.Now().Round(time.Second),
	}

}

var browserVariants = map[string]*project.Variant{
	"Chrome": {
		Name:     "Chrome",
		Versions: chromeVersions,
	},
	"Firefox": {
		Name:     "Firefox",
		Versions: firefoxVersions,
	},
	"Microsoft Edge": {
		Name:     "Microsoft Edge",
		Versions: microsoftEdgeVersions,
	},
}

var plugins = map[string]*project.Variant{
	"Chrome": {
		Name: "Chrome",
		Versions: []project.Version{
			0: {Name: "v0.4.6"},
			1: {Name: "v42.5.25"},
		},
	},
	"Firefox": {
		Name: "Firefox",
		Versions: []project.Version{
			0: {Name: "v0.4.0"},
			1: {Name: "v0.4.6"},
		},
	},
}

var chromeVersions = []project.Version{
	0: {Name: "v0.2"},
	1: {Name: "v0.3.1"},
	2: {Name: "v0.4.6"},
	3: {Name: "v42.5.25"},
}

var firefoxVersions = []project.Version{
	0: {Name: "v0.1.11"},
	1: {Name: "v0.2.49"},
	2: {Name: "v0.4.0"},
	3: {Name: "v0.4.6"},
}

var microsoftEdgeVersions = []project.Version{
	0: {Name: "v0.175"},
	1: {Name: "v0.204"},
	2: {Name: "v1000"},
	3: {Name: "v1019"},
}

func exampleTestCases(ownerID id.ActorID) map[string][]*test.Case {
	projectName := "DuckDuckGo.com"
	projectID := id.NewProjectID(ownerID, projectName)

	return map[string][]*test.Case{
		projectName: {
			{
				Name:    "Change Theme",
				Project: projectID,
				Labels: []project.Label{
					{
						Name:        "High Priority",
						Description: "This test has a high priority",
					},
					{
						Name:        "Low Priority",
						Description: "This test has a low priority",
					},
					{
						Name:        "Elepant",
						Description: "This test has a low priority",
					},
				},
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Change the theme of the user interface",
						Preconditions: "DuckDuckGo.com is opened. The third theme is not selected in the settings.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      plugins,
						Steps:         test.ExampleSteps[0],
						CreationDate:  time.Now().AddDate(0, 0, -1),
						Case:          id.NewTestID(projectID, "Change Theme", true),
						Tester:        map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Search for Websites",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A default keyword-search",
						Preconditions: "DuckDuckGo.com is opened. Nothing is typed into the search bar.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps:         test.ExampleSteps[1],
						CreationDate:  time.Now().AddDate(0, 0, -1),
						Case:          id.NewTestID(projectID, "Search for Websites", true),
						Tester:        map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Search for Images",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A image search",
						Preconditions: "DuckDuckGo.com is opened. Nothing is typed into the search bar.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps:         test.ExampleSteps[2],
						CreationDate:  time.Now().AddDate(0, 0, -1),
						Case:          id.NewTestID(projectID, "Search for Images", true),
						Tester:        map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Search for Definitions",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A search for a definition",
						Preconditions: "DuckDuckGo.com is opened. Nothing is typed into the search bar.",
						Duration:      duration.NewDuration(0, 3, 0),
						Variants:      browserVariants,
						Steps:         test.ExampleSteps[3],
						CreationDate:  time.Now().AddDate(0, 0, -1),
						Case:          id.NewTestID(projectID, "Search for Definitions", true),
						Tester:        map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Change Language",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:   1,
						Message:     "Initial test case created",
						IsMinor:     false,
						Description: "Change the language settings",
						Preconditions: "DuckDuckGo.com is opened. " +
							"\"Español de España\" is not selected in the language settings of DuckDuckGo.com.",
						Duration:     duration.NewDuration(0, 7, 0),
						Variants:     plugins,
						Steps:        test.ExampleSteps[4],
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Change Language", true),
						Tester:       map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Install DuckDuckGo.com for Chrome",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Add DuckDuckGo.com as a search engine to your browser",
						Preconditions: "DuckDuckGo.com is opened.",
						Duration:      duration.NewDuration(0, 8, 0),
						Variants: map[string]*project.Variant{
							"Chrome": {
								Name:     "Chrome",
								Versions: chromeVersions,
							}},
						Steps:        test.ExampleSteps[5],
						CreationDate: time.Now().AddDate(0, 0, -2),
						Case:         id.NewTestID(projectID, "Install DuckDuckGo.com for Chrome", true),
						Tester:       map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Change Font Size",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:   1,
						Message:     "Initial test case created",
						IsMinor:     false,
						Description: "Change the font size of the search engine",
						Preconditions: "DuckDuckGo.com is opened." +
							" \"Large\" is selected as font size in the settings.",
						Duration:     duration.NewDuration(0, 7, 0),
						Variants:     plugins,
						Steps:        test.ExampleSteps[6],
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Change Font Size", true),
						Tester:       map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Map",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:   1,
						Message:     "Initial test case created",
						IsMinor:     false,
						Description: "Search a place and view it on a map",
						Preconditions: "DuckDuckGo.com is opened." +
							" Nothing is typed into the search bar.",
						Duration:     duration.NewDuration(0, 5, 0),
						Variants:     browserVariants,
						Steps:        test.ExampleSteps[7],
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Map", true),
						Tester:       map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Enable Autosuggestion",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Enable autosuggestion in your settings",
						Preconditions: "DuckDuckGo.com is opened. Autosuggestion is disabled.",
						Duration:      duration.NewDuration(0, 6, 0),
						Variants:      plugins,
						Steps:         test.ExampleSteps[8],
						CreationDate:  time.Now().AddDate(0, 0, -1),
						Case:          id.NewTestID(projectID, "Enable Autosuggestion", true),
						Tester:        map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Search for Recipes",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:   2,
						Message:     "Correcting a spelling mistake",
						IsMinor:     true,
						Description: "A search for recipes",
						Preconditions: "DuckDuckGo.com is opened. " +
							" Nothing is typed into the search bar.",
						Duration:     duration.NewDuration(0, 3, 0),
						Variants:     browserVariants,
						Steps:        test.ExampleSteps[9],
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Search for Recipes", true),
						Tester:       map[string]*user.User{},
					},
					1: {
						VersionNr:   1,
						Message:     "Initial test case created",
						IsMinor:     false,
						Description: "A search for receipts",
						Preconditions: "DuckDuckGo.com is opened." +
							" Nothing is typed into the search bar.",
						Duration:     duration.NewDuration(0, 3, 0),
						Variants:     browserVariants,
						Steps:        test.ExampleSteps[9],
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Search for Recipes", true),
						Tester:       map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Install DuckDuckGo.com for Firefox",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Add DuckDuckGo.com as a search engine to your browser",
						Preconditions: "DuckDuckGo.com is opened.",
						Duration:      duration.NewDuration(0, 3, 0),
						Variants: map[string]*project.Variant{
							"Firefox": {
								Name:     "Firefox",
								Versions: firefoxVersions,
							}},
						Steps:        test.ExampleSteps[10],
						CreationDate: time.Now().Add(-time.Hour * 2),
						Case:         id.NewTestID(projectID, "Install DuckDuckGo.com for Firefox", true),
						Tester:       map[string]*user.User{},
					},
				},
			},
			{
				Name:    "Install DuckDuckGo.com for Microsoft Edge",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Add DuckDuckGo.com as a search engine to your browser",
						Preconditions: "DuckDuckGo.com is opened.",
						Duration:      duration.NewDuration(0, 3, 0),
						Variants: map[string]*project.Variant{
							"Microsoft Edge": {
								Name:     "Microsoft Edge",
								Versions: microsoftEdgeVersions,
							}},
						Steps:        test.ExampleSteps[11],
						CreationDate: time.Now().Add(-time.Hour * 2),
						Case:         id.NewTestID(projectID, "Install DuckDuckGo.com for Microsoft Edge", true),
						Tester:       map[string]*user.User{},
					},
				},
			},
		},
	}
}
func exampleTestSequences(ownerID id.ActorID) map[string][]*test.Sequence {
	projectName := "DuckDuckGo.com"
	projectID := id.NewProjectID(ownerID, projectName)

	return map[string][]*test.Sequence{
		projectName: {
			{
				Name:    "Searching",
				Project: projectID,
				Labels:  []project.Label{},
				SequenceVersions: []test.SequenceVersion{
					0: {
						VersionNr:     2,
						Message:       "Add test case \"Map\"",
						IsMinor:       false,
						Description:   "This sequence tests the feature searching.",
						Preconditions: "DuckDuckGo.com is opened.  Nothing is typed into the search bar.",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases: []test.Case{*exampleTestCases(ownerID)["DuckDuckGo.com"][1],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][2],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][3],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][7],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][9]},
						Testsequence: id.NewTestID(projectID, "Searching", false),
						SequenceInfo: struct {
							Variants      map[string]*project.Variant
							DurationHours int
							DurationMin   int
						}{Variants: plugins, DurationHours: 0, DurationMin: 21},
					},
					1: {
						VersionNr:     1,
						Message:       "Initial test sequence created",
						IsMinor:       false,
						Description:   "This sequence tests the feature searching.",
						Preconditions: "DuckDuckGo.com is opened.  Nothing is typed into the search bar.",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases: []test.Case{*exampleTestCases(ownerID)["DuckDuckGo.com"][1],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][2],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][3],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][9]},
						Testsequence: id.NewTestID(projectID, "Searching", false),
						SequenceInfo: struct {
							Variants      map[string]*project.Variant
							DurationHours int
							DurationMin   int
						}{Variants: browserVariants, DurationHours: 0, DurationMin: 16},
					},
				},
			},
			{
				Name:    "Settings",
				Project: projectID,
				SequenceVersions: []test.SequenceVersion{
					0: {
						VersionNr:     2,
						Message:       "Add test cases",
						IsMinor:       false,
						Description:   "This sequence tests the settings.",
						Preconditions: "DuckDuckGo.com is opened.",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases: []test.Case{*exampleTestCases(ownerID)["DuckDuckGo.com"][0],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][4],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][6],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][8]},
						Testsequence: id.NewTestID(projectID, "Settings", false),
						SequenceInfo: struct {
							Variants      map[string]*project.Variant
							DurationHours int
							DurationMin   int
						}{Variants: plugins, DurationHours: 0, DurationMin: 25},
					},
					1: {
						VersionNr:     1,
						Message:       "Initial test sequence created",
						IsMinor:       false,
						Description:   "This sequence tests the settings.",
						Preconditions: "DuckDuckGo.com is opened.",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases:         []test.Case{},
						Testsequence:  id.NewTestID(projectID, "Settings", false),
						SequenceInfo: struct {
							Variants      map[string]*project.Variant
							DurationHours int
							DurationMin   int
						}{Variants: browserVariants, DurationHours: 0, DurationMin: 0},
					},
				},
			},
			{
				Name:    "Install DuckDuckGo.com",
				Project: projectID,
				SequenceVersions: []test.SequenceVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test sequence created",
						IsMinor:       false,
						Description:   "This sequence tests the installation of DuckDuckGo.com on different browsers.",
						Preconditions: "DuckDuckGo.com is opened.",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases: []test.Case{*exampleTestCases(ownerID)["DuckDuckGo.com"][5],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][10],
							*exampleTestCases(ownerID)["DuckDuckGo.com"][11]},
						Testsequence: id.NewTestID(projectID, "Install DuckDuckGo.com", false),
						SequenceInfo: struct {
							Variants      map[string]*project.Variant
							DurationHours int
							DurationMin   int
						}{Variants: nil, DurationHours: 0, DurationMin: 14},
					},
				},
			},
		},
	}
}
