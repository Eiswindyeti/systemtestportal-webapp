/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
)

func main() {
	if err := config.Load(); err != nil {
		panic(err)
	}
	store.InitializeDatabase()

	exp := exampleProject(id.ActorID("default"))
	extc := exampleTestCases("default")
	exts := exampleTestSequences("default")

	if err := store.GetProjectStore().Add(exp); err != nil {
		panic(err)
	}

	for _, tcs := range extc {
		for _, tc := range tcs {
			if err := store.GetCaseStore().Add(tc); err != nil {
				panic(err)
			}
		}
	}

	for _, tss := range exts {
		for _, ts := range tss {
			if err := store.GetSequenceStore().Add(ts); err != nil {
				panic(err)
			}
		}
	}

}

func exampleProject(owner id.ActorID) *project.Project {
	return &project.Project{
		Name:        "SystemTestPortal",
		Owner:       owner,
		Description: "Tests the web application \"SystemTestPortal\".",
		Visibility:  visibility.Public,
		Variants: map[string]*project.Variant{
			"Chrome": {
				Name:     "Chrome",
				Versions: chromeVersions,
			},
			"Firefox": {
				Name:     "Firefox",
				Versions: firefoxVersions,
			},
			"Microsoft Edge": {
				Name:     "Microsoft Edge",
				Versions: microsoftEdgeVersions,
			},
		},
		Labels: []project.Label{
			{
				Name:        "High Priority",
				Description: "This test has a high priority",
			},
			{
				Name:        "Low Priority",
				Description: "This test has a low priority",
			},
		},
		CreationDate: time.Now().Round(time.Second),
	}

}

var browserVariants = map[string]*project.Variant{
	"Chrome": {
		Name:     "Chrome",
		Versions: chromeVersions,
	},
	"Firefox": {
		Name:     "Firefox",
		Versions: firefoxVersions,
	},
	"Microsoft Edge": {
		Name:     "Microsoft Edge",
		Versions: microsoftEdgeVersions,
	},
}

var chromeVersions = []project.Version{
	0: {Name: "v0.12.0"},
}

var firefoxVersions = []project.Version{
	0: {Name: "v0.12.0"},
}

var microsoftEdgeVersions = []project.Version{
	0: {Name: "v0.12.0"},
}

func exampleTestCases(ownerID id.ActorID) map[string][]*test.Case {
	projectName := "SystemTestPortal"
	projectID := id.NewProjectID(ownerID, projectName)

	return map[string][]*test.Case{
		projectName: {
			{
				Name:    "Sign in",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A user signs in.",
						Preconditions: "The user is not signed in. The user has been registrated.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click the \"Sign In\" button.",
								"The sign in modal opens.", 1},
							{"Enter an e-mail address and password and click \"Sign In\".",
								"The \"Sign In\" button from earlier disappeared. " +
									"The user is now able to work on public projects (e.g. edit test cases).", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Sign in", true),
					},
				},
			},
			{
				Name:    "Sign out",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A user signs out.",
						Preconditions: "The user is already signed in.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click on your display name in the top right corner.",
								"A context menue opens.", 1},
							{"Click \"Sign out\" in the context menu.",
								"Your display name is replaced by a \"Sign in\" button." +
									" The user isn't able to work on any projects (e.g. edit test cases).", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Sign out", true),
					},
				},
			},
			{
				Name:    "Registration",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A user gets registrated.",
						Preconditions: "The user name doesn't exist already.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click on \"Add new user\".",
								"The site \"Create new User\" opens.", 1},
							{"Enter user name, display name, e-mail address, password and repeat" +
								" the password. Click Register",
								"The user is now able to sign in with the inserted information above.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Registration", true),
					},
				},
			},
			{
				Name:    "Create a Project",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A new project is created.",
						Preconditions: "A user is signed in.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click on \"+ New Project\".",
								"A new page opens.", 1},
							{"Enter project name and project description." +
								" Click \"Create Project\".",
								"A list of all visible projects is viewed." +
									" The project created above is in this list.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Create a Project", true),
					},
				},
			},
			{
				Name:    "Comment on a test case",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Create a new comment.",
						Preconditions: "A user is signed in. A detailed view of a test case is viewed.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Enter a new comment. Click \"Comment\".",
								"The comment is displayed below. ", 1},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Comment on a test case", true),
					},
				},
			},
			{
				Name:    "Comment on a test sequence",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Create a new comment.",
						Preconditions: "A user is signed in. A detailed view of a test sequence is viewed.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Enter a new comment. Click \"Comment\".",
								"The comment is displayed below.", 1},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Comment on a test sequence", true),
					},
				},
			},
			{
				Name:    "Create test case",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A new test case is created.",
						Preconditions: "A user is signed in. The name of the test case doesn't exist already.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"+ New Test Case\".",
								"The page \"Create Test Case\" opens.", 1},
							{"Enter test case name, description and conditions, " +
								"add test steps and select Variants, Versions, estimated time and labels. Click \"Save\".",
								"A list of all test cases of the project is viewed." +
									"The test case created above is in this list.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Create test case", true),
					},
				},
			},
			{
				Name:    "Add test steps",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "A new test step is added to an existing test case.",
						Preconditions: "A user is signed in.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"Add Test Step\".",
								"The test step modal opens.", 1},
							{"Enter a task and description. Click \"OK\".",
								"The modal closes. The new test step is viewed in the list of test steps.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Add test steps", true),
					},
				},
			},
			{
				Name:    "Edit a test case",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "An existing test case is edited.",
						Preconditions: "A user is signed in. The test case that will be edited already exists.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"Edit\".",
								"The test step modal opens.", 1},
							{"Make some changes to the test case and click \"Save\".",
								"A modal opens.", 2},
							{"Fill out the modal and click \"Save\".",
								"The test case is viewed with the changes.", 3},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Edit a test case", true),
					},
				},
			},
			{
				Name:    "Execute a test case",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "An existing test case is executed.",
						Preconditions: "The test case that will be executed already exists.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"Start\".",
								"The step-for-step execution starts.", 1},
							{"Follow this execution and click \"Finish\".",
								"For each test step and for the overall result \"not assesed\"" +
									", \"failed\", \"passed with comment\" and \"passed\" can be selected. " +
									"A protocol of the execution is created.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Execute a test case", true),
					},
				},
			},
			{
				Name:    "Create test sequence",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:   1,
						Message:     "Initial test case created",
						IsMinor:     false,
						Description: "Group multiple test cases in one test sequence.",
						Preconditions: "The user is signed in. " +
							"The test cases that will be grouped into the test sequence already exist.",
						Duration: duration.NewDuration(0, 5, 0),
						Variants: browserVariants,
						Steps: []test.Step{
							{"Click \"+ New Test Sequence\".",
								"The page \"Create Test Sequence\" opens.", 1},
							{"Enter test sequence name, description and conditions, " +
								"add test cases and select labels. Click \"Save\".",
								"A list of all test sequences of the project is viewed." +
									"The test sequence created above is in this list.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Create test sequence", true),
					},
				},
			},
			{
				Name:    "Assign a test case",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Assign a test case.",
						Preconditions: "The test case that will be assigned exist already.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"Assign\".",
								"The assign modal opens.", 1},
							{"Select the user you want to assign the test case to. Click \"OK\".",
								"The selected users can see this assignment in their TODO list.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Assign a test case", true),
					},
				},
			},
			{
				Name:    "Assign a test sequence",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Assign a test sequence.",
						Preconditions: "The test sequence that will be assigned exist already.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"Assign\".",
								"The assign modal opens.", 1},
							{"Select the user you want to assign the test sequence to. Click \"OK\".",
								"The selected users can see this assignment in their TODO list.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Assign a test sequence", true),
					},
				},
			},
			{
				Name:    "View protocols",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "View the details of a protocol.",
						Preconditions: "The protocol that will be viewed already exist.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"If the protocol is from a test case execution select \"Test Cases\"" +
								" otherwise \"Test Sequences\". Select the name of the executed test case or " +
								"test sequence in the drop down.",
								"A list of all protocols of the selected test case or test sequence is viewed.", 1},
							{"Select the protocol you want to see.",
								"The results, comments and notes of the execution are viewed.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "view protocols", true),
					},
				},
			},
			{
				Name:    "time execution",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "The time of an test execution is stopped.",
						Preconditions: "The test that will be executed already exists.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"Start\".",
								"The step-by-step execution and the timer start.", 1},
							{"Pause the timer.",
								"The timer stops counting.", 2},
							{"Continue the timer.",
								"The timer continues counting.", 3},
							{"Continue the execution and finish it.",
								"The timer stopps. The execution time is saved with the test protocol.", 4},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "time execution", true),
					},
				},
			},
			{
				Name:    "Versioning",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Test cases are versioned.",
						Preconditions: "The test that is viewed has been changed at least one time.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click \"History\".",
								"The history of the test is viewed.", 1},
							{"Click on an older version.",
								"The older version of the test ist viewed.", 2},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Versioning", true),
					},
				},
			},
			{
				Name:    "Print",
				Project: projectID,
				TestCaseVersions: []test.CaseVersion{
					0: {
						VersionNr:     1,
						Message:       "Initial test case created",
						IsMinor:       false,
						Description:   "Print a test sequence, test case, protocol or a list of them.",
						Preconditions: "The test sequence, test case or protocol to be printed exist.",
						Duration:      duration.NewDuration(0, 5, 0),
						Variants:      browserVariants,
						Steps: []test.Step{
							{"Click on the print button.",
								"A print preview opens in a separate window.", 1},
							{"Click \"Print\".",
								"The settings of the pushing operation appear.", 2},
							{"Adjust the settings of the pushing operation and print the document.",
								"The selected document is printed.", 3},
						},
						CreationDate: time.Now().AddDate(0, 0, -1),
						Case:         id.NewTestID(projectID, "Print", true),
					},
				},
			},
		},
	}
}
func exampleTestSequences(ownerID id.ActorID) map[string][]*test.Sequence {
	projectName := "SystemTestPortal"
	projectID := id.NewProjectID(ownerID, projectName)

	return map[string][]*test.Sequence{
		projectName: {
			{
				Name:    "Assign",
				Project: projectID,
				Labels:  []project.Label{},
				SequenceVersions: []test.SequenceVersion{
					0: {
						VersionNr:     2,
						Message:       "Initial test sequence created",
						IsMinor:       false,
						Description:   "This sequence tests the assignment of test cases and test sequences.",
						Preconditions: "The test case and test sequence that will be assigned already exist.",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases: []test.Case{*exampleTestCases(ownerID)["SystemTestPortal"][1],
							*exampleTestCases(ownerID)["SystemTestPortal"][13],
							*exampleTestCases(ownerID)["SystemTestPortal"][14]},
						Testsequence: id.NewTestID(projectID, "Assign", false),
						SequenceInfo: struct {
							Variants      map[string]*project.Variant
							DurationHours int
							DurationMin   int
						}{Variants: browserVariants, DurationHours: 0, DurationMin: 21},
					},
				},
			},
			{
				Name:    "Comment",
				Project: projectID,
				Labels:  []project.Label{},
				SequenceVersions: []test.SequenceVersion{
					0: {
						VersionNr:     2,
						Message:       "Initial test sequence created",
						IsMinor:       false,
						Description:   "This sequence tests the ability to comment on test cases and test sequences.",
						Preconditions: "A user is signed in.",
						CreationDate:  time.Now().AddDate(-4, -2, -1),
						Cases: []test.Case{*exampleTestCases(ownerID)["SystemTestPortal"][1],
							*exampleTestCases(ownerID)["SystemTestPortal"][4],
							*exampleTestCases(ownerID)["SystemTestPortal"][5]},
						Testsequence: id.NewTestID(projectID, "Comment", false),
						SequenceInfo: struct {
							Variants      map[string]*project.Variant
							DurationHours int
							DurationMin   int
						}{Variants: browserVariants, DurationHours: 0, DurationMin: 21},
					},
				},
			},
		},
	}
}
