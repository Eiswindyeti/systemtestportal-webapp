[![Build-Badge][build]][commits-master]
[![Coverage-Badge][coverage]][coverage-report]
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stp-team/systemtestportal-webapp)](https://goreportcard.com/report/gitlab.com/stp-team/systemtestportal-webapp)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[coverage]: https://gitlab.com/stp-team/systemtestportal-webapp/badges/master/coverage.svg?job=test-project "Master test coverage"
[build]: https://gitlab.com/stp-team/systemtestportal-webapp/badges/master/build.svg "Master pipeline status"
[coverage-report]: https://gitlab.com/stp-team/systemtestportal-webapp/builds/artifacts/master/browse?job=test-project
[commits-master]: https://gitlab.com/stp-team/systemtestportal-webapp/commits/master

# SystemTestPortal - http://www.systemtestportal.org

SystemTestPortal is a web application that allows to create, run and analyze
manual system tests. It aims to be useful for developers, testers and end-users.

## Features

- Step-by-step execution of tests
- Organizing test cases with the help of test sequences
- Inspect protocols of test executions
- Print tests to execute them offline

Discover more on our [Homepage](http://www.systemtestportal.org/discover/).

## Quick start

Once you have [downloaded](ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/) the 
binary or compiled it yourself. The server can be started using:
```
stp --host=<the hostname (e.g. localhost or 0.0.0.0)> --port=<the port to listen to>
```
Since version 0.11.0 the server will listen to all interfaces on port 8080 by default 
(if no host or port is provided).
Earlier versions listen exclusively to localhost on port 8080.
If you are using an earlier version, but want to listen to all interfaces
you can use "" or 0.0.0.0 as hostname.

### Docker image

We also offer a docker image via our [registry](https://gitlab.com/stp-team/systemtestportal-webapp/container_registry)
You can simple pull the latest image using:

```bash
docker pull registry.gitlab.com/stp-team/systemtestportal-webapp:latest
```

or

```bash
docker pull registry.gitlab.com/stp-team/systemtestportal-webapp:stable
```

for the latest stable image (These should be available for versions >= 1.0.0).
A container created from the image will immediately start the system, which will listen on the exposed port 8080
of the container.

> **Note:** Since our build machine has the architecture `armhf` you will most likely not be able to explore the image
  using
  ```bash
  docker run -it --rm registry.gitlab.com/stp-team/systemtestportal-webapp:latest /bin/ash
  ```
  unless you are working on a system with the same architecture.  
  If you want to avoid this issue you can build the docker image yourself so it's fitted to your architecture.
  The required files are the [binary-package for linux-amd64][linux-amd64] and
  obviously the [Dockerfile](./Dockerfile). Optionally you can provide a [config.ini](./config.ini) which will be copied
  to the image as well. Place all these files in the same folder and then just run:
  ```bash
  docker build -t <your name for the image> .
  ```
  inside it. Viola the build docker image is suited to your system.

[linux-amd64]: https://gitlab.com/stp-team/systemtestportal-webapp/-/jobs/artifacts/master/download?job=linux-amd64

# Configuration

STP can be configured using a ini configuration file. The default path for the configuration file on
linux is `/etc/stp/config.ini`. On windows STP looks for the config.ini inside the working directory
(the directory STP is started in).
You can define a different path to the configuration file via the commandline flag
`--config-path=<your custom path to the configuration>`.

A configuration file with all defaults can be found [here](./config.ini).

# Flags

The available flags you can pass to STP are as follows:

 - `--basepath=<the base path>`: This is directory STP searches for its resource folders
 (templates, static, migrations(currently only necessary if STP was build with the sqlite flag)).
 - `--port=<port number>`: The port that STP should listen on.
 - `--host=<host>`: The host that STP should interface with.
 - `--debug`: If present the log output will be a bit more verbose.
 - `--config-path=<path/to/the/configuration>`: Sets the path where STP searches for its
 configuration file.
 - `--data-dir=<path/where/stp/stores/date>`: The directory that STP should stores its data
 in. E.g. when build with sqlite STP will store the database file there.

## Installation guide

The installation guide can be found on [our website](http://www.systemtestportal.org/get/).

## You want to contribute?

Check out our [workflow](https://gitlab.com/stp-team/systemtestportal-webapp/blob/master/CONTRIBUTING.md).

## About the developers

The SystemTestPortal is developed by students of the [Institute of Software Technology at the Universität Stuttgart](http://www.iste.uni-stuttgart.de/se) in a student project.

## License

[GNU GPL-3.0](https://opensource.org/licenses/GPL-3.0)