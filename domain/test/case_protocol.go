/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"reflect"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

// CaseProtocolGetter is used to get a specific protocols for a given protocolID.
type CaseProtocolGetter interface {
	// GetCaseExecutionProtocol gets the protocol with given id
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (CaseExecutionProtocol, error)
}

//CaseExecutionProtocol contains all information relating to an execution of a test case
type CaseExecutionProtocol struct {
	TestVersion id.TestVersionID
	ProtocolNr  int

	SUTVersion    string
	SUTVariant    string
	ExecutionDate time.Time

	StepProtocols   []StepExecutionProtocol
	Result          Result
	Comment         string
	OtherNeededTime duration.Duration
}

// NewCaseExecutionProtocol creates a new execution protocol of a test case execution with the given information
func NewCaseExecutionProtocol(caseProtocolLister ProtocolLister, caseVersion id.TestVersionID, SUTVariant string,
	SUTVersion string, neededTime duration.Duration, stepCount int) (CaseExecutionProtocol, error) {

	newID, err := getProtocolNr(caseProtocolLister, caseVersion)
	if err != nil {
		return CaseExecutionProtocol{}, err
	}

	protocol := CaseExecutionProtocol{
		ProtocolNr:  newID,
		TestVersion: caseVersion,

		SUTVersion: SUTVersion,
		SUTVariant: SUTVariant,

		ExecutionDate:   time.Now().Round(time.Second),
		StepProtocols:   make([]StepExecutionProtocol, stepCount),
		Result:          NotAssessed,
		Comment:         "",
		OtherNeededTime: neededTime,
	}
	return protocol, nil
}

/*Equals compares two CaseExecutionProtocols and returns true, if these two are equal.
If in is nil, Equals will return false.
*/
func (prt CaseExecutionProtocol) Equals(in interface{}) bool {
	return reflect.DeepEqual(prt, in)
}

//DeepCopy returns a copy, that is completely independent of its original.
func (prt CaseExecutionProtocol) DeepCopy() CaseExecutionProtocol {
	result := CaseExecutionProtocol{
		ProtocolNr:  prt.ProtocolNr,
		TestVersion: prt.TestVersion,

		SUTVersion: prt.SUTVersion,
		SUTVariant: prt.SUTVariant,

		ExecutionDate:   prt.ExecutionDate,
		StepProtocols:   make([]StepExecutionProtocol, len(prt.StepProtocols)),
		Result:          prt.Result,
		Comment:         prt.Comment,
		OtherNeededTime: prt.OtherNeededTime,
	}
	copy(result.StepProtocols, prt.StepProtocols)
	return result
}

// IsFinished checks, if the TestCase was executed till end or was interrupted and isn't completed yet.
func (prt *CaseExecutionProtocol) IsFinished() bool {
	result := true
	for _, step := range prt.StepProtocols {
		result = result && step.Visited
	}
	return result
}

// SaveStep will be called, if the user executed one test step.
// The results of the execution will be saved to the protocol.
// stepNr starts with step 1.
func (prt *CaseExecutionProtocol) SaveStep(stepNr int, observedBehavior string, result Result, comment string,
	neededTime duration.Duration) {
	stepPrt := StepExecutionProtocol{
		NeededTime:       neededTime,
		ObservedBehavior: observedBehavior,
		Result:           result,
		Comment:          comment,
		Visited:          true,
	}

	prt.StepProtocols[stepNr-1] = stepPrt
}

// Finish will finish the Protocol and saves over all Data.
func (prt *CaseExecutionProtocol) Finish(result Result, comment string, neededTime duration.Duration) {
	prt.Result = result
	prt.Comment = comment
	prt.OtherNeededTime = prt.OtherNeededTime.Add(neededTime)
}

// GetNeededTime returns the summed up time, needed for this execution so far.
func (prt *CaseExecutionProtocol) GetNeededTime() duration.Duration {
	var result = prt.OtherNeededTime
	for _, v := range prt.StepProtocols {
		result = result.Add(v.NeededTime)
	}
	return result
}
