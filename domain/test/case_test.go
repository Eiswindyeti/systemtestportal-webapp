/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"reflect"
	"testing"

	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// TestNewTestCase contains test cases for the NewTestCase function
func TestNewTestCase(t *testing.T) {
	type tcArgs struct {
		name          string
		description   string
		preconditions string
	}
	tcs := []struct {
		in   tcArgs
		want Case
	}{
		{
			in: tcArgs{},
			want: Case{
				Project:          defaultProject(),
				Name:             "",
				TestCaseVersions: defaultTCVersion(id.NewTestID(defaultProject(), "", true)),
				Labels:           []project.Label{},
			},
		},
		{
			in: tcArgs{name: "  "},
			want: Case{
				Project:          defaultProject(),
				Name:             "",
				TestCaseVersions: defaultTCVersion(id.NewTestID(defaultProject(), "", true)),
				Labels:           []project.Label{},
			},
		},
		{
			in: tcArgs{name: "Test"},
			want: Case{
				Project:          defaultProject(),
				Name:             "Test",
				TestCaseVersions: defaultTCVersion(id.NewTestID(defaultProject(), "Test", true)),
				Labels:           []project.Label{},
			},
		},
		{
			in: tcArgs{name: " Test 1"},
			want: Case{
				Project:          defaultProject(),
				Name:             "Test 1",
				TestCaseVersions: defaultTCVersion(id.NewTestID(defaultProject(), "Test 1", true)),
				Labels:           []project.Label{},
			},
		},
	}

	for _, tc := range tcs {
		got := NewTestCase(tc.in.name, tc.in.description, tc.in.preconditions, []project.Label{},
			map[string]*project.Variant{}, duration.NewDuration(0, 0, 0), defaultProject())

		if !equalCases(got, tc.want) {
			t.Errorf("NewTestCase(%s, %s, %s) = \n%+v \nwant \n%+v", tc.in.name, tc.in.description,
				tc.in.preconditions, got, tc.want)
		}
	}
}

func defaultTCVersion(parentCase id.TestID) []CaseVersion {
	return []CaseVersion{
		{
			VersionNr:    1,
			Message:      "Initial test case created",
			IsMinor:      false,
			Variants:     map[string]*project.Variant{},
			CreationDate: time.Now().Round(time.Second),
			Case:         parentCase,
			Tester:       map[string]*user.User{},
		},
	}
}

func defaultProject() id.ProjectID {
	return id.NewProjectID(id.NewActorID("owner"), "project")
}

func equalCases(c1, c2 Case) bool {
	if c1.Project != c2.Project {
		return false
	}

	if c1.Name != c2.Name {
		return false
	}

	if !reflect.DeepEqual(c1.Labels, c2.Labels) {
		return false
	}

	if len(c1.TestCaseVersions) != len(c2.TestCaseVersions) {
		return false
	}

	for i := range c1.TestCaseVersions {
		if !equalCaseVersions(c1.TestCaseVersions[i], c2.TestCaseVersions[i]) {
			return false
		}
	}

	return true
}

func equalCaseVersions(cv1, cv2 CaseVersion) bool {
	if cv1.Case != cv2.Case {
		return false
	}

	if cv1.VersionNr != cv2.VersionNr {
		return false
	}

	if cv1.Description != cv2.Description {
		return false
	}

	if cv1.Preconditions != cv2.Preconditions {
		return false
	}

	if cv1.Duration != cv2.Duration {
		return false
	}

	if !reflect.DeepEqual(cv1.Variants, cv2.Variants) {
		return false
	}

	if cv1.CreationDate.Sub(cv2.CreationDate) > time.Second {
		return false
	}

	if !reflect.DeepEqual(cv1.Steps, cv2.Steps) {
		return false
	}

	return true
}
