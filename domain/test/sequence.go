/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// The Sequence struct contains information for describing a test sequence
type Sequence struct {
	Name             string
	Project          id.ProjectID
	Labels           []project.Label
	SequenceVersions []SequenceVersion
}

// SequenceVersion contains information for describing a version of a test sequence
type SequenceVersion struct {
	VersionNr     int
	Message       string
	IsMinor       bool
	Description   string
	Preconditions string
	CreationDate  time.Time
	Cases         []Case
	SequenceInfo  SequenceInfo
	Tester        map[string]*user.User
	Testsequence  id.TestID
}

// SequenceInfo contains meta information for a SequenceVersion
type SequenceInfo struct {
	Variants      map[string]*project.Variant
	DurationHours int
	DurationMin   int
}

// ItemEstimatedDuration returns the estimated Duration for the sequence
func (tsv SequenceVersion) ItemEstimatedDuration() (minutes int, hours int) {
	minutes = 0
	hours = 0

	for _, c := range tsv.Cases {
		minutes += c.TestCaseVersions[0].Duration.GetMinuteInHour()
		hours += int(c.TestCaseVersions[0].Duration.Hours())
	}
	return
}

// Rename renames this sequence also changing it's id.
func (ts *Sequence) Rename(n string) {
	ts.Name = n
}

// ItemName returns a test sequences name
func (ts Sequence) ItemName() string {
	return ts.Name
}

// CountIncludedSteps returns the total number of steps summed over all included cases
func (tsv SequenceVersion) CountIncludedSteps() int {
	result := 0
	for _, tc := range tsv.Cases {
		result += len(tc.TestCaseVersions[0].Steps)
	}
	return result
}

// CountIncludedStepsUpTo returns the total number of steps summed over all included cases up to maxCaseNr
// the numeration starts with 0 and maxCaseNr itself will be included
func (tsv SequenceVersion) CountIncludedStepsUpTo(maxCaseNr int) int {
	result := 0
	for i := 0; i <= maxCaseNr && i < len(tsv.Cases); i++ {
		tcv := tsv.Cases[i].TestCaseVersions[0]
		result += len(tcv.Steps)
	}
	return result
}

// ID returns a test sequences id
func (ts Sequence) ID() id.TestID {
	return id.NewTestID(ts.Project, ts.Name, false)
}

// ID returns a test sequences id
func (tsv SequenceVersion) ID() id.TestVersionID {
	return id.NewTestVersionID(tsv.Testsequence, tsv.VersionNr)
}

// NewTestSequence creates a new test sequence with the given information
func NewTestSequence(name, description, preconditions string, labels []project.Label,
	cases []Case, project id.ProjectID) (Sequence, error) {

	name = strings.TrimSpace(name)

	ts := Sequence{
		Name:    name,
		Labels:  labels,
		Project: project,
	}

	initVer := 1
	initMessage := "Initial test sequence created"

	tsv, err := NewTestSequenceVersion(initVer, false, initMessage, description, preconditions, cases, ts.ID())

	ts.SequenceVersions = append(ts.SequenceVersions, tsv)

	return ts, err
}

// NewTestSequenceVersion creates a new version for a test case
// Returns the test case the version belongs to
func NewTestSequenceVersion(version int, isMinor bool, message, description string, preconditions string,
	cases []Case, testsequence id.TestID) (SequenceVersion, error) {

	description = strings.TrimSpace(description)
	preconditions = strings.TrimSpace(preconditions)
	message = strings.TrimSpace(message)

	sutVariants, err := CalculateSUTVersion(cases)
	if err != nil {
		return SequenceVersion{}, err
	}

	dur := CalculateDuration(cases)

	si := SequenceInfo{
		Variants:      sutVariants,
		DurationHours: int(dur.Hours()),
		DurationMin:   dur.GetMinuteInHour(),
	}

	tsv := SequenceVersion{
		VersionNr:     version,
		Message:       message,
		IsMinor:       isMinor,
		Description:   description,
		Preconditions: preconditions,
		CreationDate:  time.Now().Round(time.Second),
		Cases:         cases,
		SequenceInfo:  si,
		Testsequence:  testsequence,
		Tester:        map[string]*user.User{},
	}

	return tsv, nil
}

//CalculateSUTVersion calculates the applicable sut versions
func CalculateSUTVersion(cases []Case) (map[string]*project.Variant, error) {
	var applicableVariants = map[string]*project.Variant{}

	// Check if cases is not empty
	// Check if the newest version of the first test case really contains variants
	if len(cases) > 0 && len(cases[0].TestCaseVersions[0].Variants) > 0 {

		// Add all variants of the first test case to the list with applicable versions
		for _, variant := range cases[0].TestCaseVersions[0].Variants {
			applicableVariants[variant.Name] = variant
		}

		// Iterate over all test cases to check all variants of them
		for _, tc := range cases {
			// If a test case contains no variants, remove all variants from the applicableVariants list
			if len(tc.TestCaseVersions[0].Variants) <= 0 {
				applicableVariants = map[string]*project.Variant{}
			} else {
				checkVariantForApplicableVersions(applicableVariants, tc)
			}
		}
	}

	return applicableVariants, nil
}

// checkVariantForApplicableVersions calculates the cutset of the test cases (tc) variants
// and test applicableVariants
func checkVariantForApplicableVersions(applicableVariants map[string]*project.Variant, tc Case) {
	for varIndex, variant := range applicableVariants {
		if _, ok := tc.TestCaseVersions[0].Variants[varIndex]; !ok {
			delete(applicableVariants, varIndex)
			continue
		}

		// Check all versions of a variant
		for versionIndex, version := range variant.Versions {
			// If the variant does not contain the version remove the version from the
			// variant in the applicableVariant list
			if !containsVersion(tc.TestCaseVersions[0].Variants[varIndex].Versions, version) {
				applicableVariants[varIndex].Versions = append(
					applicableVariants[varIndex].Versions[:versionIndex],
					applicableVariants[varIndex].Versions[versionIndex+1:]...)
			}
		}
	}
}

// containsVersion checks if the versionList contains the given version
func containsVersion(versionList []project.Version, version project.Version) bool {
	for _, ver := range versionList {
		if ver.Name == version.Name {
			return true
		}
	}
	return false
}

// CalculateDuration calculates the durationHours and durationMin for testsequences
func CalculateDuration(cases []Case) duration.Duration {
	var result = duration.Duration{0}
	var tcv = CaseVersion{}
	for _, c := range cases {
		tcv = c.TestCaseVersions[0]
		result = result.Add(tcv.Duration)
	}
	return result
}

//SetTestersOfTestSequence sets the testers of the test case
func (ts *Sequence) SetTestersOfTestSequence(tester []*user.User) {
	ts.SequenceVersions[0].Tester = map[string]*user.User{}
	for _, t := range tester {
		ts.SequenceVersions[0].Tester[t.Name] = t
	}
}
