/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

var defaultCaseProtocol = CaseExecutionProtocol{
	TestVersion: id.NewTestVersionID(id.NewTestID(id.NewProjectID(id.NewActorID("owner"), "d8ed5f42"),
		"123456G", true), 2),
	SUTVersion: "Version 1.02",
	SUTVariant: "Win 10",

	ExecutionDate:   time.Date(2000, 12, 24, 12, 0, 0, 0, time.UTC),
	StepProtocols:   []StepExecutionProtocol{stepPrt1, stepPrt2, stepPrt3, stepPrt4, stepPrt5, stepPrt6},
	Result:          NotAssessed,
	Comment:         "All OK",
	OtherNeededTime: duration.NewDuration(0, 0, 55),
}

func TestCaseExecutionProtocol_GetNeededTime(t *testing.T) {
	a := defaultCaseProtocol.DeepCopy()
	a.StepProtocols[0].NeededTime = duration.NewDuration(0, 4, 0)
	a.StepProtocols[1].NeededTime = duration.NewDuration(0, 10, 0)
	a.StepProtocols[2].NeededTime = duration.NewDuration(0, 22, 0)
	a.StepProtocols[3].NeededTime = duration.NewDuration(0, 0, 0)
	a.StepProtocols[4].NeededTime = duration.NewDuration(0, 0, 0)
	a.StepProtocols[5].NeededTime = duration.NewDuration(0, 1, 0)
	a.OtherNeededTime = duration.NewDuration(0, 1, 0)

	expectedResult := duration.NewDuration(0, 38, 0)

	if a.GetNeededTime() != expectedResult {
		t.Errorf("GetNeededTime should return %v but returned %v", expectedResult, a.GetNeededTime())
	}
}

func TestCaseExecutionProtocol_Finish(t *testing.T) {
	const OverAllResultErrorMessage = "Result wasn't saved correctly to Protocol. " +
		"Result is %s but expected %s"
	const OverAllCommentErrorMessage = "Comment wasn't saved correctly to Protocol. " +
		"Comment is %s but expected %s"
	const NeededTimeErrorMessage = "NeededTime wasn't saved correctly to Protocol."

	ProjectID := id.NewProjectID(id.NewActorID("owner"), "d8ed5f42")
	TestCase := NewTestCase("Test-TestCase", "Desc", "None", []project.Label{},
		map[string]*project.Variant{}, duration.NewDuration(0, 20, 0), ProjectID)
	TestCaseVersionNr := 1

	SUTVersion := "Version 1.02"
	SUTVariant := "Win 10"
	initalTime := duration.NewDuration(1, 2, 3)

	OverAllResult := PassWithComment
	OverAllComment := "All OK"
	neededTime := duration.NewDuration(0, 1, 0)
	tcv := TestCase.TestCaseVersions[len(TestCase.TestCaseVersions)-TestCaseVersionNr]

	a, err := NewCaseExecutionProtocol(protocolListerStub{}, tcv.ID(), SUTVariant, SUTVersion, initalTime,
		len(tcv.Steps))
	if err != nil {
		t.Error(err)
	}
	neededTimeOld := a.GetNeededTime()
	a.Finish(OverAllResult, OverAllComment, neededTime)

	if a.Result != OverAllResult {
		t.Errorf(OverAllResultErrorMessage, a.Result, OverAllResult)
	}
	if a.Comment != OverAllComment {
		t.Errorf(OverAllCommentErrorMessage, a.Comment, OverAllComment)
	}
	if a.OtherNeededTime != neededTime.Add(initalTime) || neededTimeOld.Add(neededTime) != a.GetNeededTime() {
		t.Error(NeededTimeErrorMessage)
	}
}

func TestCaseExecutionProtocol_SaveStep(t *testing.T) {
	const ObservedBehaviorsErrorMessage = "ObservedBehavior wasn't saved correctly to Protocol. " +
		"ObservedBehavior is %q but expected %q"
	const ResultsErrorMessage = "Result wasn't saved correctly to Protocol. Result is %q but expected %q"
	const CommentsErrorMessage = "Comment wasn't saved correctly to Protocol. Comment is %q but expected %q"
	const NeededTimeErrorMessage = "NeededTime wasn't saved correctly to Protocol. NeededTime is %q but expected %q"
	const VisitedErrorMessage = "Saved test step was marked as not visited, but was visited"

	TestCaseVersionNr := 1
	ProjectID := id.NewProjectID(id.NewActorID("owner"), "d8ed5f42")
	SUTVersion := "Version 1.02"
	SUTVariant := "Win 10"
	initialTime := duration.NewDuration(1, 2, 3)

	TestStep := Step{"Do something!", "All Ok", 5}
	TestCase := NewTestCase("Test-TestCase", "Desc", "None", []project.Label{},
		map[string]*project.Variant{}, duration.NewDuration(0, 20, 0), ProjectID)
	tcvIndex := len(TestCase.TestCaseVersions) - TestCaseVersionNr
	TestCase.TestCaseVersions[tcvIndex].Steps = []Step{TestStep, TestStep, TestStep}
	tcv := TestCase.TestCaseVersions[tcvIndex]

	StepNr := 2
	ObservedBehavior := "Nothing Happened"
	result := Fail
	Comment := ""
	NeededTime := duration.NewDuration(0, 2, 0)

	a, err := NewCaseExecutionProtocol(protocolListerStub{}, tcv.ID(), SUTVariant, SUTVersion, initialTime,
		len(tcv.Steps))
	if err != nil {
		t.Error(err)
	}
	a.SaveStep(StepNr, ObservedBehavior, result, Comment, NeededTime)

	if a.StepProtocols[StepNr-1].ObservedBehavior != ObservedBehavior {
		t.Errorf(ObservedBehaviorsErrorMessage, a.StepProtocols[StepNr-1].ObservedBehavior, ObservedBehavior)
	}
	if a.StepProtocols[StepNr-1].Result != result {
		t.Errorf(ResultsErrorMessage, a.StepProtocols[StepNr-1].Result, result)
	}
	if a.StepProtocols[StepNr-1].Comment != Comment {
		t.Errorf(CommentsErrorMessage, a.StepProtocols[StepNr-1].Comment, Comment)
	}
	if a.StepProtocols[StepNr-1].NeededTime != NeededTime {
		t.Errorf(NeededTimeErrorMessage, a.StepProtocols[StepNr-1].NeededTime, NeededTime)
	}
	if !a.StepProtocols[StepNr-1].Visited {
		t.Error(VisitedErrorMessage)
	}
}

func TestNewCaseExecutionProtocol(t *testing.T) {
	const SUTErrorMessage = "SUTVersion wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %q but expected %q"
	const SUTVariantErrorMessage = "SUTVariant wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %q but expected %q"
	const DateErrorMessage = "ExecutionDate wasn't saved correctly to new Protocol. " +
		"Value in new Protocol is %v but expected current time: %v"
	const OAResultErrorMessage = "Result should be %s but was %s"
	const ProjectIDErrorMessage = "Testcase version wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %v+ but expected %v+"
	const TimeErrorMessage = "Needed Time wasn't transferred correctly to new Protocol. " +
		"Value in new Protocol is %v+ but expected %v+"

	ProjectID := id.NewProjectID(id.NewActorID("owner"), "d8ed5f42")
	TestCase := NewTestCase("Test-TestCase", "Desc", "None", []project.Label{},
		map[string]*project.Variant{}, duration.NewDuration(0, 20, 0), ProjectID)

	tcv := TestCase.TestCaseVersions[len(TestCase.TestCaseVersions)-1]
	SUTVersion := "Version 1.02"
	SUTVariant := "Win 10"
	initialTime := duration.NewDuration(1, 2, 3)

	a, err := NewCaseExecutionProtocol(protocolListerStub{}, tcv.ID(), SUTVariant, SUTVersion, initialTime,
		len(tcv.Steps))
	if err != nil {
		t.Error(err)
	}

	if a.TestVersion != tcv.ID() {
		t.Errorf(ProjectIDErrorMessage, a.TestVersion, tcv.ID())
	}
	if a.SUTVersion != SUTVersion {
		t.Errorf(SUTErrorMessage, a.SUTVersion, SUTVersion)
	}
	if a.SUTVariant != SUTVariant {
		t.Errorf(SUTVariantErrorMessage, a.SUTVariant, SUTVariant)
	}
	if time.Until(a.ExecutionDate) > time.Second {
		t.Errorf(DateErrorMessage, a.ExecutionDate.Round(time.Minute), time.Now().Round(time.Minute))
	}
	if a.Result != NotAssessed {
		t.Errorf(OAResultErrorMessage, a.Result, NotAssessed)
	}
	if a.OtherNeededTime != initialTime {
		t.Errorf(TimeErrorMessage, a.OtherNeededTime, initialTime)
	}
}

func TestCaseExecutionProtocol_DeepCopy(t *testing.T) {

	const SUTErrorMessage = "Original SUTVersion was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const SUTVariantErrorMessage = "Original SUTVariant was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const DateErrorMessage = "Original ExecutionDate was changed by changing the DeepCopy. " +
		"Original is %v but expected %v"
	const NeededTimesErrorMessage = "Original NeededTimes[0] was changed by changing the DeepCopy. " +
		"Original is %v but expected %v"
	const ObsBehaviorsErrorMessage = "Original ObservedBehaviors[2] was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const ResultsErrorMessage = "Original Results[0] was changed by changing the DeepCopy. " +
		"Original is %s but expected %s"
	const CommentsErrorMessage = "Original Comments[1] was changed by changing the DeepCopy. " +
		"Original is %q but expected %q"
	const OAResult = "Original Result was changed by changing the DeepCopy. " +
		"Original is %s but expected %s"
	const OAComment = "Original Comment was changed by changing the DeepCopy. " +
		"Original is %s but expected %s"
	const ProjectIDErrorMessage = "Original CaseVersionID was changed by changing the DeepCopy. " +
		"Original is %v+ but expected %v+"

	tcvID := id.NewTestVersionID(id.NewTestID(id.NewProjectID(id.NewActorID("owner"),
		"d8ed5f42"), "123456G", true), 1)
	SUTVersion := "Version 1.02"
	SUTVariant := "Win 10"

	ExecutionDate := time.Date(2000, 12, 24, 12, 0, 0, 0, time.UTC)
	OverAllResult := NotAssessed
	OverAllComment := "All OK"

	a := CaseExecutionProtocol{
		TestVersion: tcvID,
		SUTVersion:  SUTVersion,
		SUTVariant:  SUTVariant,

		ExecutionDate: ExecutionDate,
		StepProtocols: []StepExecutionProtocol{stepPrt1.DeepCopy(), stepPrt2.DeepCopy(), stepPrt3.DeepCopy()},
		Result:        OverAllResult,
		Comment:       OverAllComment,
	}
	b := a.DeepCopy()

	if !a.Equals(b) {
		t.Errorf("DeepCopy is not equal to original. \n Original: %+v \n DeepCopy: %+v", a, b)
	}

	b.TestVersion = id.NewTestVersionID(id.NewTestID(id.NewProjectID(id.NewActorID("new owner"),
		"84sd8f"), "48d584s", true), 5)
	b.SUTVersion = "Version 0.8"
	b.SUTVariant = "Linux"
	b.ExecutionDate = time.Now()
	b.StepProtocols[0].NeededTime = duration.NewDuration(0, 78, 0)
	b.StepProtocols[2].ObservedBehavior = "Hello"
	b.StepProtocols[0].Result = Pass
	b.StepProtocols[1].Comment = "Hello!"
	b.Result = Pass
	b.Comment = "Alarm! Major errors!"

	assertEqual(a.TestVersion, tcvID, ProjectIDErrorMessage, t)
	assertEqual(a.SUTVersion, SUTVersion, SUTErrorMessage, t)
	assertEqual(a.SUTVariant, SUTVariant, SUTVariantErrorMessage, t)
	assertEqual(a.ExecutionDate, ExecutionDate, DateErrorMessage, t)
	assertEqual(a.StepProtocols[0].NeededTime, stepPrt1.NeededTime, NeededTimesErrorMessage, t)
	assertEqual(a.StepProtocols[2].ObservedBehavior, stepPrt3.ObservedBehavior, ObsBehaviorsErrorMessage, t)
	assertEqual(a.StepProtocols[0].Result, stepPrt1.Result, ResultsErrorMessage, t)
	assertEqual(a.StepProtocols[1].Comment, stepPrt2.Comment, CommentsErrorMessage, t)
	assertEqual(a.Result, OverAllResult, OAResult, t)
	assertEqual(a.Comment, OverAllComment, OAComment, t)
}

func TestCaseExecutionProtocol_Equals(t *testing.T) {
	a := defaultCaseProtocol.DeepCopy()
	b := a

	evaluateEqualityTest(a, b, true, t, "A and B are exactly the same.")
	evaluateEqualityTest(a, nil, false, t, "B was nil, but A wasn't.")
	var empty *CaseExecutionProtocol
	evaluateEqualityTest(a, empty, false, t, "B was an pointer with value nil, but A wasn't.")
	evaluateEqualityTest(a, "I'm not the expected Type", false, t, "B was of wrong type.")
	b = a.DeepCopy()
	b.TestVersion = id.NewTestVersionID(id.NewTestID(id.NewProjectID(id.NewActorID("new owner"),
		"84sdsdfg"), "456d84s", true), -5)
	evaluateEqualityTest(a, b, false, t, "CaseVersionID of B was different from A.")
	b = a.DeepCopy()
	b.SUTVersion = "Version 0.84"
	evaluateEqualityTest(a, b, false, t, "SUTVersion of B was different from A.")
	b = a.DeepCopy()
	b.SUTVariant = "Unix"
	evaluateEqualityTest(a, b, false, t, "SUTVariant of B was different from A.")
	b = a.DeepCopy()
	b.ExecutionDate = time.Now()
	evaluateEqualityTest(a, b, false, t, "ExecutionDate of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols[0].NeededTime = duration.NewDuration(0, 0, 0)
	evaluateEqualityTest(a, b, false, t, "NeededTimes[0] of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols = append(b.StepProtocols, stepPrt5)
	evaluateEqualityTest(a, b, false, t,
		"StepProtocols of B is one entry longer than StepProtocols of A.")
	b = a.DeepCopy()
	b.StepProtocols[4].NeededTime = duration.NewDuration(0, 0, 4)
	evaluateEqualityTest(a, b, false, t, "NeededTimes[4] of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols[1].ObservedBehavior = "Ring-Ding-Dong!!!"
	evaluateEqualityTest(a, b, false, t, "ObservedBehaviors[1] of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols[2].ObservedBehavior = ""
	evaluateEqualityTest(a, b, false, t, "ObservedBehaviors[2] of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols[1].Result = 0
	evaluateEqualityTest(a, b, false, t, "Results[1] of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols[0].Result = 1
	evaluateEqualityTest(a, b, false, t, "Results[0] of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols[2].Comment = "World?"
	evaluateEqualityTest(a, b, false, t, "Comments[2] of B was different from A.")
	b = a.DeepCopy()
	b.StepProtocols[1].Comment = ""
	evaluateEqualityTest(a, b, false, t, "Comments[1] of B was different from A.")
	b = a.DeepCopy()
	b.Result = Pass
	evaluateEqualityTest(a, b, false, t, "Result of B was different from A.")
	b = a.DeepCopy()
	b.Comment = "No, not ok. We have to do it again"
	evaluateEqualityTest(a, b, false, t, "Comment of B was different from A.")
}

func evaluateEqualityTest(a CaseExecutionProtocol, b interface{}, expected bool, t *testing.T, ErrorSuffix string) {
	const ErrorMessage = "Equals does not work symmetric! 'a.Equals(b)' returned %t, but 'b.Equals(a)' returned %t."

	resultA := a.Equals(b)

	if v, ok := b.(CaseExecutionProtocol); ok {
		resultB := v.Equals(a)

		if resultA != resultB {
			t.Errorf(ErrorMessage, resultA, resultB)
		}
	}
	if resultA != expected {
		t.Errorf("Equals-Function returend %t, but expected %t. "+ErrorSuffix, resultA, expected)
	}
}
