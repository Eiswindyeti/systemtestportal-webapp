/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package comment

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// Comment represents a commentary text posted by a user
// to a test case or test sequence
type Comment struct {
	ID           id.TestID
	Text         string
	Author       user.User
	CreationDate time.Time
}

// NewComment creates a new comment
func NewComment(id id.TestID, text string, author user.User) Comment {
	date := time.Now().Round(time.Second)
	c := Comment{id, text, author, date}
	return c
}
