/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package comment

import (
	"testing"

	"time"

	id2 "gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

func TestNewComment(t *testing.T) {
	pID := id2.NewProjectID(id2.ActorID(defaultUser().Name), "")
	id := id2.NewTestID(pID, "test-id-viable", false)
	text := "One Comment"
	textWant := "One Comment"
	author := defaultUser()
	authorWant := defaultUser()

	c := NewComment(id, text, author)
	if c.ID != id {
		t.Errorf("Comment Id = %+v, want %+v", c.ID, id)
	}
	if c.Text != textWant {
		t.Errorf("Commenttext = %s, want %s", c.Text, textWant)
	}
	if c.Author != authorWant {
		t.Errorf("Comment author = %s, want %s", c.Author, authorWant)
	}
}

func defaultUser() user.User {
	return user.User{
		DisplayName:      "The Lord",
		Name:             "the-lord",
		EMail:            "",
		RegistrationDate: time.Now().Round(time.Second)}
}
