/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package group

import (
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

// Group represents a group with its information in the system
type Group struct {
	Name         string
	Description  string
	Visibility   visibility.Visibility
	Members      map[string]user.User
	CreationDate time.Time
}

// ItemName returns a groups name
func (g Group) ItemName() string {
	return g.Name
}

// ID returns a groups id struct
func (g Group) ID() id.ActorID {
	return id.NewActorID(g.Name)
}

// ItemVisibility returns a groups visibility
func (g Group) ItemVisibility() visibility.Visibility {
	return g.Visibility
}

// NewGroup creates a new group
func NewGroup(name, description string, visibility visibility.Visibility, members map[string]user.User) Group {
	name = strings.TrimSpace(name)
	description = strings.TrimSpace(description)
	creationDate := time.Now().Round(time.Second)

	g := Group{
		Name:         name,
		Description:  description,
		Visibility:   visibility,
		Members:      members,
		CreationDate: creationDate,
	}

	return g
}
