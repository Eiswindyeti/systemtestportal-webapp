/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"fmt"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// ProtocolExistenceChecker provides everything needed to check, if a protocol is already existent in storage.
type ProtocolExistenceChecker interface {
	//Exists checks, if the given protocol is already existent in storage.
	Exists(id ProtocolID) (bool, error)
}

// nonPositiveProtocolNr returns an ErrorMessage detailing that a given ProtocolNr is null or negative, but shouldn't.
func nonPositiveProtocolNr() error {
	return errors.ConstructStd(http.StatusBadRequest, "Invalid ProtocolNr",
		fmt.Sprintf("The given ProtocolNr is null or negative, but shouldn't."), nil).
		WithLog("The ProtocolNr, send by the client, is null or negative, but shouldn't..").
		WithStackTrace(2).
		Finish()
}

//ProtocolID hold all needed data to identify the a protocol in the whole system.
//This is the number of the protocol and the test version that was executed.
//Protocol can be related to a case verison or a sequence version
type ProtocolID struct {
	TestVersionID
	protocol int
}

//NewProtocolID returns a new ProtocolID containing the given protocol number and related test version.
//No validation will be performed.
func NewProtocolID(testVersion TestVersionID, protocol int) ProtocolID {
	return ProtocolID{testVersion, protocol}
}

//Protocol returns the protocol number
func (id ProtocolID) Protocol() int {
	return id.protocol
}

//Validate checks if this is a valid id for a new protocol.
func (id ProtocolID) Validate(pec ProtocolExistenceChecker) error {
	if id.Protocol() <= 0 {
		return nonPositiveProtocolNr()
	}
	exists, err := pec.Exists(id)
	if err != nil {
		return err
	}
	if exists {
		return alreadyExistsErr(id)
	}
	return nil
}
