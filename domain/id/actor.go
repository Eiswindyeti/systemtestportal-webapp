/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

// ActorExistenceChecker provides everything needed to check, if an actor is already existent in storage.
type ActorExistenceChecker interface {
	//Exists checks, if the given actor is already existent in storage.
	Exists(id ActorID) (bool, error)
}

//ActorID hold all needed data to identify the an actor in the whole system.
//For an actor this is only his name
//Actors can be users or groups
type ActorID string

//NewActorID returns a new ActorID containing the given name. No validation will be performed.
func NewActorID(actor string) ActorID {
	return ActorID(actor)
}

//Actor returns the name of the actor
func (id ActorID) Actor() string {
	return string(id)
}

//Validate checks if this is a valid id for a new actor.
func (id ActorID) Validate(oec ActorExistenceChecker) error {
	exists, err := oec.Exists(id)
	if err != nil {
		return err
	}
	if exists {
		return alreadyExistsErr(id)
	}
	return validateName(id.Actor())
}
