/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"time"

	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

// The Project struct contains information used to describe a project in the system
type Project struct {
	Name         string
	Description  string
	Visibility   visibility.Visibility
	CreationDate time.Time
	Owner        id.ActorID
	Variants     map[string]*Variant
	Labels       []Label
	Members      map[id.ActorID]*user.User
}

// NewProject creates a new project
func NewProject(name string, owner id.ActorID, description string, visibility visibility.Visibility) Project {
	name = strings.TrimSpace(name)
	description = strings.TrimSpace(description)
	creationDate := time.Now().Round(time.Second)

	p := Project{
		Name:         name,
		Description:  description,
		Visibility:   visibility,
		CreationDate: creationDate,
		Owner:        owner,

		Variants: map[string]*Variant{},
		Members:  map[id.ActorID]*user.User{},
	}

	return p
}

// ItemName returns a projects name
func (p Project) ItemName() string {
	return p.Name
}

// ID returns a projects id
func (p Project) ID() id.ProjectID {
	return id.NewProjectID(p.Owner, p.Name)
}

// ItemVisibility returns a projects visibility
func (p Project) ItemVisibility() visibility.Visibility {
	return p.Visibility
}

//AddMember adds the given user to the project
func (p Project) AddMember(member *user.User) {
	p.Members[id.ActorID(member.Name)] = member
}

//RemoveMember removes the given user of the project
func (p Project) RemoveMember(member *user.User) {
	delete(p.Members, id.ActorID(member.Name))
}
