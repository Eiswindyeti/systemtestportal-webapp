# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Downloads

Releases of the SystemTestPortal can be downloaded from the [tags](https://gitlab.com/stp-team/systemtestportal-webapp/tags).
Nightly builds can be found on the [FTP Server of the University of Stuttgart](ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/).

## [Unreleased]

## [v1.0.0-rc2] - 16/04/2018

### Fixed
- Fix execution of test-sequences. Previously only the first case could be executed
- Clicking on an older version in the history of a test now requests the correct url
- Clicking on the version of a test in a test-protocol now requests the correct url
- Show new-project button on explore-projects page if you are signed in
- When creating new project, the owner is added to the members and can add/remove members

## [v1.0.0-rc1] - 31/03/2018

### Added
- Projects have members now

### Fixed
- Fix bug where an empty test step was created for every manually entered test step on test case creation

## [v1.0.0-beta] - 27/03/2018

### Added
- Show the needed time for testing in the summary page of a text execution
- Add Architectural Decision Records (https://adr.github.io/)
- Test for handlers
- Test plan to manually test the SystemTestPortal

### Changed
- Only renaming a test does not create a new test version anymore because there would not be any visible changes in the history of a test
- Url encoding for projects and tests
- Remember which protocol was selected
- By default, the support for sqlite is disabled due to issues with broken binaries (See #223)

### Fixed
- Fix binaries not starting anymore (See #223)
- Fix registration of users
- Fix disappearing protocols after renaming a test case
- Fix a bug that did not allow the name of a test case being changed back to its previous name
- Show correct results of executed test sequences in protocols

### Known issues
- Test sequences without any applicable sut-versions can't be executed (#239)
- During an test sequence execution the start page of each test case shows an empty dropdown instead of the selected sut-variant + sut-version (#236)

## [v0.11.0] - 13/02/2018

### Added
- Add persistent storage
- Add docker image

### Fixed
- Fix test sequence protocols showing "Invalid Date" for execution date
- Fix listing of projects. Now all projects can be listed on the explore page
- Fix wording of error when test sequence cannot be updated. The error does not show "Can not update test case" anymore.

### Known issues
- Registering a user does not work
- Renaming a project does not work correctly
- Protocols of test sequences show result as "Not assessed" altough there is a result
- The urls are sometimes encoded wrong

## [v0.10.0] - 30/01/2018

### Added
- Add comments for test cases and test sequences
- Add settings for projects
- Improved GUI

### Fixed
- Automatically update the list of sut-versions with the text when adding a sut-version to a project containing no sut-versions
- Only update the labels when clicking "save" instead of updating them when the modal closes
- Fix missing characters in the protocol title (e.g. a missing "ß")
- Trying to sign in with a wrong password now shows a full error page

### Security
- Add hashing of passwords in the database

### Known issues
- Protocols of test sequences contain the wrong test case protocols
- Not all projects are listed in the explore tab. Only the projects of the "default" user are listed
- Renaming or deleting a test prevents its protocols from being listed

## [v0.9.0] - 23/01/2018

### Added
- Add a print dialog for the list of test cases and test sequences
- Add a print dialog for the detailed view of test cases and test sequences
- Add a print dialog for test cases and test sequences with free input forms to execute the tests with pen and paper
- Add option to delete a variant of the sut
- Add labels for test cases and test sequences

### Changed
- Move hardcoded strings (paths, parameters, keys) to constants
- Wrap up ids of projects, groups, tests, users

## [v0.8.0] - 08/01/2018

### Added
- Add a timer to track the time needed for a test execution
- Add system-under-test variants in addition to the existing system-under-test-versions
- Use https://github.com/dimfeld/httptreemux as a router for the requests

### Fixed
- Fix whitespace in the textarea of the commit-message when editing tests

## [v0.6.0] - 24/11/2017

This is the first public release of the SystemTestPortal. Basic features such as the creation of projects, test cases and test sequences and the execution are implemented.
Additionally, there is a basic concept of users and permission management. Some features are restricted to signed-in users.

### Added
- Simple registration process
- Features limited to signed-in users (e.g. creating tests)
- Add creation of single tests and compound test sequences
- Add editing of tests and test sequences
- Add history for tests. Older versions of a test can be shown but not edited
- Add execution for tests and test sequences
- Add protocols to show results of executed tests and test sequences
