/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"github.com/dimfeld/httptreemux"
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/store/protocol"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func registerProjectsHandler(r *httptreemux.ContextMux) {
	userStore := store.GetUserStore()
	groupStore := store.GetGroupStore()
	projectStore := store.GetProjectStore()

	tcStore := store.GetCaseStore()
	tsStore := store.GetSequenceStore()
	protocolStore := protocol.GetProtocolStore()

	nBasic := negroni.New(middleware.Container(userStore, groupStore))
	n := nBasic.With(middleware.Project(projectStore))

	registerProjectCreateHandler(r, nBasic, projectStore)

	pg := wrapContextGroup(r.NewContextGroup(VarContainer + VarProject))

	registerProjectHandler(pg, n, projectStore)
	registerCasesHandler(pg, n, tcStore)
	registerSequencesHandler(pg, n, tsStore)
	registerProtocolsHandler(pg, n, tcStore, tsStore, protocolStore)
	registerMembersHandler(pg, n)
}

func registerProjectCreateHandler(r *httptreemux.ContextMux, nBasic *negroni.Negroni, projectStore store.Projects) {

	r.Handler(http.MethodGet, VarContainer+Projects+New,
		nBasic.With(negroni.WrapFunc(display.CreateProjectGet)))
	r.Handler(http.MethodPost, VarContainer+Projects+Save,
		nBasic.With(negroni.WrapFunc(creation.ProjectPost(projectStore, projectStore))))
}

func registerProjectHandler(pg *contextGroup, n *negroni.Negroni, ps store.Projects) {

	pg.HandlerGet(Show,
		n.With(negroni.Wrap(DefaultRedirect("testcases"))))

	registerProjectVersionsHandler(pg, n, ps)
	registerProjectSettingsHandler(pg, n, ps)
	registerProjectLabelsHandler(pg, n, ps)
}

func registerProjectVersionsHandler(pg *contextGroup, n *negroni.Negroni, ps handler.ProjectAdder) {
	pg.HandlerGet(Versions,
		n.With(negroni.WrapFunc(json.ProjectVariantsGet)))
	pg.HandlerPost(Versions,
		n.With(negroni.WrapFunc(json.ProjectVariantsPost(ps))))
}

func registerProjectSettingsHandler(pg *contextGroup, n *negroni.Negroni, ps store.Projects) {
	pg.HandlerGet(Settings,
		n.With(negroni.WrapFunc(display.ProjectSettingsGet)))
	pg.HandlerPost(Settings,
		n.With(negroni.WrapFunc(update.ProjectPost(ps, ps))))

	pg.HandlerDelete(Settings+"/",
		n.With(negroni.WrapFunc(deletion.ProjectDelete(ps))))
}

func registerProjectLabelsHandler(pg *contextGroup, n *negroni.Negroni, ps handler.ProjectAdder) {
	pg.HandlerGet(Labels,
		n.With(negroni.WrapFunc(json.ProjectLabelsGet)))
	pg.HandlerPost(Labels,
		n.With(negroni.WrapFunc(json.ProjectLabelsPost(ps))))
}
