/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"net/http"

	"log"
	"net/url"
	"path"
	"runtime/debug"

	"github.com/dimfeld/httptreemux"
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

// InitRouter initializes the main router that
// handles all incoming requests
func InitRouter() http.Handler {
	n := initSession()

	r := registerAuthRouter()
	rs := registerStaticRouter()

	registerAboutGet(r)
	registerAccountHandler(r)
	registerExploreHandler(r)
	registerProjectsHandler(r)
	registerGroupHandler(r)
	r.PathSource = httptreemux.URLPath

	n.UseHandler(r)
	rs.Handler(http.MethodGet, "/", DefaultRedirect(Explore))
	allMethods(rs, "/*", n)
	rs.PathSource = httptreemux.URLPath

	return rs
}

func initSession() *negroni.Negroni {
	sessions.InitSessionManagement(nil, nil)
	sessionStore := sessions.GetSessionStore()
	n := negroni.New(middleware.Auth(sessionStore))

	return n
}

func registerAuthRouter() *httptreemux.ContextMux {
	r := httptreemux.NewContextMux()
	r.NotFoundHandler = NotFound
	r.MethodNotAllowedHandler = MethodNotAllowed
	r.PanicHandler = panic

	return r
}

func registerStaticRouter() *httptreemux.ContextMux {
	rs := httptreemux.NewContextMux()
	rs.NotFoundHandler = NotFound
	rs.MethodNotAllowedHandler = MethodNotAllowed
	rs.PanicHandler = panic
	rs.Handler(http.MethodGet, "/static/*", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	rs.Handler(http.MethodGet, "/favicon.ico", http.FileServer(http.Dir("static/img/favicons/")))

	return rs
}

const (
	issueTracker = "<a href='" +
		"https://gitlab.com/stp-team/systemtestportal-webapp/issues'>issue tracker" +
		"</a>"
	errPanicTitle = "An unexpected error occurred."
	errPanic      = "It seems your request couldn't be handled correctly. " +
		"This is most likely a bug. If you want  " +
		"please contact us via our " + issueTracker + "."
)

// panic is used to handle panic while routing.
func panic(w http.ResponseWriter, r *http.Request, v interface{}) {
	if err, ok := v.(error); ok {
		log.Print("Recovered from panic with following error:")
		errors.Handle(err, w, r)
	} else {
		log.Printf("Recovered from panic with following content:\n%v", v)
		errors.ConstructStd(http.StatusInternalServerError,
			errPanicTitle, errPanic, r).
			Respond(w)
	}
	debug.PrintStack()
}

// Redirect returns a simple handler that just
// redirects to given url using given status.
func Redirect(redirectURL string, status int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//This workaround is necessary, because otherwise the url-package will complete the relative url to an absolute.
		//And will also encode the path, but not enough chars. Therefor we do this manually and use RawPath.
		newURL := redirectURL
		if u, err := url.Parse(newURL); err == nil &&
			u.Scheme == "" &&
			u.Host == "" &&
			(newURL == "" || newURL[0] != '/') {
			oldPath := r.URL.EscapedPath()
			if oldPath == "" {
				oldPath = "/"
			}
			oldDir, _ := path.Split(oldPath)
			newURL = oldDir + newURL
		}
		http.Redirect(w, r, newURL, status)
	}
}

// DefaultRedirect returns a simple handler that just
// redirects to given url using 303 as status.
func DefaultRedirect(url string) http.HandlerFunc {
	return Redirect(url, http.StatusSeeOther)
}

const (
	errNotFoundTitle = "The page you requested doesn't exist."
	errNotFound      = "We are sorry, but we were unable to resolve the page " +
		"you requested. If you believe this is a bug " +
		"please contact us via our " + issueTracker + "."
	errMethodNotAllowedTitle = "Method not allowed."
	errMethodNotAllowed      = "It seems the request you send used an " +
		"unsupported method. If you believe this is a bug " +
		"please contact us via our " + issueTracker + "."
)

// NotFound responds with a not found page on every request.
func NotFound(w http.ResponseWriter, r *http.Request) {
	errors.ConstructStd(http.StatusNotFound,
		errNotFoundTitle, errNotFound, r).
		Respond(w)
}

// MethodNotAllowed responds with a method not allowed page on every request.
//noinspection GoUnusedParameter
func MethodNotAllowed(w http.ResponseWriter, r *http.Request,
	methods map[string]httptreemux.HandlerFunc) {
	errors.ConstructStd(http.StatusMethodNotAllowed,
		errMethodNotAllowedTitle, errMethodNotAllowed, r).
		Respond(w)
}
