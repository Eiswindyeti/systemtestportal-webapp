/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// SequenceDelete handles requests that demand the deletion of a sequence.
func SequenceDelete(tsd handler.TestSequenceDeleter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ts, err := handler.GetTestSequence(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		err = tsd.Delete(ts.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		http.Redirect(w, r, "..?fragment=true", http.StatusSeeOther)
	}
}
