/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// The error-messages for the membership in a project
const (
	failedMembership       = "failed membership"
	unableToRemoveMember   = "We were unable to remove the requested user as a member from the project."
	removeOwner            = "remove owner"
	ownerCantRemoveHimself = "The owner is not allowed to remove himself from the project members."
)

// MemberDelete is used to remove a member from the project
func MemberDelete(us middleware.UserRetriever) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		mID := r.FormValue(httputil.Members)
		var x []string
		err = json.Unmarshal([]byte(mID), &x)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		for _, i := range x {
			tid := id.ActorID(i)
			m, ok, err := us.Get(tid)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
			if !ok {
				errors.ConstructStd(http.StatusInternalServerError,
					failedMembership, unableToRemoveMember, r).
					WithLogf("Unable to get the user with id %v!", mID).
					WithStackTrace(1).
					WithRequestDump(r).
					Respond(w)
				return
			}
			if strings.Compare(p.Owner.Actor(), m.Name) == 0 {
				er := errors.ConstructStd(http.StatusBadRequest,
					removeOwner, ownerCantRemoveHimself, nil).
					WithLogf("Tried to remove owner from the project members.").
					WithStackTrace(1).
					WithRequestDump(r).
					Finish()
				errors.Handle(er, w, r)
			} else {
				p.RemoveMember(m)
			}
		}
	}
}
