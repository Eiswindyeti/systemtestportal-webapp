/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestSequenceDelete(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceDeletionMock{}
				return SequenceDelete(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodDelete, handler.NoParams),
		),
		handler.CreateTestHandler("Successful delete",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceDeletionMock{}
				return SequenceDelete(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusSeeOther),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodDelete, handler.NoParams),
		),
	)
}
