/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// The error-messages for the settings of a project
const (
	errCanNotDeleteProjectTitle = "Couldn't delete project."
	errCanNotDeleteProject      = "We are sorry but we were unable to delete the project as you requested." +
		"If you believe this is a bug please contact us via our <a href='" +
		"https://gitlab.com/stp-team/systemtestportal-webapp/issues'>issue tracker" +
		"</a>. "
)

// ProjectDelete is used to delete a project in the system
func ProjectDelete(pd handler.ProjectDeleter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		if err := pd.Delete(p.ID()); err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				errCanNotDeleteProjectTitle, errCanNotDeleteProject, r).
				WithLog("Unable to delete project").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}
	}
}
