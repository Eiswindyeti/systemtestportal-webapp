/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package assignment

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

const (
	failedAssignment   = "assignment failed"
	unableToAssignUser = "We were unable to assign the requested user as tester."
)

// getTesters receives the tester ids and returns the users with these ids
func getTesters(us middleware.UserRetriever, w http.ResponseWriter, r *http.Request) []*user.User {
	testerIds := r.FormValue(httputil.NewTesters)
	var x []string
	err := json.Unmarshal([]byte(testerIds), &x)
	if err != nil {
		errors.Handle(err, w, r)
	}
	var ttIds []*user.User
	for _, t := range x {
		u, ok, err := us.Get(id.ActorID(t))
		if err != nil {
			errors.Handle(err, w, r)
		}
		if !ok {
			errors.ConstructStd(http.StatusInternalServerError,
				failedAssignment, unableToAssignUser, r).
				WithLogf("Unable to get the user with id %v!", t).
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return nil
		}
		ttIds = append(ttIds, u)
	}
	return ttIds
}
