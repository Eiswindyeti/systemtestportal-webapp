/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// InvalidVisibility returns an ErrorMessage describing that a value is not valid.
func InvalidVisibility() errors.HandlerError {
	return errors.ConstructStd(http.StatusBadRequest,
		"Invalid visibility", "The visibility has to be "+
			"either public, internal or private",
		nil).
		WithLog("The client send an invalid visibility handle.").
		WithStackTrace(2).
		Finish()
}

// InvalidTCVersion returns an ErrorMessage describing that the test case version is not valid
func InvalidTCVersion() errors.HandlerError {
	return errors.ConstructStd(http.StatusBadRequest,
		"Invalid test case version", "The test case"+
			"version is invalid", nil).
		WithLog("Invalid test case version send by client.").
		WithStackTrace(2).
		Finish()
}
