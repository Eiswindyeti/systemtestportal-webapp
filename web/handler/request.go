/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"net/http"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// StringToInt gets a string and converts it to int.
// Returns -1 if the string cannot be converted to an int.
func StringToInt(s string) int {
	ver, err := strconv.ParseInt(s, 10, 32)
	if err != nil {
		return -1
	}
	return int(ver)
}

// GetVersion gets the version field from a request.
// Returns defaultVersion if the version is empty.
// Returns -1 if the version is not an integer.
func GetVersion(r *http.Request, defaultVersion int) int {
	ver := r.FormValue(httputil.Version)
	if ver == "" {
		return defaultVersion
	}
	return StringToInt(ver)
}

const (
	errInvalidTestCaseVersionTitle = "Invalid testcase version."
	errInvalidTestCaseVersion      = "Your client tried to access an invalid version " +
		"of a testcase. If you believe this is a bug please contact us via our " +
		IssueTracker + "."
)

// GetTestCaseVersion gets the version of a testcase from a request.
// If the request doesn't contain a version number the latest is assumed.
// If the number send by the request is out of the range of possible versions
// an error is returned.
func GetTestCaseVersion(r *http.Request, tc *test.Case) (*test.CaseVersion, error) {
	latest := len(tc.TestCaseVersions)
	v := GetVersion(r, latest)
	if 0 > v || v > latest {
		return nil, errors.ConstructStd(http.StatusBadRequest,
			errInvalidTestCaseVersionTitle, errInvalidTestCaseVersion, r).
			WithLogf("Client sent invalid testcase version number %d. "+
				"Should be in between 1 and %d.", v, latest).
			WithStackTrace(1).
			WithRequestDump(r).
			Finish()
	}
	return &tc.TestCaseVersions[len(tc.TestCaseVersions)-v], nil
}

// GetLatestTestCaseVersion gets the version of a testcase from a request.
// If the request doesn't contain a version number the latest is assumed.
// If the number send by the request is not the latest version an error is returned.
func GetLatestTestCaseVersion(r *http.Request, tc *test.Case) (*test.CaseVersion, error) {
	latest := len(tc.TestCaseVersions)
	v := GetVersion(r, latest)
	if v != latest {
		return nil, errors.ConstructStd(http.StatusBadRequest,
			errInvalidTestCaseVersionTitle, errInvalidTestCaseVersion, r).
			WithLogf("Client sent invalid testcase version number %d."+
				"Should've been %d.", v, latest).
			WithStackTrace(1).
			WithRequestDump(r).
			Finish()
	}
	return &tc.TestCaseVersions[len(tc.TestCaseVersions)-v], nil
}

// Error-messages for retrieving sequence versions from the request
const (
	ErrInvalidTestSequenceVersionTitle = "Invalid test sequence version."
	ErrInvalidTestSequenceVersion      = "Your client tried to access an invalid version " +
		"of a test sequence. If you believe this is a bug please contact us via our " +
		IssueTracker + "."
)

// GetTestSequenceVersion gets the version of a testsequence from a request.
// If the request doesn't contain a version number the latest is assumed.
// If the number send by the request is out of the range of possible versions
// an error is returned.
func GetTestSequenceVersion(r *http.Request, ts *test.Sequence) (*test.SequenceVersion, error) {
	latest := len(ts.SequenceVersions)
	v := GetVersion(r, latest)
	if 0 > v || v > latest {
		return nil, errors.ConstructStd(http.StatusBadRequest,
			ErrInvalidTestSequenceVersionTitle, ErrInvalidTestSequenceVersion, r).
			WithLogf("Client sent invalid testsequence version number %d."+
				"Should be in between 1 and %d.", v, latest).
			WithStackTrace(1).
			WithRequestDump(r).
			Finish()
	}
	return &ts.SequenceVersions[latest-v], nil
}
