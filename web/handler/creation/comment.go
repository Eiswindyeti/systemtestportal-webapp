/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// CaseCommentPut returns a function
// which processes a comment posted to a test case
func CaseCommentPut(ca handler.CommentAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tc, err := handler.GetTestCase(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		user, err := handler.GetUser(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		text := r.FormValue(httputil.CommentText)
		c := comment.NewComment(tc.ID(), text, *user)

		if err = ca.Add(&c); err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

// SequenceCommentPut returns a function
// which processes a comment posted to a test sequence
func SequenceCommentPut(ca handler.CommentAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ts, err := handler.GetTestSequence(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		user, err := handler.GetUser(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		text := r.FormValue(httputil.CommentText)
		c := comment.NewComment(ts.ID(), text, *user)

		if err = ca.Add(&c); err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}
