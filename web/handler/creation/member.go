/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

const (
	failedMembership  = "failed membership"
	unableToAddMember = "We were unable to add the requested user as a member to the project."
)

//MemberPut is used to add a existing user to the project
func MemberPut(us middleware.UserRetriever) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		mID := r.FormValue(httputil.Members)
		var x []string
		err = json.Unmarshal([]byte(mID), &x)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		for _, m := range x {
			tid := id.ActorID(m)
			m, ok, err := us.Get(tid)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}

			if !ok {
				errors.ConstructStd(http.StatusInternalServerError,
					failedMembership, unableToAddMember, r).
					WithLogf("Unable to add the user with id %v as a member to the project!", mID).
					WithStackTrace(1).
					WithRequestDump(r).
					Respond(w)
				return
			}
			p.AddMember(m)
		}
	}
}
