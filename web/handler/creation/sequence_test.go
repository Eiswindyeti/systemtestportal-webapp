/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestSequencePost(t *testing.T) {
	params := url.Values{}
	params.Add(httputil.Label, "[]")
	params.Add(httputil.TestSequenceName, "Sequence name")
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("No label",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseGetter := handler.CaseGetterMock{}
				sequenceAdder := handler.SequenceAdderMock{}
				ex := handler.TestExistenceCheckerMock{}
				return SequencePost(&caseGetter, &sequenceAdder, &ex),
					handler.Matches(
						handler.HasStatus(http.StatusBadRequest),
						handler.HasCalls(&sequenceAdder, 0),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTestHandler("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseGetter := handler.CaseGetterMock{}
				sequenceAdder := handler.SequenceAdderMock{}
				ex := handler.TestExistenceCheckerMock{}
				return SequencePost(&caseGetter, &sequenceAdder, &ex),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&sequenceAdder, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodPost, params),
		),
		handler.CreateTestHandler("Successful creation",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				caseGetter := handler.CaseGetterMock{}
				sequenceAdder := handler.SequenceAdderMock{}
				ex := handler.TestExistenceCheckerMock{}
				return SequencePost(&caseGetter, &sequenceAdder, &ex),
					handler.Matches(
						handler.HasStatus(http.StatusSeeOther),
						handler.HasCalls(&sequenceAdder, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, params),
		),
	)
}
