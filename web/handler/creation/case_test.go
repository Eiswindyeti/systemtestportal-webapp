/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCasePost(t *testing.T) {
	body := "{\"inputTestCaseName\":\"Test\"," +
		"\"inputTestCaseDescription\":\"Desc\"," +
		"\"inputTestCasePreconditions\":\"Cond\"," +
		"\"inputTestCaseSUTVariants\":{" +
		"\"Var\":{" +
		"\"Name\":\"Var\"," +
		"\"Versions\":[{\"Name\":\"1.0\"}]}}," +
		"\"inputTestCaseLabels\":[{\"Name\":\"Test\",\"Description\":\"Desc\"}]," +
		"\"inputHours\":1," +
		"\"inputMinutes\":4," +
		"\"inputSteps\":[{\"ID\":0," +
		"\"actual\":\"Step\"," +
		"\"expected\":\"Result\"}]}"

	noStepsBody := "{\"inputTestCaseName\":\"Test\"," +
		"\"inputTestCaseDescription\":\"Desc\"," +
		"\"inputTestCasePreconditions\":\"Cond\"," +
		"\"inputTestCaseSUTVariants\":{" +
		"\"Var\":{" +
		"\"Name\":\"Var\"," +
		"\"Versions\":[{\"Name\":\"1.0\"}]}}," +
		"\"inputTestCaseLabels\":[{\"Name\":\"Test\",\"Description\":\"Desc\"}]," +
		"\"inputHours\":1," +
		"\"inputMinutes\":4," +
		"\"inputSteps\":[]}"

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTestHandler("No project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTestHandler("No test steps",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusSeeOther),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, noStepsBody),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, noStepsBody),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				c := &handler.TestExistenceCheckerMock{}
				return CasePost(a, c), handler.Matches(
					handler.HasStatus(http.StatusSeeOther),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
	)
}
