/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"
	"testing"

	"net/url"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

func TestGroupPost(t *testing.T) {
	params := url.Values{}
	params.Add(httputil.GroupVisibility, visibility.PrivateStr)
	params.Add(httputil.GroupName, "test name")

	handler.Suite(t,
		handler.CreateTestHandler("No visibility",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.GroupAdderMock{}
				c := &handler.ActorExistenceCheckerMock{}
				return GroupPost(a, c), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
					handler.HasCalls(c, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Exists already",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.GroupAdderMock{}
				c := &handler.ActorExistenceCheckerMock{Exst: true}
				return GroupPost(a, c), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
					handler.HasCalls(c, 1),
				)
			},
			handler.SimpleRequest(handler.EmptyCtx, http.MethodGet, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
		handler.CreateTestHandler("Adder returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.GroupAdderMock{Err: handler.ErrTest}
				c := &handler.ActorExistenceCheckerMock{}
				return GroupPost(a, c), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 1),
					handler.HasCalls(c, 1),
				)
			},
			handler.SimpleRequest(handler.EmptyCtx, http.MethodGet, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.GroupAdderMock{}
				c := &handler.ActorExistenceCheckerMock{}
				return GroupPost(a, c), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(a, 1),
					handler.HasCalls(c, 1),
				)
			},
			handler.SimpleRequest(handler.EmptyCtx, http.MethodGet, params),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, params),
		),
	)
}
