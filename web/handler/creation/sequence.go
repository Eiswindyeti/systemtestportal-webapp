/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"
	"strings"

	"encoding/json"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// TestSequenceInput contains general input that can be used
// to create a sequence.
type TestSequenceInput struct {
	InputTestSequenceName,
	InputTestSequenceDescription,
	InputTestSequencePreconditions,
	InputTestSequenceTestCase string
	InputTestSequenceLabels []project.Label
}

// SequencePost handles requests that demand the creation of a new sequence.
func SequencePost(t handler.TestCaseGetter, tsa handler.TestSequenceAdder,
	tec id.TestExistenceChecker) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		input, err := GetTestSequenceInput(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		ts, err := createTestSequence(t, tec, p, input)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		err = tsa.Add(ts)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		httputil.SetHeaderValue(w, httputil.NewName, ts.Name)

		http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
	}
}

// GetTestSequenceInput gets the test sequence input from the request
func GetTestSequenceInput(r *http.Request) (TestSequenceInput, error) {
	labels, err := getTestSequenceLabels(r)
	return TestSequenceInput{
		r.FormValue(httputil.TestSequenceName),
		r.FormValue(httputil.TestSequenceDescription),
		r.FormValue(httputil.TestSequencePreconditions),
		r.FormValue(httputil.TestSequenceTestCase),
		labels,
	}, err
}

const (
	labelInvalidJSONTitle = "Unable to read labels from request."
	labelInvalidJSON      = "The labels of the testsequence couldn't be read from your request. " +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
)

// getTestSequenceLabels gets the labels from the request in json format
// and returns a slice with the labels.
// Returns nil if the json object could not be decoded
func getTestSequenceLabels(r *http.Request) (labels []project.Label, err error) {
	labelString := r.FormValue(httputil.TestSequenceLabels)
	if errJSON := json.Unmarshal([]byte(labelString), &labels); errJSON != nil {
		labels = nil
		err = errors.ConstructStd(http.StatusBadRequest,
			labelInvalidJSONTitle, labelInvalidJSON, r).
			WithLogf("Client send invalid json for labels '%s'.", labelString).
			WithCause(errJSON).
			WithStackTrace(1).
			WithRequestDump(r).
			Finish()
	}
	return labels, err
}

// createTestSequence tries to create a sequence from user input. If it
// it fails an error is returned instead.
func createTestSequence(t handler.TestCaseGetter, sequenceChecker id.TestExistenceChecker, p *project.Project,
	input TestSequenceInput) (*test.Sequence, error) {

	caseNames := strings.Split(input.InputTestSequenceTestCase, "/")
	caseIDs := make([]id.TestID, len(caseNames))
	for i, tcName := range caseNames {
		caseIDs[i] = id.NewTestID(p.ID(), tcName, true)
	}

	tcs, err := handler.GetTestCases(t, caseIDs...)
	if err != nil {
		return nil, err
	}

	ts, err := test.NewTestSequence(
		input.InputTestSequenceName,
		input.InputTestSequenceDescription,
		input.InputTestSequencePreconditions,
		input.InputTestSequenceLabels,
		tcs,
		p.ID(),
	)
	if err != nil {
		return nil, err
	}

	if err := ts.ID().Validate(sequenceChecker); err != nil {
		return nil, err
	}

	return &ts, nil
}
