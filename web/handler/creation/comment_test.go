/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCaseCommentPut(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     &handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CommentAdderMock{}
				return CaseCommentPut(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTestHandler("No logged in user",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CommentAdderMock{}
				return CaseCommentPut(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CommentAdderMock{}
				return CaseCommentPut(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodPut, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodPut, handler.NoParams),
		),
	)
}

func TestSequenceCommentPut(t *testing.T) {
	params := url.Values{}
	params.Add(httputil.NewTestCases, "testcaseID1/testcaseID2/")
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:         &handler.DummyUser,
			middleware.TestSequenceKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:         &handler.DummyUser,
			middleware.TestSequenceKey: handler.DummyTestSequence,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CommentAdderMock{}
				return SequenceCommentPut(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodPut, params),
		),
		handler.CreateTestHandler("Normal case no ids",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CommentAdderMock{}
				return SequenceCommentPut(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CommentAdderMock{}
				return SequenceCommentPut(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPut, params),
		),
	)
}
