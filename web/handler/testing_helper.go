/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

/*
This file contains utilities for testing that are used throughout
the handler package.
*/

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bytes"
	"context"
	"io/ioutil"
	"log"
	"net/url"
	"time"

	"strings"

	"errors"

	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// ResponseMatcher is used to check a condition on a response that
// a handler gave.
type ResponseMatcher func(t *testing.T, r *http.Response)

// TestHandler is a simple test for a handler.
type TestHandler interface {
	// Name returned the name of the test.
	Name() string
	// Tested returns the handler that will be tested.
	Tested() http.HandlerFunc
	// Requests returns the request that will be send to the
	// handler during the test.
	Requests() []*http.Request
	// Checks returns a number of checks that need to be performed
	// on the response the handler gave during the test.
	Checks() []ResponseMatcher
	// SetUp prepared the test.
	SetUp()
}

type handlerTest struct {
	name    string
	request []*http.Request
	tested  http.HandlerFunc
	checks  []ResponseMatcher
	TestSetUp
}

// Name returned the name of the test.
func (t *handlerTest) Name() string {
	return t.name
}

// Tested returns the handler that will be tested.
func (t *handlerTest) Tested() http.HandlerFunc {
	return t.tested
}

// Requests returns the requests that will be send to the
// handler during the test.
func (t *handlerTest) Requests() []*http.Request {
	return t.request
}

// Checks returns a number of checks that need to be performed
// on the response the handler gave during the test.
func (t *handlerTest) Checks() []ResponseMatcher {
	return t.checks
}

// SetUp prepared the test.
func (t *handlerTest) SetUp() {
	tested, checks := t.TestSetUp()
	t.tested = tested
	t.checks = checks
}

// TestSetUp sets up a test by creating the tested handler and
// matchers that need to be fulfilled by the response the handler
// gives.
type TestSetUp func() (http.HandlerFunc, []ResponseMatcher)

// CreateTestHandler is used to create a simple test for a handler.
func CreateTestHandler(name string, setUp TestSetUp, rs ...*http.Request) TestHandler {
	return &handlerTest{name, rs, nil, nil, setUp}
}

// ExpectResponse is a simple setup method that can be used if there is no shared
// data between the matchers and the handler itself.
func ExpectResponse(tested http.HandlerFunc, matchers ...ResponseMatcher) TestSetUp {
	return func() (http.HandlerFunc, []ResponseMatcher) {
		return tested, matchers
	}
}

// Suite runs several handlers test as sub tests of the current test.
func Suite(t *testing.T, tests ...TestHandler) {
	for _, test := range tests {
		t.Run(test.Name(), func(t *testing.T) {
			rs := test.Requests()
			for _, r := range rs {
				test.SetUp()
				AssertResponse(t, test.Tested(), r, test.Checks()...)
				if t.Failed() && len(rs) > 1 {
					t.Logf("Tested with request: \n%s\n", httputil.DumpRequest(r))
				}
			}
		})
	}
}

// Matches is a utility method to get a ResponseMatcher array the easy way.
func Matches(matchers ...ResponseMatcher) []ResponseMatcher {
	return matchers
}

// AssertResponse sends given request to given tested handler and runs given
// ResponseMatchers on it.
func AssertResponse(t *testing.T, tested http.HandlerFunc, r *http.Request,
	matchers ...ResponseMatcher) {
	w := httptest.NewRecorder()
	logger := bytes.NewBufferString("")
	log.SetOutput(logger)

	tested(w, r)

	for _, matcher := range matchers {
		matcher(t, w.Result())
	}

	if t.Failed() {
		t.Errorf(
			"Log output was:\n"+
				"---- Begin ----\n"+
				"%s\n"+
				"---- End ----",
			logger.String(),
		)
	}
}

// CallerMock mocks the Caller
type CallerMock struct {
	Called int
}

// Calls returns the number of calls made to the mock.
func (m *CallerMock) Calls() int {
	return m.Called
}

// CallerCounterMock is an interface for the CallerMock
// to count the calls made to the mock
type CallerCounterMock interface {
	// Calls returns the number of calls made to the mock.
	Calls() int
}

// HasCalls is a simple matcher that checks whether given mock has
// a certain number of calls.
func HasCalls(mock CallerCounterMock, number int) ResponseMatcher {
	return func(t *testing.T, r *http.Response) {
		if mock.Calls() != number {
			t.Errorf(
				"Error for mock: %T\nExpected that "+
					"mock is called %d times. \nBut was called %d times.",
				mock,
				number,
				mock.Calls(),
			)
		}
	}
}

// HasBody is a matcher that test if the response has given body or not.
// Whitespace will be trimmed from the body and given string.
func HasBody(expectedBody string) ResponseMatcher {
	return func(t *testing.T, r *http.Response) {
		defer func() {
			err := r.Body.Close()
			if err != nil {
				t.Errorf("Body could not be closed. \n Respone:\n%s", httputil.DumpResponse(r))
			}
		}()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			t.Errorf("Body couldn't be read from response: \n%s", httputil.DumpResponse(r))
		}
		b := strings.TrimSpace(string(body))
		eb := strings.TrimSpace(expectedBody)
		if b != eb {
			t.Errorf(
				"Handler returned wrong response body.\n"+
					"Expected: \n%q\n"+
					"But got: \n%q\n",
				eb,
				b,
			)
		}
	}
}

// HasStatus is a simple matcher that checks whether
// teh response has given status code.
func HasStatus(expectedStatus int) ResponseMatcher {
	return func(t *testing.T, r *http.Response) {
		if r.StatusCode != expectedStatus {
			t.Errorf(
				"Handler returned wrong status code.\n"+
					"Expected: %d(%s)\n"+
					"But got: %d(%s)",
				expectedStatus,
				http.StatusText(expectedStatus),
				r.StatusCode,
				http.StatusText(r.StatusCode),
			)
		}
	}
}

// EmptyCtx is an empty context for testing purpses
var EmptyCtx = &testContext{make(map[interface{}]interface{})}

type testContext struct {
	values map[interface{}]interface{}
}

func (c *testContext) Deadline() (deadline time.Time, ok bool) {
	return
}

func (c *testContext) Done() <-chan struct{} {
	return nil
}

func (c *testContext) Err() error {
	return nil
}

func (c *testContext) Value(key interface{}) interface{} {
	return c.values[key]
}

// SimpleContext returns a context made of given map. It is a very simple
// implementation of a context.Context.
func SimpleContext(values map[interface{}]interface{}) context.Context {
	return &testContext{values}
}

// EmptyRequest creates a simple test request with no parameters and
// no context attached to it.
func EmptyRequest(method string) *http.Request {
	return SimpleRequest(EmptyCtx, method, NoParams)

}

// NoParams is simply an empty set of parameters for a request.
var NoParams = url.Values{}

// SimpleRequest is used to create a simple request for testing.
func SimpleRequest(ctx context.Context, method string, params url.Values) *http.Request {
	return NewRequest(ctx, method, params, "")
}

// SimpleFragmentRequest is used to create a simple request for testing.
func SimpleFragmentRequest(ctx context.Context, method string, params url.Values) *http.Request {
	return NewFragmentRequest(ctx, method, params, "")
}

// NewRequest is used to create a request for testing.
func NewRequest(ctx context.Context, method string, params url.Values, body string) *http.Request {
	r := httptest.NewRequest(method, "/test", bytes.NewBufferString(body))
	if ctx != nil {
		r = r.WithContext(ctx)
	}
	r.Form = params
	return r
}

// NewFragmentRequest is used to create a request for testing.
func NewFragmentRequest(ctx context.Context, method string, params url.Values, body string) *http.Request {
	p := url.Values{}
	for k := range params {
		p.Add(k, params.Get(k))
	}
	p.Add(httputil.Fragment, "true")
	return NewRequest(ctx, method, p, body)
}

// ErrTest is an error that can be used if your test needs to
var ErrTest = errors.New("error was injected by a test to check reaction")

/*

// TestTestHandlerBadRequest checks if unsupported requests are handled correctly. The unsupported request is
// an OPTION request
func testBadRequest(t *testing.T, url string) {
	req, err := http.NewRequest(http.MethodOptions, url, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()

	hut := http.HandlerFunc(ProjectHandler)
	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusMethodNotAllowed {
		t.Errorf("%s returned wrong status code: got %v want %v", url, status, http.StatusMethodNotAllowed)
	}
}

func testGetRequestTest(t *testing.T, testType, testName string) {
	p := DummyProject

	urls := []struct {
		url        string
		statusWant int
	}{
		{"/user/" + p.ID + "/" + testType + "/", http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/" + testName, http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/----", http.StatusNotFound},
		{"/user/" + p.ID + "/" + testType + "/" + action.New, http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/" + testName + "/" + action.Edit, http.StatusOK},
		{"/user/" + p.ID + "/" + testType + "/NO_TEST_ID/" + action.Edit, http.StatusNotFound},
		{"/user/" + p.ID + "/" + testType + "/" + testName + "/" + action.History, http.StatusOK},
	}
	for _, caseURL := range urls {
		getRequest(t, caseURL.url, caseURL.statusWant, false, ProjectHandler)
		getRequest(t, caseURL.url, caseURL.statusWant, true, ProjectHandler)
	}
}

// getRequest creates a GET request to the given path and passes it to the given handler. Depending on the isFrag
// parameter it creates a fragment request and passes the request to the given handler
func getRequest(t *testing.T, path string, statusWant int, isFrag bool, handler http.HandlerFunc) {

	req, err := http.NewRequest(http.MethodGet, path, nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(handler)

	if isFrag {
		form := url.Values{}
		form.Add("fragment", "true")
		req.Form = form
	}

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("%s returned wrong status code: got %v want %v", path, status, statusWant)
	}
}

func testTestDeleteRequest(p *project.Project, testType, name string, statusWant int) func(*testing.T) {
	return func(t *testing.T) {
		testURL := "/user/" + p.ID + "/" + testType + "/" + name + "/"

		req, err := http.NewRequest(http.MethodDelete, testURL, nil)
		if err != nil {
			t.Fatal(err)
		}

		rr := httptest.NewRecorder()

		hut := http.HandlerFunc(ProjectHandler)
		hut.ServeHTTP(rr, req)

		if status := rr.Code; status != statusWant {
			t.Errorf("%s returned wrong status code: got %v want %v", testURL, status, statusWant)
		}

		switch testType {
		case TestCases:
			if _, ok, _ := testStore.GetCaseStore().Get(id.NewTestID(p.ID(), name)); ok {
				t.Errorf("test case %s was not deleted", name)
			}
		case TestSequences:
			if _, ok, _ := testStore.GetSequenceStore().Get(id.NewTestID(p.ID(), name)); ok {
				t.Errorf("test sequence %s was not deleted", name)
			}
		}
	}
}

// testPostRequestEmpty tests an empty post request with the given url
// Useful for testing post requests on wrong urls
func testPostRequestEmpty(t *testing.T, url string, statusWant int) {

	req, err := http.NewRequest(http.MethodPost, url, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	hut := http.HandlerFunc(ProjectHandler)

	hut.ServeHTTP(rr, req)

	if status := rr.Code; status != statusWant {
		t.Errorf("%s returned wrong status code: got %v want %v", url, status, statusWant)
	}

}
*/
