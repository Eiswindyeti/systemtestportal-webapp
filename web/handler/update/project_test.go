/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestProjectSettingsPost(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)
	params := url.Values{}
	params.Add(httputil.ProjectVisibility, visibility.PublicStr)
	params.Add(httputil.ProjectName, "New Project Name")

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				e := &handler.ProjectExistenceMock{}
				return ProjectPost(a, e), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
					//	HasCalls(e, 0),
				)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, params),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, params),
		),
		handler.CreateTestHandler("Add returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{Err: handler.ErrTest}
				e := &handler.ProjectExistenceMock{}
				return ProjectPost(a, e), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 1),
					//	HasCalls(e, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, params),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, params),
		),
		handler.CreateTestHandler("Invalid visibility",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				e := &handler.ProjectExistenceMock{}
				return ProjectPost(a, e), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
					//	HasCalls(e, 0),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Existence checker returns false",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				e := &handler.ProjectExistenceMock{Exst: true}
				return ProjectPost(a, e), handler.Matches(
				//	HasStatus(http.StatusBadRequest),
				//	HasCalls(a, 0),
				//	HasCalls(e, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, params),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, params),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				e := &handler.ProjectExistenceMock{}
				return ProjectPost(a, e), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(a, 1),
					//	HasCalls(e, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, params),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, params),
		),
	)
}
