/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCasePut(t *testing.T) {
	body := "{" +
		"\"isMinor\":false," +
		"\"inputCommitMessage\":\"Commit Msg\"," +
		"\"data\":{" +
		"\"inputTestCaseName\":\"Test\"," +
		"\"inputTestCaseDescription\":\"Desc\"," +
		"\"inputTestCasePreconditions\":\"Cond\"," +
		"\"inputTestCaseSUTVariants\":{\"Var\":{\"Name\":\"Var\",\"Versions\":[{\"Name\":\"1.0\"}]}}," +
		"\"inputTestCaseLabels\":[]," +
		"\"inputHours\":1," +
		"\"inputMinutes\":4," +
		"\"inputSteps\":[{\"ID\":0,\"actual\":\"Step\",\"expected\":\"Result\"}]" +
		"}" +
		"}"

	noStepsBody := "{" +
		"\"isMinor\":false," +
		"\"inputCommitMessage\":\"Commit Msg\"," +
		"\"data\":{" +
		"\"inputTestCaseName\":\"Test\"," +
		"\"inputTestCaseDescription\":\"Desc\"," +
		"\"inputTestCasePreconditions\":\"Cond\"," +
		"\"inputTestCaseSUTVariants\":{\"Var\":{\"Name\":\"Var\",\"Versions\":[{\"Name\":\"1.0\"}]}}," +
		"\"inputTestCaseLabels\":[]," +
		"\"inputHours\":1," +
		"\"inputMinutes\":4," +
		"\"inputSteps\":[]" +
		"}" +
		"}"

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: nil,
			middleware.ProjectKey:  handler.DummyProject,
		},
	)
	ctx := map[interface{}]interface{}{
		middleware.TestCaseKey: handler.DummyTestCase,
		middleware.ProjectKey:  handler.DummyProject,
	}

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				u := &handler.CaseUpdaterMock{}
				c := &handler.TestExistenceCheckerMock{}
				r := &handler.CaseProtocolRenamerMock{}
				return CasePut(u, c, r), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(&u.CaseAdderMock, 0),
					handler.HasCalls(&u.CaseDeleterMock, 0),
					handler.HasCalls(&u.CaseRenamerMock, 0),
					handler.HasCalls(r, 0),
				)
			},
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTestHandler("No project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				u := &handler.CaseUpdaterMock{}
				c := &handler.TestExistenceCheckerMock{}
				r := &handler.CaseProtocolRenamerMock{}
				return CasePut(u, c, r), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(&u.CaseAdderMock, 0),
					handler.HasCalls(&u.CaseDeleterMock, 0),
					handler.HasCalls(&u.CaseRenamerMock, 0),
					handler.HasCalls(r, 0),
				)
			},
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTestHandler("No test steps",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				u := &handler.CaseUpdaterMock{}
				c := &handler.TestExistenceCheckerMock{}
				r := &handler.CaseProtocolRenamerMock{}
				return CasePut(u, c, r), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(&u.CaseAdderMock, 1),
					handler.HasCalls(&u.CaseDeleterMock, 0),
					handler.HasCalls(&u.CaseRenamerMock, 1),
					handler.HasCalls(r, 1),
				)
			},
			handler.NewRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, noStepsBody),
			handler.NewFragmentRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, noStepsBody),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				u := &handler.CaseUpdaterMock{}
				c := &handler.TestExistenceCheckerMock{}
				r := &handler.CaseProtocolRenamerMock{}
				return CasePut(u, c, r), handler.Matches(
					handler.HasStatus(http.StatusCreated),
					handler.HasCalls(&u.CaseAdderMock, 1),
					handler.HasCalls(&u.CaseDeleterMock, 0),
					handler.HasCalls(&u.CaseRenamerMock, 1),
					handler.HasCalls(r, 1),
				)
			},
			handler.NewRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, body),
			handler.NewFragmentRequest(handler.SimpleContext(ctx), http.MethodPut, handler.NoParams, body),
		),
	)
}
