/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"net/url"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

type sequenceAdderMockMatcher func(*testing.T, *handler.SequenceAdderMock)

func sequenceAdder(m *handler.SequenceAdderMock, matchers ...sequenceAdderMockMatcher) handler.ResponseMatcher {
	return func(t *testing.T, r *http.Response) {
		for _, matcher := range matchers {
			matcher(t, m)
		}
	}
}

func hasLatestSequenceVersion(e test.SequenceVersion) sequenceAdderMockMatcher {
	return func(t *testing.T, mock *handler.SequenceAdderMock) {
		a := mock.TestSequence.SequenceVersions[0]
		if cmp.Equal(e, a, cmpopts.IgnoreUnexported(id.ProjectID{}, id.TestID{})) {
			t.Errorf(
				"Unequal sequence version.\n"+
					"Expected: \n%+v\n"+
					"But got: \n%+v\n",
				e,
				a,
			)
		}
	}
}

func hasSequenceName(e string) sequenceAdderMockMatcher {
	return func(t *testing.T, mock *handler.SequenceAdderMock) {
		a := mock.TestSequence.Name
		if e != a {
			t.Errorf(
				"Unequal sequence version.\n"+
					"Expected: \n%q\n"+
					"But got: \n%q\n",
				e,
				a,
			)
		}
	}
}

func TestSequencePut(t *testing.T) {
	params := url.Values{}
	params.Add(httputil.CommitMessage, "Msg")
	params.Add(httputil.TestSequenceDescription, "Desc")
	params.Add(httputil.TestSequenceLabels, "[]")
	params.Add(httputil.TestSequenceName, handler.DummyTestSequence.Name)
	params.Add(httputil.TestSequencePreconditions, "Cond")
	params.Add(httputil.IsMinor, "true")
	params.Add(httputil.TestSequenceTestCase, "")

	paramsNameChange := url.Values{}
	paramsNameChange.Add(httputil.CommitMessage, "Msg")
	paramsNameChange.Add(httputil.TestSequenceDescription, "Desc")
	paramsNameChange.Add(httputil.TestSequenceLabels, "[]")
	paramsNameChange.Add(httputil.TestSequenceName, "new name")
	paramsNameChange.Add(httputil.TestSequencePreconditions, "Cond")
	paramsNameChange.Add(httputil.IsMinor, "true")
	paramsNameChange.Add(httputil.TestSequenceTestCase, "")

	expectedVr, _ := test.NewTestSequenceVersion(
		len(handler.DummyTestSequence.SequenceVersions)+1,
		true,
		"Msg",
		"Desc",
		"Cond",
		handler.DummyTestSequence.SequenceVersions[0].Cases,
		handler.DummyTestSequence.ID(),
	)
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:         &handler.DummyUser,
			middleware.TestSequenceKey: nil,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:         &handler.DummyUser,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cg := &handler.CaseGetterMock{}
				ex := &handler.TestExistenceCheckerMock{}
				su := &handler.SequenceUpdaterMock{}
				return SequencePut(cg, su, ex, nil),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(cg, 0),
						handler.HasCalls(&su.SequenceAdderMock, 0),
						handler.HasCalls(&su.SequenceDeletionMock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodPut, params),
		),
		handler.CreateTestHandler("No parameters",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cg := &handler.CaseGetterMock{}
				ex := &handler.TestExistenceCheckerMock{}
				su := &handler.SequenceUpdaterMock{}
				return SequencePut(cg, su, ex, nil),
					handler.Matches(
						handler.HasStatus(http.StatusBadRequest),
						handler.HasCalls(cg, 0),
						handler.HasCalls(&su.SequenceAdderMock, 0),
						handler.HasCalls(&su.SequenceDeletionMock, 0),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPut, handler.NoParams),
		),
		handler.CreateTestHandler("With update parameters",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cg := &handler.CaseGetterMock{}
				ex := &handler.TestExistenceCheckerMock{}
				su := &handler.SequenceUpdaterMock{}
				return SequencePut(cg, su, ex, nil),
					handler.Matches(
						handler.HasStatus(http.StatusCreated),
						handler.HasCalls(cg, 0),
						handler.HasCalls(&su.SequenceAdderMock, 1),
						handler.HasCalls(&su.SequenceDeletionMock, 0),
						sequenceAdder(&su.SequenceAdderMock, hasLatestSequenceVersion(expectedVr)),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPut, params),
		),
		handler.CreateTestHandler("With new name",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cg := &handler.CaseGetterMock{}
				ex := &handler.TestExistenceCheckerMock{}
				su := &handler.SequenceUpdaterMock{}
				ps := &handler.SequenceRenameHandlerMock{}
				return SequencePut(cg, su, ex, ps),
					handler.Matches(
						handler.HasStatus(http.StatusCreated),
						handler.HasCalls(cg, 0),
						handler.HasCalls(&su.SequenceAdderMock, 1),
						handler.HasCalls(&su.SequenceRenamerMock, 1),
						handler.HasCalls(ps, 1),
						sequenceAdder(&su.SequenceAdderMock, hasLatestSequenceVersion(expectedVr)),
						sequenceAdder(&su.SequenceAdderMock, hasSequenceName("new name")),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPut, paramsNameChange),
		),
	)
}
