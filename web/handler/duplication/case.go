/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package duplication

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// CasePost handles requests for duplicating testcases.
func CasePost(ta handler.TestCaseAdder, caseChecker id.TestExistenceChecker) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tc, err := handler.GetTestCase(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		dupTc := *tc
		newName := r.FormValue(httputil.TestCaseName)
		if newName != tc.Name {
			dupTc.Rename(newName)
		}

		if err = dupTc.ID().Validate(caseChecker); err != nil {
			errors.Handle(err, w, r)
			return
		}

		if err = cutNewerVersions(&dupTc, getFormValueVersion(r)); err != nil {
			errors.Handle(err, w, r)
			return
		}

		err = ta.Add(&dupTc)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		httputil.SetHeaderValue(w, httputil.NewName, dupTc.ItemName())
		w.WriteHeader(http.StatusCreated)

		display.ShowCase(p, &dupTc, nil, w, r)
	}
}

// cutNewerVersions cuts away the versions that are more recent than
// the given version (to). If the version (to) was outside of the
// possible range an error is returned.
func cutNewerVersions(tc *test.Case, to int) error {
	latest := len(tc.TestCaseVersions)
	ver := to
	if ver <= 0 || ver > latest {
		return handler.InvalidTCVersion()
	}
	// Versions are listed backwards
	verIndex := latest - ver
	// Copy current testcase versions and slice afterwards
	tcv := tc.TestCaseVersions
	tc.TestCaseVersions = tcv[verIndex:]
	return nil
}

// getFormValueVersion gets the version from the request.
// Returns 0 if the version is empty.
// Returns -1 if the version is not an integer.
func getFormValueVersion(r *http.Request) int {
	return handler.GetVersion(r, 0)
}
