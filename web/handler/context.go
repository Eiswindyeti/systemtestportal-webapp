/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

const (
	errNoContainerTitle = "Can't resolve container."
	errNoContainer      = "We are sorry, but we were unable to resolve the container " +
		"(user or group) for the url you requested. This is most likely a bug. " +
		"Please contact us via our " + IssueTracker + "."
	errNoProjectTitle = "Can't resolve project."
	errNoProject      = "We are sorry, but we were unable to resolve the project " +
		"for the url you requested. This is most likely a bug. " +
		"Please contact us via our " + IssueTracker + "."
	errNoTestCaseTitle = "Can't resolve testcase."
	errNoTestCase      = "We are sorry, but we were unable to resolve the testcase " +
		"for the url you requested. This is most likely a bug. " +
		"Please contact us via our " + IssueTracker + "."
	errNoTestSequenceTitle = "Can't resolve testsequence."
	errNoTestSequence      = "We are sorry, but we were unable to resolve the testsequence " +
		"for the url you requested. This is most likely a bug. " +
		"Please contact us via our " + IssueTracker + "."
	errNoUserTitle = "Can't detect currently signed in user."
	errNoUser      = "We are sorry, but we were unable to resolve the signed in user " +
		"from your request. This is most likely a bug. " +
		"Please contact us via our " + IssueTracker + "."
)

// getValue gets a value with given key from given request.
func getValue(r *http.Request, key interface{}) interface{} {
	return r.Context().Value(key)
}

// GetProject gets the project from given request if it
// was stored there by middleware. If the project is nil
// or not contained in the request an error will be returned
// and the returned project will be nil.
func GetProject(r *http.Request) (*project.Project, error) {
	v := getValue(r, middleware.ProjectKey)
	p, ok := v.(*project.Project)
	if ok && p != nil {
		return p, nil
	}
	return nil, errors.ConstructStd(http.StatusInternalServerError,
		errNoProjectTitle, errNoProject, r).
		WithLog("Unable to get project from request.\n" +
			"This means the project middleware is either not " +
			"working or not registered on the route.").
		WithStackTrace(2).
		WithRequestDump(r).
		Finish()
}

// GetTestCase gets the testcase from given request if it
// was stored there by middleware. If the testcase is nil
// or not contained in the request an error will be returned
// and the returned testcase will be nil.
func GetTestCase(r *http.Request) (*test.Case, error) {
	v := getValue(r, middleware.TestCaseKey)
	tc, ok := v.(*test.Case)
	if ok && tc != nil {
		return tc, nil
	}
	return nil, errors.ConstructStd(http.StatusInternalServerError,
		errNoTestCaseTitle, errNoTestCase, r).
		WithLog("Unable to get testcase from request.\n" +
			"This means the testcase middleware is either not " +
			"working or not registered on the route.").
		WithStackTrace(2).
		WithRequestDump(r).
		Finish()
}

// GetTestSequence gets the testsequence from given request if it
// was stored there by middleware. If the testsequence is nil
// or not contained in the request an error will be returned
// and the returned testsequence will be nil.
func GetTestSequence(r *http.Request) (*test.Sequence, error) {
	v := getValue(r, middleware.TestSequenceKey)
	ts, ok := v.(*test.Sequence)
	if ok && ts != nil {
		return ts, nil
	}
	return nil, errors.
		ConstructStd(http.StatusInternalServerError, errNoTestSequenceTitle, errNoTestSequence, r).
		WithLog(fmt.Sprintf("Unable to get testsequence from request.\n"+
			"This means the testsequence middleware is either not "+
			"working or not registered on the route.\n"+
			"Request was:\n%s", httputil.DumpRequest(r))).
		WithStackTrace(2).
		Finish()
}

// GetUser gets the currently signed in user from given
// request if it was stored there by middleware. If the user
// is not contained in the request an error will be
// returned and the returned user will be nil.
// If no user is signed in the user will be nil as well as the
// returned error.
func GetUser(r *http.Request) (*user.User, error) {
	v := getValue(r, middleware.UserKey)
	u, ok := v.(*user.User)
	if ok {
		return u, nil
	}
	return nil, errors.ConstructStd(http.StatusInternalServerError,
		errNoUserTitle, errNoUser, r).
		WithLog("Unable to get signed in user " +
			"from request.\n" +
			"This means the auth middleware is either not " +
			"working or not registered on the route.").
		WithStackTrace(2).
		WithRequestDump(r).
		Finish()
}

// GetContainer gets the container (user or group) from given
// request if it was stored there by middleware. If the container
// is nil or not contained in the request an error will be returned
// and the returned container will be nil.
func GetContainer(r *http.Request) (*group.Group, *user.User, error) {
	v := getValue(r, middleware.ContainerKey)
	switch container := v.(type) {
	case *group.Group:
		return container, nil, nil
	case *user.User:
		return nil, container, nil
	default:
		return nil, nil, errors.ConstructStd(http.StatusInternalServerError,
			errNoContainerTitle, errNoContainer, r).
			WithLog("Unable to get container(user or group) " +
				"from request.\n" +
				"This means the container middleware is either not " +
				"working or not registered on the route.").
			WithStackTrace(2).
			WithRequestDump(r).
			Finish()
	}
}

// GetContainerID gets the id of the container (user or group) from given
// request if it was stored there by middleware. If the container
// is nil or not contained in the request an error will be returned
// and the returned container id will be an empty id.
func GetContainerID(r *http.Request) (id.ActorID, error) {
	g, u, err := GetContainer(r)
	if err != nil {
		return "", err
	}
	if g != nil {
		return g.ID(), nil
	}
	if u != nil {
		return u.ID(), nil
	}
	return "", err
}

// GetComments gets the comments from a given request if they
// were stored there by middleware. If the comment slice is nil
// or not contained in the request, nil will be returned.
func GetComments(r *http.Request) []*comment.Comment {
	v := getValue(r, middleware.CommentsKey)
	c, ok := v.([]*comment.Comment)
	if ok && c != nil {
		return c
	}
	return nil
}
