/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCaseGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	tested := CaseGet
	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Invalid context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestCasesGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseListerMock{}
				return CasesGet(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("No project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseListerMock{}
				return CasesGet(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(m, 0),
				)
			},
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseListerMock{}
				return CasesGet(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(m, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
