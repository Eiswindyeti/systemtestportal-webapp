/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCaseLabelsGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	tested := CaseLabelsGet
	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestCaseLabelsPost(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	body := "[]"

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				return CaseLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(invalidCtx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTestHandler("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				return CaseLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, ""),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, ""),
		),
		handler.CreateTestHandler("Adder returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{Err: handler.ErrTest}
				return CaseLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.CaseAdderMock{}
				return CaseLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
	)
}

func TestSequenceLabelsGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
		},
	)

	tested := SequenceLabelsGet
	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestSequenceLabelsPost(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
		},
	)
	body := "[]"

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.SequenceAdderMock{}
				return SequenceLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(invalidCtx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTestHandler("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.SequenceAdderMock{}
				return SequenceLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, ""),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, ""),
		),
		handler.CreateTestHandler("Adder returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.SequenceAdderMock{Err: handler.ErrTest}
				return SequenceLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.SequenceAdderMock{}
				return SequenceLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
	)
}

func TestProjectLabelsGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	tested := ProjectLabelsGet
	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestProjectLabelsPost(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)
	body := "[]"

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				return ProjectLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(invalidCtx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTestHandler("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				return ProjectLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(a, 0),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, ""),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, ""),
		),
		handler.CreateTestHandler("Adder returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{Err: handler.ErrTest}
				return ProjectLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				a := &handler.ProjectAdderMock{}
				return ProjectLabelsPost(a), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(a, 1),
				)
			},
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
	)
}
