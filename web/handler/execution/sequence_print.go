/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// sequenceExecutionPrinter is used to print the pages for a testsequence execution.
type sequenceExecutionPrinter struct {
	SequenceSessionGetter
	CaseProtocolGetter
	TimeSession
}

// printStartPage prints the start page for the execution of a testsequence.
func (printer *sequenceExecutionPrinter) printStartPage(w http.ResponseWriter, r *http.Request,
	tsv test.SequenceVersion) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ts, err := handler.GetTestSequence(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionStartPage)
	min, hours := tsv.ItemEstimatedDuration()
	time := duration.Duration{}

	progressTotal := totalWorkSteps(tsv)
	progressPercent := int(1 / float64(progressTotal) * 100.0)

	ctx := context.New().
		With(keyIsSeq, true).
		With(keyIsInSeq, true).
		With(keyCaseNr, 0).
		With(keyProject, p).
		With(keyTestObject, ts).
		With(keyTestObjectVersion, tsv).
		With(keyProgress, 1).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyEstimatedMinutes, min).
		With(keyEstimatedHours, hours).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute())
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// printSummaryPage prints a summary page for the execution of a testsequence.
func (printer *sequenceExecutionPrinter) printSummaryPage(w http.ResponseWriter, r *http.Request,
	tsv test.SequenceVersion) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ts, err := handler.GetTestSequence(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionSummary)
	seqPrt, _ := printer.GetCurrentSequenceProtocol(r)
	if seqPrt == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadSummaryPage, r).
			WithLog("Error while trying to get current protocol.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}
	time, _ := printer.GetDuration(r)
	if time == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadTime, r).
			WithLog("Error while trying to get needed time.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	type caseSummary struct {
		Result     test.Result
		Name       string
		Comment    string
		NeededTime duration.Duration
	}
	var cases []caseSummary
	for i := 0; i < len(tsv.Cases); i++ {
		casePrt, err := printer.GetCaseExecutionProtocol(seqPrt.CaseExecutionProtocols[i])
		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToLoadSummaryPage, r).
				WithLog("Error while trying to get protocol.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}
		cases = append(cases, caseSummary{
			Result:     casePrt.Result,
			Name:       tsv.Cases[i].Name,
			Comment:    casePrt.Comment,
			NeededTime: casePrt.GetNeededTime(),
		})
	}
	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, true).
		With(keyIsInSeq, true).
		With(keyProject, p).
		With(keyTestObjectVersion, tsv).
		With(keyTestObject, ts).
		With(keyAllSubObjects, cases).
		With(keyCaseNr, 0).
		With(keyStepNr, 1).
		With(keyProgress, totalWorkSteps(tsv)).
		With(keyProgressTotal, totalWorkSteps(tsv)).
		With(keyProgressPercent, 100).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute())
	handler.PrintTmpl(ctx, tmpl, w, r)
}
