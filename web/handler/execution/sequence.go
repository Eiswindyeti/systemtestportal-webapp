/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// SequenceSession is the session that stores sequence data during execution of
// a testsequence.
type SequenceSession interface {
	SequenceSessionSetter
	//RemoveCurrentSequenceProtocol removes the current sequence protocol from the session.
	RemoveCurrentSequenceProtocol(w http.ResponseWriter, r *http.Request) error
	SequenceSessionGetter
	TimeSession
}

// SequenceSessionGetter interface for retrieving testsequence protocols.
type SequenceSessionGetter interface {
	//GetCurrentSequenceProtocol returns the protocol to the currently running sequence execution to the given request.
	//If there is no sequence execution running, the function will return nil, nil
	//If an error occurs nil and the error will be returned
	GetCurrentSequenceProtocol(r *http.Request) (*test.SequenceExecutionProtocol, error)
}

// SequenceSessionSetter is used to set the managed testsequence protocol during requests.
type SequenceSessionSetter interface {
	//SetCurrentSequenceProtocol saves the given protocol to the session.
	//After this call, you can get the current sequence protocol via the GetCurrentSequenceProtocol-function
	SetCurrentSequenceProtocol(w http.ResponseWriter, r *http.Request,
		protocol *test.SequenceExecutionProtocol) error
}

// SequenceProtocolStore interface for storing testsequence protocols.
type SequenceProtocolStore interface {
	SequenceProtocolAdder
	// GetSequenceExecutionProtocols gets the protocols for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocols(projectID, sequenceID string) ([]test.SequenceExecutionProtocol, error)
	// GetSequenceExecutionProtocol gets the protocol with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocol(projectID, sequenceID string, protocolID int) (test.SequenceExecutionProtocol, error)
}

// SequenceProtocolAdder interface for storing testsequence protocols.
type SequenceProtocolAdder interface {
	// AddSequenceProtocol adds the given protocol to the store
	AddSequenceProtocol(r *test.SequenceExecutionProtocol) (err error)
}

// SequenceStarPageGet serves the start page for a testsequence execution.
func SequenceStarPageGet(time TimeSession, caseGetter CaseProtocolGetter,
	getter SequenceSessionGetter) http.HandlerFunc {
	printer := &sequenceExecutionPrinter{getter, caseGetter, time}
	return func(w http.ResponseWriter, r *http.Request) {
		ts, err := handler.GetTestSequence(r)
		if err != nil {
			errors.Handle(err, w, r)
		}
		tsv, err := handler.GetTestSequenceVersion(r, ts)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		printer.printStartPage(w, r, *tsv)
	}
}

// testSequencePageHandler is the type of the sub handlers that are used during the execution
// of a sequence.
type testSequencePageHandler func(w http.ResponseWriter, r *http.Request,
	ts test.Sequence, tsv test.SequenceVersion)

// testSequenceExecutionPost is used to handle all requests during the execution of
// a sequence.
type testSequenceExecutionPost struct {
	Start    testSequencePageHandler
	Summary  testSequencePageHandler
	TestCase http.Handler
}

// ServeHTTP delegates a request to the different sub handlers based on where the user
// currently is in the process of a sequence execution.
func (t *testSequenceExecutionPost) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ts, err := handler.GetTestSequence(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	tsv, err := handler.GetTestSequenceVersion(r, ts)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	determineNextExecStep(w, r, t, ts, tsv)
}

// determineNextExecStep determines which sub handler to
// call depending on the current process of the user.
func determineNextExecStep(w http.ResponseWriter, r *http.Request, t *testSequenceExecutionPost,
	ts *test.Sequence, tsv *test.SequenceVersion) {

	caseNr := getFormValueInt(r, keyCaseNr)
	stepNr := getFormValueInt(r, keyStepNr)
	caseCount := len(tsv.Cases)

	if caseNr > 0 && caseNr <= caseCount {
		middleware.AddToContext(r, middleware.TestCaseKey, &tsv.Cases[caseNr-1])
		t.TestCase.ServeHTTP(w, r)
		return
	}

	switch {
	case caseNr == 0 && stepNr == 0:
		t.Start(w, r, *ts, *tsv)
	case caseNr == 0 && stepNr == 1:
		// Execution finished showing sequence
		http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
	case caseNr == caseCount+1:
		t.Summary(w, r, *ts, *tsv)
	default:
		executionPageNotFound(w, r)
	}

}

// sequenceProgress is a progressMeter used to show
// progress during the execution of sequence.
type sequenceProgress struct {
	caseProgress
}

// Init initializes the progress meter with given request data.
func (p *sequenceProgress) Init(r *http.Request) error {
	ts, err := handler.GetTestSequence(r)
	if err != nil {
		return err
	}
	tsv, err := handler.GetTestSequenceVersion(r, ts)
	if err != nil {
		return err
	}
	stepNr := getFormValueInt(r, keyStepNr)
	caseNr := getFormValueInt(r, keyCaseNr)
	p.cur = totalWorkStepsUpTo(*tsv, caseNr-1) + stepNr + 2
	p.max = totalWorkSteps(*tsv)
	return nil
}

// SequenceExecutionPost returns a handler capable of handling every request during the execution
// of a sequence.
func SequenceExecutionPost(caseProtocolLister, seqenceProtocolLister test.ProtocolLister,
	caseStore CaseProtocolStore, sequenceStore SequenceProtocolAdder,
	caseSession CaseSession, sequenceSession SequenceSession) http.Handler {
	t := &sessionTimer{caseSession}
	cp := caseExecutionPrinter{
		caseSession,
		sequenceSession,
		caseSession,
		true,
	}
	sp := sequenceExecutionPrinter{sequenceSession, caseStore, caseSession}
	tc := &testCaseExecutionPost{
		caseStartPagePost(caseProtocolLister, t, caseSession, cp),
		caseStepPagePost(t, caseSession, cp),
		caseSummaryPagePost(t, caseSession, caseStore,
			sequencePreSummaryPagePost(t, sequenceStore, sequenceSession, cp, sp)),
		&sequenceProgress{},
		caseStore,
		caseSession,
	}
	return &testSequenceExecutionPost{
		sequenceStarPagePost(seqenceProtocolLister, t, sequenceSession, cp),
		func(w http.ResponseWriter, r *http.Request, ts test.Sequence, tsv test.SequenceVersion) {
			// Finished redirect to testsequence page
			http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
		},
		tc,
	}
}

// sequenceStarPagePost handles requests from the start page of
// a sequence execution.
func sequenceStarPagePost(protocolLister test.ProtocolLister, timer timer, setter SequenceSessionSetter,
	printer caseExecutionPrinter) testSequencePageHandler {
	return func(w http.ResponseWriter, r *http.Request, ts test.Sequence, tsv test.SequenceVersion) {
		sutVersion := getFormValueString(r, keySUTVersion)
		sutVariant := getFormValueString(r, keySUTVariant)
		time, err := timer.updateTime(w, r)
		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveStartPage, r).
				WithLog("Error while trying to create and save current protocol.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		// Create and store protocol.
		prt, err := test.NewSequenceExecutionProtocol(protocolLister, tsv.ID(), sutVariant, sutVersion, time)
		if err == nil {
			err = setter.SetCurrentSequenceProtocol(w, r, &prt)

		}
		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveStartPage, r).
				WithLog("Error while trying to create and save current protocol.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		caseNr := getFormValueInt(r, keyCaseNr)
		tc := tsv.Cases[caseNr]
		tcv := tc.TestCaseVersions[0]

		printer.printStartPage(w, r, tc, tcv, 2, totalWorkSteps(tsv))
	}
}

// sequencePreSummaryPagePost handles request from the last case summary within
// a sequence execution.
func sequencePreSummaryPagePost(timer timer, adder SequenceProtocolAdder, session SequenceSession,
	cp caseExecutionPrinter, sp sequenceExecutionPrinter) func(http.ResponseWriter, *http.Request,
	test.CaseExecutionProtocol, progressMeter) {
	return func(w http.ResponseWriter, r *http.Request, protocol test.CaseExecutionProtocol, progress progressMeter) {
		ts, err := handler.GetTestSequence(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tsv, err := handler.GetTestSequenceVersion(r, ts)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		caseNr := getFormValueInt(r, keyCaseNr)
		tc := tsv.Cases[caseNr-1]

		seqPrt, err := session.GetCurrentSequenceProtocol(r)
		if seqPrt == nil {
			errors.Handle(err, w, r)
			return
		}
		seqPrt.AddCaseExecutionProtocol(protocol)

		err = session.SetCurrentSequenceProtocol(w, r, seqPrt)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		if caseNr == len(tsv.Cases) {
			sp.printSummaryPage(w, r, *tsv)
			// Save sequence protocol
			prt, _ := session.GetCurrentSequenceProtocol(r)
			if prt == nil {
				errors.ConstructStd(http.StatusInternalServerError,
					failedSave, unableToSaveSummaryPage, r).
					WithLog("Error while trying to get current protocol.").
					WithStackTrace(1).
					WithRequestDump(r).
					Respond(w)
				return
			}

			if err := adder.AddSequenceProtocol(prt); err != nil {
				errors.ConstructStd(http.StatusInternalServerError,
					failedSave, unableToSaveSummaryPage, r).
					WithLog("Error while trying to save protocol into store.").
					WithCause(err).
					WithStackTrace(1).
					WithRequestDump(r).
					Respond(w)
				return
			}

			if err := session.RemoveCurrentSequenceProtocol(w, r); err != nil {
				err := errors.ConstructWithStackTrace(http.StatusInternalServerError,
					"Error while trying to save current protocol.").Finish()
				errors.Log(err)
				return
			}

			if err := timer.resetTime(w, r); err != nil {
				errors.Handle(err, w, r)
				return
			}
		} else {
			tc = tsv.Cases[caseNr]
			tcv := tc.TestCaseVersions[0]
			cp.printStartPage(w, r, tc, tcv, progress.Get(), progress.Max())
		}

	}
}
