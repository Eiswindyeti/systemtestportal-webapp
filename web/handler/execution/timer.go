/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// timer used to store time during requests.
type timer interface {
	// resetTime updates the time stored in the timer. Using given request.
	// Returns the difference in time between the timer currently stored time and
	// the time given by the request. Or an error if something goes wrong.
	updateTime(w http.ResponseWriter, r *http.Request) (duration.Duration, error)
	// resetTime resets the time stored in this timer.
	resetTime(w http.ResponseWriter, r *http.Request) error
}

// TimeSession is used to store time in during requests.
type TimeSession interface {
	//GetDuration returns the duration needed in this execution til now.
	//If there is no sequence execution running, the function will return nil, nil
	//If an error occurs nil and the error will be returned
	GetDuration(r *http.Request) (*duration.Duration, error)
	//SetDuration saves the given duration to the session.
	//After this call, you can get the current duration via the GetDuration-function
	SetDuration(w http.ResponseWriter, r *http.Request, duration *duration.Duration) error
}

// sessionTimer a timer using the session as store.
type sessionTimer struct {
	TimeSession
}

// resetTime updates the time stored in the session. Using given request.
// Returns the difference in time between the timer currently stored time and
// the time given by the request.
func (t *sessionTimer) updateTime(w http.ResponseWriter, r *http.Request) (duration.Duration, error) {
	time := getPostedDuration(r)
	prevTime, _ := t.TimeSession.GetDuration(r)
	if prevTime == nil {
		prevTime = &duration.Duration{}
	}
	err := t.TimeSession.SetDuration(w, r, &time)
	if err != nil {
		return duration.NewDuration(0, 0, 0), errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveTime, r).
			WithLog("Error while trying to save needed time.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Finish()
	}
	passedTime := time.Sub(*prevTime)
	return passedTime, nil
}

// resetTime resets the time stored in the session.
func (t *sessionTimer) resetTime(w http.ResponseWriter, r *http.Request) error {
	time := duration.NewDuration(0, 0, 0)
	err := t.TimeSession.SetDuration(w, r, &time)
	if err != nil {
		return errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveTime, r).
			WithLog("Error while trying to reset needed time.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Finish()
	}
	return nil
}

// getPostedDuration simply reads a duration out of the request.
func getPostedDuration(r *http.Request) duration.Duration {
	hh := getFormValueInt(r, keyHours)
	mm := getFormValueInt(r, keyMinutes)
	ss := getFormValueInt(r, keySeconds)
	return duration.NewDuration(hh, mm, ss)
}
