/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// caseExecutionPrinter prints pages for a testcase execution
type caseExecutionPrinter struct {
	CaseSessionGetter
	SequenceSessionGetter
	TimeSession
	inSequence bool
}

// printStartPage prints the starting page for a testcase execution.
func (printer *caseExecutionPrinter) printStartPage(w http.ResponseWriter, r *http.Request,
	tc test.Case, tcv test.CaseVersion, progress int, progressTotal int) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionStartPage)
	seqPrt, _ := printer.GetCurrentSequenceProtocol(r)
	var sutVersion, sutVariant string
	var time *duration.Duration
	if seqPrt == nil {
		sutVersion = ""
		sutVariant = ""
		time = &duration.Duration{}
	} else {
		sutVersion = seqPrt.SUTVersion
		sutVariant = seqPrt.SUTVariant
		time, _ = printer.GetDuration(r)
	}
	if time == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadTime, r).
			WithLog("Error while trying to get needed time.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}
	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	caseNr := getFormValueInt(r, keyCaseNr) + 1

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, false).
		With(keyIsInSeq, printer.inSequence).
		With(keyCaseNr, caseNr).
		With(keyProject, p).
		With(keyTestObject, tc).
		With(keyTestObjectVersion, tcv).
		With(keyEstimatedMinutes, tcv.Duration.GetMinuteInHour()).
		With(keyEstimatedHours, int(tcv.Duration.Hours())).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keySUTVersion, sutVersion).
		With(keySUTVariant, sutVariant).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute())
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// printStepPage prints the page for teststep during execution of a testcase.
func (printer *caseExecutionPrinter) printStepPage(w http.ResponseWriter, r *http.Request, tcv test.CaseVersion,
	stepNr int, progress int, progressTotal int) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tc, err := handler.GetTestCase(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionStep)
	prt, _ := printer.GetCurrentCaseProtocol(r)
	if prt == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadStepPage, r).
			WithLog("Error while trying to get current protocol.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}
	time, _ := printer.GetDuration(r)
	if time == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadTime, r).
			WithLog("Error while trying to get needed time.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	caseNr := getFormValueInt(r, keyCaseNr)

	ctx := context.New().
		WithUserInformation(r).
		With(keyProject, p).
		With(keyTestObject, tc).
		With(keyTestObjectVersion, tcv).
		With(keyStepNr, stepNr).
		With(keyCaseNr, caseNr).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyAction, tcv.Steps[stepNr-1].Action).
		With(keyExpectedResult, tcv.Steps[stepNr-1].ExpectedResult).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute())

	handler.PrintTmpl(ctx, tmpl, w, r)
}

// printSummaryPage prints the summary page for the execution of a testcase.
func (printer *caseExecutionPrinter) printSummaryPage(w http.ResponseWriter, r *http.Request, tcv test.CaseVersion,
	progress int, progressTotal int) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tc, err := handler.GetTestCase(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionSummary)
	prt, _ := printer.GetCurrentCaseProtocol(r)
	if prt == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadSummaryPage, r).
			WithLog("Error while trying to get current protocol.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}
	time, _ := printer.GetDuration(r)
	if time == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadTime, r).
			WithLog("Error while trying to get needed time.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	type stepSummary struct {
		Action           string
		Result           test.Result
		ObservedBehavior string
		Comment          string
		NeededTime       duration.Duration
	}
	var steps []stepSummary
	for i := 0; i < len(tcv.Steps); i++ {
		steps = append(steps, stepSummary{
			Action:           tcv.Steps[i].Action,
			Result:           prt.StepProtocols[i].Result,
			ObservedBehavior: prt.StepProtocols[i].ObservedBehavior,
			Comment:          prt.StepProtocols[i].Comment,
			NeededTime:       prt.StepProtocols[i].NeededTime,
		})
	}
	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	caseNr := getFormValueInt(r, keyCaseNr)

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsInSeq, printer.inSequence).
		With(keyIsSeq, false).
		With(keyTestObjectVersion, tcv).
		With(keyProject, p).
		With(keyTestObject, tc).
		With(keyAllSubObjects, steps).
		With(keyCaseNr, caseNr).
		With(keyStepNr, len(tcv.Steps)+1).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute())
	handler.PrintTmpl(ctx, tmpl, w, r)
}
