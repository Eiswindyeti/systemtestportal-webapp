/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// CaseSession is used to manage the session for testcases during execution.
type CaseSession interface {
	CaseSessionUpdater
	//RemoveCurrentCaseProtocol removes the current case protocol from the session.
	RemoveCurrentCaseProtocol(w http.ResponseWriter, r *http.Request) error
	TimeSession
}

// CaseSessionUpdater is used to update the testcase currently contained within a session.
type CaseSessionUpdater interface {
	CaseSessionGetter
	CaseSessionSetter
}

// CaseSessionGetter is used to get the testcase currently contained within a session.
type CaseSessionGetter interface {
	//GetCurrentCaseProtocol returns the protocol to the currently running case execution to the given request.
	//If there is no case execution running, the function will return nil, nil
	//If an error occurs nil and the error will be returned
	GetCurrentCaseProtocol(r *http.Request) (*test.CaseExecutionProtocol, error)
}

// CaseSessionSetter is used to set the managed testcase during a session.
type CaseSessionSetter interface {
	//SetCurrentCaseProtocol saves the given protocol to the session.
	//After this call, you can get the current case protocol via the GetCurrentCaseProtocol-function
	SetCurrentCaseProtocol(w http.ResponseWriter, r *http.Request, protocol *test.CaseExecutionProtocol) error
}

// CaseProtocolCleaner is used to clean the testcase session after a new protocol has
// been successfully added.
type CaseProtocolCleaner interface {
	CaseSessionGetter
	//RemoveCurrentCaseProtocol removes the current case protocol from the session.
	RemoveCurrentCaseProtocol(w http.ResponseWriter, r *http.Request) error
}

// CaseProtocolStore interface for storing testcase protocols.
type CaseProtocolStore interface {
	CaseProtocolAdder
	CaseProtocolGetter
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testCaseID id.TestID) ([]test.CaseExecutionProtocol, error)
}

// CaseProtocolAdder is used to add new testcase protocols to the store.
type CaseProtocolAdder interface {
	// AddCaseProtocol adds the given protocol to the store
	AddCaseProtocol(r *test.CaseExecutionProtocol) (err error)
}

// CaseProtocolGetter is used to get testcase protocols from the store.
type CaseProtocolGetter interface {
	// GetCaseExecutionProtocol gets the protocol with the given id for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error)
}

// CaseStartPageGet serves the start page for testcase executions.
func CaseStartPageGet(cs CaseSession, ss SequenceSessionGetter) http.HandlerFunc {
	printer := caseExecutionPrinter{cs, ss, cs, false}
	return func(w http.ResponseWriter, r *http.Request) {
		tc, err := handler.GetTestCase(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		tcv, err := handler.GetTestCaseVersion(r, tc)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		printer.printStartPage(w, r, *tc, *tcv, 1, len(tcv.Steps)+2)
	}
}

// testCasePageHandler is the type for sub handler used by testCaseExecutionPost
// to delegate requests.
type testCasePageHandler func(w http.ResponseWriter, r *http.Request,
	tc test.Case, tcv test.CaseVersion, progress progressMeter)

// testCaseExecutionPost is the handler for all post requests during a
// test case execution.
type testCaseExecutionPost struct {
	Start    testCasePageHandler
	Step     testCasePageHandler
	Summary  testCasePageHandler
	Progress progressMeter
	Store    CaseProtocolStore
	Session  CaseSession
}

// ServeHTTP simply distributes request onto the specific sub handlers.
func (t *testCaseExecutionPost) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	tc, err := handler.GetTestCase(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	tcv, err := handler.GetTestCaseVersion(r, tc)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	stepNr := getFormValueInt(r, keyStepNr)
	steps := len(tcv.Steps)

	if err := t.Progress.Init(r); err != nil {
		errors.Handle(err, w, r)
		return
	}
	switch {
	case stepNr == 0:
		t.Start(w, r, *tc, *tcv, t.Progress)
	case stepNr > 0 && stepNr < steps+1:
		t.Step(w, r, *tc, *tcv, t.Progress)
	case stepNr == steps+1:
		t.Summary(w, r, *tc, *tcv, t.Progress)
	default:
		executionPageNotFound(w, r)
	}
}

// caseProgress is the progress meter for the execution of a
// single testcase.
type caseProgress struct {
	cur, max int
}

func (p *caseProgress) Init(r *http.Request) error {
	tc, err := handler.GetTestCase(r)
	if err != nil {
		return err
	}
	tcv, err := handler.GetTestCaseVersion(r, tc)
	if err != nil {
		return err
	}
	p.cur = getFormValueInt(r, keyStepNr) + 2
	p.max = len(tcv.Steps) + 2
	return nil
}

func (p *caseProgress) Add(progress int) {
	p.cur += progress
}

func (p *caseProgress) Get() int {
	return p.cur
}

func (p *caseProgress) Max() int {
	return p.max
}

// CaseExecutionPost handles all post requests during a testcase execution. It is a meta handler
// that further calls it's sub handlers.
func CaseExecutionPost(protocolLister test.ProtocolLister, store CaseProtocolStore, cs CaseSession,
	ss SequenceSessionGetter) http.Handler {

	t := sessionTimer{cs}
	p := caseExecutionPrinter{cs, ss, cs, false}

	return &testCaseExecutionPost{
		caseStartPagePost(protocolLister, &t, cs, p),
		caseStepPagePost(&t, cs, p),
		caseSummaryPagePost(&t, cs, store, func(w http.ResponseWriter, r *http.Request,
			protocol test.CaseExecutionProtocol, meter progressMeter) {
			// Execution finished redirect back to testcase if everything worked fine
			url := strings.TrimSuffix(r.URL.RawPath, "execute")
			url = url + "?fragment=true"
			http.Redirect(w, r, url, http.StatusSeeOther)
		}),
		&caseProgress{},
		store,
		cs,
	}
}

// caseStartPagePost is a sub handler that handles requests from the start
// page of a test case execution.
func caseStartPagePost(protocolLister test.ProtocolLister, timer timer, setter CaseSessionSetter,
	printer caseExecutionPrinter) testCasePageHandler {

	return func(w http.ResponseWriter, r *http.Request, tc test.Case, tcv test.CaseVersion, progress progressMeter) {

		sutVersion := getFormValueString(r, keySUTVersion)
		sutVariant := getFormValueString(r, keySUTVariant)
		time, err := timer.updateTime(w, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		prt, err := test.NewCaseExecutionProtocol(protocolLister, tcv.ID(), sutVariant, sutVersion, time, len(tcv.Steps))
		if err == nil {
			err = setter.SetCurrentCaseProtocol(w, r, &prt)
		}
		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveStartPage, r).
				WithLog("Error while trying to create and save current protocol.").
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		printer.printStepPage(w, r, tcv, 1, progress.Get(), progress.Max())
	}
}

// caseStepPagePost is a sub handler that handles requests from the intermediate step
// page of a test case execution.
func caseStepPagePost(timer timer, session CaseSessionUpdater, printer caseExecutionPrinter) testCasePageHandler {
	return func(w http.ResponseWriter, r *http.Request, tc test.Case, tcv test.CaseVersion, progress progressMeter) {
		observedBehavior := getFormValueString(r, keyObservedBehavior)
		comment := getFormValueString(r, keyComment)
		time, err := timer.updateTime(w, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		result, ok := getFormValueResult(r, keyResult)
		if !ok {
			errors.ConstructStd(http.StatusBadRequest,
				failedSave, unableToSaveStep, r).
				WithLog("Client send an invalid request.").
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		prt, err := session.GetCurrentCaseProtocol(r)
		if prt == nil || err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveStep, r).
				WithLog("Error while trying to get current protocol.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		stepNr := getFormValueInt(r, keyStepNr)

		prt.SaveStep(stepNr, observedBehavior, result, comment, time)
		if err := session.SetCurrentCaseProtocol(w, r, prt); err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveStep, r).
				WithLog("Error while trying to save current protocol.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		steps := len(tcv.Steps)

		if stepNr == steps {
			printer.printSummaryPage(w, r, tcv, progress.Get(), progress.Max())
		} else {
			printer.printStepPage(w, r, tcv, stepNr+1, progress.Get(), progress.Max())
		}
	}
}

// caseSummaryPagePost is a sub handler that
// deals with requests from the summary page of
// a testcase execution.
func caseSummaryPagePost(timer timer, session CaseProtocolCleaner, adder CaseProtocolAdder,
	summaryAction func(http.ResponseWriter, *http.Request, test.CaseExecutionProtocol, progressMeter),
) testCasePageHandler {
	return func(w http.ResponseWriter, r *http.Request, tc test.Case, tcv test.CaseVersion, progress progressMeter) {
		comment := getFormValueString(r, keyComment)
		time, err := timer.updateTime(w, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		result, ok := getFormValueResult(r, keyResult)
		if !ok {
			errors.ConstructStd(http.StatusBadRequest,
				failedSave, unableToSaveStep, r).
				WithLog("Client send an invalid request.").
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		//Get Current Protocol
		prt, _ := session.GetCurrentCaseProtocol(r)
		if prt == nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveSummaryPage, r).
				WithLog("Error while trying to get current protocol.").
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		//Save Data to protocol and protocol to store
		prt.Finish(result, comment, time)
		err = adder.AddCaseProtocol(prt)
		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				failedSave, unableToSaveSummaryPage, r).
				WithLog("Error while trying to save protocol into store.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		//Remove Protocol from Session
		if err = session.RemoveCurrentCaseProtocol(w, r); err != nil {
			e := errors.ConstructWithStackTrace(http.StatusInternalServerError,
				"Error while trying to save current protocol.").Finish()
			errors.Log(e)
			return
		}
		if err := timer.resetTime(w, r); err != nil {
			errors.Handle(err, w, r)
			return
		}

		summaryAction(w, r, *prt, progress)
	}
}
