/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

const failedSave = "Saving input failed"
const unableToSaveStep = "We were unable to save the results for this step. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"
const unableToSaveStartPage = "We were unable to save the information for this test case. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"
const unableToSaveSummaryPage = "We were unable to save the information for this test case. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"
const unableToLoadSummaryPage = "We were unable to load the information for the summary of this test case. " +
	"This is our fault. We don't know how this happened and are terribly sorry :|"
const unableToLoadStepPage = "We were unable to load the information for the next test step. This is our fault. " +
	"We don't know how this happened and are terribly sorry :|"
const unableToLoadTime = "We were unable to load the needed time so far from your cookie. This is our fault. " +
	"We don't know how this happened and are terribly sorry :| (Maybe the cookie monster ate them)"
const unableToSaveTime = "We were unable to save the needed time so far to your cookie. This is our fault. " +
	"We don't know how this happened and are terribly sorry :| (Maybe the cookie monster ate them)"
const pageNotFoundTitle = "Couldn't get requested page!"
const pageNotFound = "It seems your client requested a page that doesn't exist. If you believe this is a bug " +
	"please contact us via our " + handler.IssueTracker + "."

// executionPageNotFound returns a corresponding error if a page of the
// execution isn't found.
func executionPageNotFound(w http.ResponseWriter, r *http.Request) {
	errors.ConstructStd(http.StatusNotFound, pageNotFoundTitle, pageNotFound, r).
		WithLog("Client requested invalid page number!").
		WithStackTrace(2).
		WithRequestDump(r).
		Respond(w)
}
