/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

// containsLabel checks whether the slice with labels contains
// a label with the given name (ln).
// Returns true if the slice contains a label with the name.
// Else return false.
func containsLabel(ln string, ls *[]project.Label) bool {
	for _, l := range *ls {
		if l.Name == ln {
			return true
		}
	}
	return false
}
