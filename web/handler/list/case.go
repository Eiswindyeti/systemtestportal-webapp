/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CasesGet simply serves a page that is used to list testcases,
func CasesGet(lister handler.TestCaseLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tcs, err := lister.List(p.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		filter := r.FormValue(httputil.Filter)
		tcs = filterCases(filter, tcs)

		tmpl := getTestCaseListFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, p).
			With(context.Filtered, filter).
			With(context.TestCases, tcs), tmpl, w, r)
	}
}

func filterCases(filter string, cases []*test.Case) []*test.Case {
	if filter == "" {
		return cases
	}

	i := 0
	for _, c := range cases {
		if containsLabel(filter, &c.Labels) {
			cases[i] = c
			i++
		}
	}
	cases = cases[:i]

	return cases
}

// getTestCaseListFragment returns either only the test case list fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTestCaseListFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesListFragment()
	}
	return getTabTestCasesListTree()
}

// getTabTestCasesListTree returns the test case list tab template with all parent templates
func getTabTestCasesListTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab testcase list tree
		Append(templates.TestCasesList, templates.ManageLabels).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestCasesListFragment returns only the test case list tab template
func getTabTestCasesListFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.TestCasesList, templates.ManageLabels).
		Get().Lookup(templates.TabContent)
}
