/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// SequencesGet simply serves the page that lists sequences.
func SequencesGet(lister handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tss, err := lister.List(p.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		filter := r.FormValue(httputil.Filter)
		tss = filterSequences(filter, tss)

		tmpl := getTestSequenceListFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, p).
			With(context.Filtered, filter).
			With(context.TestSequences, tss), tmpl, w, r)

	}
}

func filterSequences(filter string, sequences []*test.Sequence) []*test.Sequence {
	if filter == "" {
		return sequences
	}

	i := 0
	for _, s := range sequences {
		if containsLabel(filter, &s.Labels) {
			sequences[i] = s
			i++
		}
	}
	sequences = sequences[:i]

	return sequences
}

func getTestSequenceListFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesListFragment()
	}
	return getTabTestSequencesListTree()
}

// getTabTestSequencesListTree returns the test sequence list tab template with all parent templates
func getTabTestSequencesListTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence list tree
		Append(templates.TestSequencesList, templates.ManageLabels).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesListFragment returns only the test sequence list tab template
func getTabTestSequencesListFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.TestSequencesList, templates.ManageLabels).
		Get().Lookup(templates.TabContent)
}
