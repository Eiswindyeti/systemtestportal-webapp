/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"html/template"
	"io"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// ProtocolsGet serves the page that displays the list of protocols
func ProtocolsGet(tcl handler.TestCaseLister, tsl handler.TestSequenceLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		tss, err := tsl.List(p.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tcs, err := tcl.List(p.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		sel := GetSelection(r)

		showTabProtocolList(w, r, p, tcs, tss, sel)
	}
}

// showTabProtocolList executes either the protocol list fragment only or the protocol
// list fragment with all its parent fragments, depending on the isFrag parameter
func showTabProtocolList(w io.Writer, r *http.Request,
	p *project.Project, tcs []*test.Case, tss []*test.Sequence, sel Selection) {

	tmpl := getShowProtocolList(r)

	ctx := context.New().
		WithUserInformation(r).
		With(context.SelectedTestCase, sel.SelectedCase).
		With(context.SelectedTestSequence, sel.SelectedSequence).
		With(context.SelectedType, sel.SelectedType).
		With(context.Project, p).
		With(context.TestCases, tcs).
		With(context.TestSequences, tss)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getShowProtocolList returns either the fragment for the
// protocol list or the list with all parent templates
func getShowProtocolList(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabProtocolsListFragment()
	}
	return getTabProtocolsListTree()
}

// getTabProtocolsListFragment returns only the protocol list tab template
func getTabProtocolsListFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.ProtocolsList).
		Get().Lookup(templates.TabContent)
}

// getTabProtocolsListTree returns the protocol list tab template with all parent templates
func getTabProtocolsListTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence edit tree
		Append(templates.ProtocolsList).
		Get().Lookup(templates.HeaderDef)
}
