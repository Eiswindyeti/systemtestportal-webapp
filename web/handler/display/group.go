/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// ShowGroupGet is a handler for creating a group
func ShowGroupGet(u handler.UserLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tmpl := getNewGroupTree()
		users, err := u.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Users, users), tmpl, w, r)
	}
}

// getNewGroupTree returns the new group template with all parent templates
func getNewGroupTree() *template.Template {
	return handler.GetNoSideBarTree().
		// New group tree
		Append(templates.NewGroup).
		Get().Lookup(templates.HeaderDef)
}
