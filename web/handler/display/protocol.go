/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"io"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CaseProtocolsGet serves the page displaying case protocols.
func CaseProtocolsGet(w http.ResponseWriter, r *http.Request) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
	} else {
		selType := r.FormValue(httputil.ProtocolType)
		selCase := r.FormValue(httputil.SelectedProtocol)
		selSequence := r.FormValue(httputil.SelectedProtocol)

		showTabTestCaseProtocols(w, r, selType, selCase, selSequence, *p)
	}
}

// showTabTestCaseProtocols executes either the protocol show test case fragment only or the protocol show test case
// fragment with all its parent fragments, depending on the isFrag parameter
func showTabTestCaseProtocols(w io.Writer, r *http.Request, t string, c string, s string, p project.Project) {
	tmpl := getTabProtocolsShowTestCaseTree()
	if httputil.IsFragmentRequest(r) {
		tmpl = getTabProtocolsShowTestCaseFragment()
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.SelectedTestCase, c).
		With(context.SelectedTestSequence, s).
		With(context.SelectedType, t).
		With(context.Project, p)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabProtocolsShowTestCaseFragment returns only the protocol show test case tab template
func getTabProtocolsShowTestCaseFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.TestCaseProtocols).
		Get().Lookup(templates.TabContent)
}

// getTabProtocolsShowTestCaseTree returns the protocol show test case tab template with all parent templates
func getTabProtocolsShowTestCaseTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence edit tree
		Append(templates.TestCaseProtocols).
		Get().Lookup(templates.HeaderDef)
}

// SequenceProtocolsGet serves the page displaying testsequence protocols.
func SequenceProtocolsGet(w http.ResponseWriter, r *http.Request) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
	} else {
		selType := r.FormValue(httputil.ProtocolType)
		selCase := r.FormValue(httputil.SelectedProtocol)
		selSequence := r.FormValue(httputil.SelectedProtocol)

		getTabSequenceProtocol(w, r, selType, selCase, selSequence, *p)
	}
}

// getTabSequenceProtocol executes either the protocol show test sequence fragment only or the protocol show
// test sequence fragment with all its parent fragments, depending on the isFrag parameter
func getTabSequenceProtocol(w io.Writer, r *http.Request, t string, c string, s string, p project.Project) {

	tmpl := getTabSequenceProtocolTree()
	if httputil.IsFragmentRequest(r) {
		tmpl = getTabSequenceProtocolFragment()
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.SelectedTestCase, c).
		With(context.SelectedTestSequence, s).
		With(context.SelectedType, t).
		With(context.Project, p)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabSequenceProtocolFragment returns only the protocol show test sequence tab template
func getTabSequenceProtocolFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.TestSequenceProtocols).
		Get().Lookup(templates.TabContent)
}

// getTabSequenceProtocolTree returns the protocol show test sequence tab template with all parent templates
func getTabSequenceProtocolTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence protocols
		Append(templates.TestSequenceProtocols).
		Get().Lookup(templates.HeaderDef)
}
