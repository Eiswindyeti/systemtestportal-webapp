/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// ShowSequenceGet serves the page showing a sequence.
func ShowSequenceGet(w http.ResponseWriter, r *http.Request) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ts, err := handler.GetTestSequence(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	c := handler.GetComments(r)

	ShowSequence(p, ts, c, w, r)
}

// ShowSequence tries to respond with the display page for a sequence
// if an error occurs an error response is sent instead.
func ShowSequence(p *project.Project, ts *test.Sequence, c []*comment.Comment, w http.ResponseWriter,
	r *http.Request) {
	tmpl := getTabTestSequenceShow(r)
	tsv, err := handler.GetTestSequenceVersion(r, ts)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, p).
		With(context.TestSequence, ts).
		With(context.TestSequenceVersion, tsv).
		With(context.Comments, c)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabTestSequenceShow returns either the test sequence show fragment only or the show fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestSequenceShow(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesShowFragment()
	}
	return getTabTestSequencesShowTree()
}

// getTabTestSequencesShowTree returns the test sequence show tab template with all parent templates
func getTabTestSequencesShowTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		Append("modal/tester-assignment").
		// Tab test sequence show tree
		Append(templates.ShowTestSequence).
		Append(templates.Comments).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesShowFragment returns only the test sequence show template
func getTabTestSequencesShowFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.ShowTestSequence).
		Append(templates.Comments).
		Append("modal/tester-assignment").
		Get().Lookup(templates.TabContent)
}

// CreateSequenceGet serves the page used to create a new testsequence.
func CreateSequenceGet(lister handler.TestCaseLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tcs, err := lister.List(p.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getTestSequenceNewFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, p).
			With(context.TestCases, tcs), tmpl, w, r)

	}
}

func getTestSequenceNewFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesNewFragment()
	}
	return getTabTestSequencesNewTree()

}

// getTabTestSequencesNewTree returns the new test sequence tab template with all parent templates
func getTabTestSequencesNewTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence new tree
		Append(templates.NewTestSequence, templates.TestCaseSelection, templates.ManageVersions).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesNewFragment returns only the new test sequence tab template
func getTabTestSequencesNewFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.NewTestSequence, templates.TestCaseSelection, templates.ManageVersions).
		Get().Lookup(templates.TabContent)
}

// EditSequenceGet serves the page used to edit a testsequence.
func EditSequenceGet(t handler.TestCaseLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		p, err := handler.GetProject(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		ts, err := handler.GetTestSequence(r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getTabTestSequenceEdit(r)
		latest := len(ts.SequenceVersions)
		tsv := handler.GetVersion(r, latest)
		if tsv != latest {
			errors.ConstructStd(http.StatusBadRequest,
				handler.ErrInvalidTestSequenceVersionTitle, handler.ErrInvalidTestSequenceVersion, r).
				WithLogf("Client sent invalid case version number %d for editing."+
					"Should've been %d.", tsv, latest).
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		tcs, err := t.List(p.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		ctx := context.New().
			WithUserInformation(r).
			With(context.Project, p).
			With(context.TestCases, tcs).
			With(context.TestSequence, ts).
			With(context.TestSequenceVersion, ts.SequenceVersions[len(ts.SequenceVersions)-tsv])
		handler.PrintTmpl(ctx, tmpl, w, r)
	}
}

// getTabTestSequenceEdit returns either the test sequence edit fragment only or the edit fragment with all its parent
// fragments, depending on the isFrag parameter
func getTabTestSequenceEdit(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesEditFragment()
	}
	return getTabTestSequencesEditTree()
}

// getTabTestSequencesEditTree returns the test sequence edit tab template with all parent templates
func getTabTestSequencesEditTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence edit tree
		Append(templates.EditTestSequence, templates.TestCaseSelection, templates.ManageVersions).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesEditFragment returns only the test sequence edit template
func getTabTestSequencesEditFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.EditTestSequence, templates.TestCaseSelection, templates.ManageVersions).
		Get().Lookup(templates.TabContent)
}

// HistorySequenceGet serves the page showing the history of a sequence.
func HistorySequenceGet(w http.ResponseWriter, r *http.Request) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	ts, err := handler.GetTestSequence(r)
	if err != nil {
		errors.Handle(err, w, r)
	}

	tmpl := getTabTestSequencesHistory(r)
	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, p).
		With(context.TestSequence, ts)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getTabTestSequencesHistory returns either the test sequence history fragment only or the edit fragment
// with all its parent fragments, depending on the isFrag parameter
func getTabTestSequencesHistory(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestSequencesHistoryFragment()
	}
	return getTabTestSequencesHistoryTree()
}

// getTabTestSequencesHistoryTree returns the test sequence history tab template with all parent templates
func getTabTestSequencesHistoryTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence history tree
		Append(templates.TestSequenceHistory).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestSequencesHistoryFragment returns only the test sequence history template
func getTabTestSequencesHistoryFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.TestSequenceHistory).
		Get().Lookup(templates.TabContent)
}
