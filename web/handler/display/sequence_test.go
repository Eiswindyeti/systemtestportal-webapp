/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestTestSequenceGet(t *testing.T) {
	wrongVersionParams := url.Values{}
	wrongVersionParams.Add(httputil.Version, "-1")
	nilCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	tested := ShowSequenceGet
	handler.Suite(t,
		handler.CreateTestHandler("Invalid context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Nil as testsequence in context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.SimpleRequest(nilCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(nilCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Request with invalid version",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusBadRequest)),
			handler.SimpleRequest(ctx, http.MethodGet, wrongVersionParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, wrongVersionParams),
		),
		handler.CreateTestHandler("Valid request",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusOK)),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestCreateSequenceGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)
	handler.Suite(t,
		handler.CreateTestHandler("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return CreateSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return CreateSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestEditSequenceGet(t *testing.T) {
	wrongVersionParams := url.Values{}
	wrongVersionParams.Add(httputil.Version, "-1")
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Invalid version number",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusBadRequest),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, wrongVersionParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, wrongVersionParams),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestHistorySequenceGet(t *testing.T) {
	nilCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	tested := HistorySequenceGet
	handler.Suite(t,
		handler.CreateTestHandler("Invalid context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Nil as testsequence in context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.SimpleRequest(nilCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(nilCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Valid request",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusOK)),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
