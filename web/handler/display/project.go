/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CreateProjectGet is a handler for showing the screen for creating a new project
func CreateProjectGet(w http.ResponseWriter, r *http.Request) {
	tmpl := getNewProjectTree()
	handler.PrintTmpl(context.New().WithUserInformation(r), tmpl, w, r)
}

// getNewProjectTree returns the new project template with all parent templates
func getNewProjectTree() *template.Template {
	return handler.GetNoSideBarTree().
		// New project tree
		Append(templates.NewProject).
		Get().Lookup(templates.HeaderDef)
}

// ProjectSettingsGet is a handler for showing the settings page of a project
func ProjectSettingsGet(w http.ResponseWriter, r *http.Request) {
	p, err := handler.GetProject(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	_, err = handler.GetContainerID(r)
	if err != nil {
		errors.Handle(err, w, r)
	}

	tmpl := getProjectSettingsFragment(r)
	handler.PrintTmpl(context.New().
		WithUserInformation(r).
		With("Project", p), tmpl, w, r)
}

func getProjectSettingsFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabSettingsFragment()
	}
	return getTabSettingsTree()
}

func getTabSettingsTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		Append(templates.Settings).
		Get().Lookup(templates.HeaderDef)
}

func getTabSettingsFragment() *template.Template {
	return handler.GetBaseTree().Append(templates.Settings).Get().Lookup(templates.TabContent)
}
