/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package printing

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestPrintProtocolsList(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				s := &handler.SequenceListerMock{}
				c := &handler.CaseListerMock{}
				return ProtocolsListGet(c, s), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(s, 0),
					handler.HasCalls(c, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Sequence lister returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				s := &handler.SequenceListerMock{Err: handler.ErrTest}
				c := &handler.CaseListerMock{}
				return ProtocolsListGet(c, s), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Case lister returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				s := &handler.SequenceListerMock{}
				c := &handler.CaseListerMock{Err: handler.ErrTest}
				return ProtocolsListGet(c, s), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				s := &handler.SequenceListerMock{}
				c := &handler.CaseListerMock{}
				return ProtocolsListGet(c, s), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(s, 1),
					handler.HasCalls(c, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestPrintTestCaseProtocols(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	tested := CaseProtocolsGet
	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestPrintTestSequenceProtocols(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	tested := SequenceProtocolsGet
	handler.Suite(t,
		handler.CreateTestHandler("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTestHandler("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
