{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-4 col-sm-2 p-4">
            <img src="/static/img/STP-Logo.png" class="img-fluid image-responsive">
        </div>
        <div class="col-12 col-sm-10 col-md-6">
            <h4>About:</h4>
            <p>The System Test Portal is originally created by students of the University of Stuttgart as part of a so
                called Studienprojekt. It's supervised by employees of the Institute of Softwaretechnology.</p>
            <a href="http://systemtestportal.org">
                <button type="button" class="btn btn-primary">Website</button>
            </a><a href="https://gitlab.com/stp-team/systemtestportal-webapp" class="ml-2">
            <button type="button" class="btn btn-primary">Project on gitlab.com</button>
        </a>
        </div>
    </div>
    <div class="row justify-content-md-center">
        <div class="col-12 col-md-8">
            <h4>Used Libraries and tools:</h4>
            <table class="table table-striped table-responsive">
                <tr>
                    <th>Category</th>
                    <th colspan="2">Library</th>
                    <th>License</th>
                    <th>Link</th>
                </tr>
                <tr>
                    <td>Client Framework</td>
                    <td><img src="/static/assets/brand/bootstrap-solid.svg" width="30" height="30" alt=""></td>
                    <td>Bootstrap</td>
                    <td><a href="https://github.com/twbs/bootstrap/blob/master/LICENSE">MIT License</a></td>
                    <td><a href="https://http://getbootstrap.com/">getbootstrap.com</a></td>
                </tr>
                <tr>
                    <td>Client Icons</td>
                    <td><i class="fa fa-font-awesome" aria-hidden="true"></i></td>
                    <td>Font Awesome by Dave Gandy</td>
                    <td><a href="http://fontawesome.io/license/">MIT License / SIL OFL 1.1</a></td>
                    <td><a href="http://fontawesome.io/">fontawesome.io</a></td>
                </tr>
                <tr>
                    <td>Client Misc</td>
                    <td></td>
                    <td>Timeago</td>
                    <td><a href="https://github.com/rmm5t/jquery-timeago/blob/master/LICENSE.txt">MIT License</a></td>
                    <td><a href="http://timeago.yarp.com/">timeago.yarp.com</a></td>
                </tr>
                <tr>
                    <td>Server Toolkit</td>
                    <td></td>
                    <td>Gorilla</td>
                    <td><a href="https://github.com/gorilla/mux/blob/master/LICENSE">New BSD License</a></td>
                    <td><a href="http://www.gorillatoolkit.org">www.gorillatoolkit.org</a></td>
                </tr>
                <tr>
                    <td>Server Library</td>
                    <td></td>
                    <td>Golang package norm</td>
                    <td><a href="https://golang.org/LICENSE">BSD License</a></td>
                    <td><a href="https://godoc.org/golang.org/x/text/unicode/norm">godoc.org</a></td>
                </tr>
                <tr>
                    <td>Server Routing</td>
                    <td></td>
                    <td>httptreemux</td>
                    <td><a href="https://github.com/dimfeld/httptreemux/blob/master/LICENSE">MIT License</a></td>
                    <td><a href="https://github.com/dimfeld/httptreemux">github.com/dimfeld/httptreemux</a></td>
                </tr>
                <tr>
                    <td>Server Middleware</td>
                    <td></td>
                    <td>negroni</td>
                    <td><a href="https://github.com/urfave/negroni/blob/master/LICENSE">MIT License</a></td>
                    <td><a href="https://github.com/urfave/negroni">github.com/urfave/negroni</a></td>
                </tr>
                <tr>
                    <td>Configuration</td>
                    <td></td>
                    <td>congo</td>
                    <td>
                        <a href="https://gitlab.com/SilentTeaCup/congo/blob/master/LICENSE">
                            MPL (Mozilla Public License)
                        </a>
                    </td>
                    <td><a href="https://gitlab.com/SilentTeaCup/congo">gitlab.com/SilentTeaCup/congo</a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
{{end}}


