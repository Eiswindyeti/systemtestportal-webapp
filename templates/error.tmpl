{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Landing Page">
    <meta name="author" content="guertlms">

    <title>System Test Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="/static/libraries/bootstrap-4.0.0-beta-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/static/css/stp-custom.css" rel="stylesheet" type="text/css">

    <!-- Icons -->
    <link href="/static/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

    <!-- jQuery -->
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="/static/assets/js/vendor/jquery-3.2.1.min.js"><\/script>')
    </script>


    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/static/img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/static/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/static/img/favicons/favicon-16x16.png">
    <link rel="manifest" href="/static/img/favicons/manifest.json">
    <link rel="shortcut icon" href="/static/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/static/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">


    <!-- timeAgo -->
    <script src="/static/assets/js/jquery.timeago.js" type="text/javascript"></script>

</head>

<body>
<div id="mainContent" class="container-fluid">
    <div class="row justify-content-center">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-12 col-md-8">
                    <p class="text-center">
                        <img src="/static/img/STP-Logo.png" class="w-50 img-fluid image-responsive">
                    </p>
                    <h1 class="text-center display-3">{{ .StatusCode }}</h1>
                    <hr class="my-4">
                    <h4 class="text-center">{{ .Title }}</h4>
                    <p class="text-center">
                        {{ .Message }}<br>
                        <a href="javascript:history.back()">Go Back</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<script src="/static/assets/js/vendor/popper.min.js"></script>
<script src="/static/libraries/bootstrap-4.0.0-beta-dist/js/bootstrap.min.js"></script>

<!-- Placed at the end of the document so the pages load faster -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/static/assets/js/ie10-viewport-bug-workaround.js"></script>

<!-- Browser History Functionality -->
<script>
    jQuery(document).ready(function ($) {
        if (window.history && window.history.pushState) {
            $(window).on('popstate', function () {
                window.location.reload();
            });
        }
    });
    <!-- Remove potential whitespace used for indentation from textareas -->
    $('document').ready(function () {
        $('textarea').each(function () {
                    $(this).val($(this).val().trim());
                }
        );
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html>