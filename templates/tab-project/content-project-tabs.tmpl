{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
                <div class="row d-sm-flex d-none mt-2 mb-4">
                    <div class="col-sm-2 d-none d-sm-block d-print-none">
                        <div class="d-flex project-small-thumbnail" id="projectImage">
                            <img src="/static/img/placeholder.png" alt="project logo">
                            <div class="project-thumbnail-background"></div>
                            <div class="project-thumbnail-content"></div>
                            <div id="project-thumbnail-letter">P</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-10">
                        <div class="card-block">
                            <div class="d-none float-right badge badge-secondary d-print-block"><span>created </span>{{ .Project.CreationDate }}</div>
                            <div class="d-none d-sm-block float-right badge badge-secondary d-print-none"><span>created </span><time class="timeago" datetime="">{{ .Project.CreationDate }}</time></div>
                            <h4 class="card-title">{{ .Project.Owner }}/{{ .Project.Name }}</h4>
                            <p class="card-text font-italic">{{ .Project.Description }}</p>
                        </div>
                    </div>
                </div>

                <!-- Nav tabs -->
                <nav class="mobile-full-width d-print-none" role="navigation">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This feature will be available in a future version">
                            <a class="nav-link disabled" id="tabButtonDashboard" data-toggle="tab" href="" role="tab">Dashboard</a>
                        </li>
                        <li class="nav-item hide-5" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This feature will be available in a future version">
                            <a class="nav-link disabled" id="tabButtonActivity" data-toggle="tab" href="" role="tab">Activity</a>
                        </li>
                        <li class="nav-item hide-4">
                            <a class="nav-link" id="tabButtonTestCases" data-toggle="tab" href="" role="tab">Test Cases</a>
                        </li>
                        <li class="nav-item hide-3">
                            <a class="nav-link" id="tabButtonTestSequences" data-toggle="tab" href="" role="tab">Test Sequences</a>
                        </li>
                        <li class="nav-item hide-2">
                            <a class="nav-link" id="tabButtonProtocols" data-toggle="tab" href="" role="tab">Protocols</a>
                        </li>
                        <li class="nav-item hide-1">
                            <a class="nav-link" id="tabButtonMembers" data-toggle="tab" href="" role="tab">Members</a>
                        </li>
                    {{ if .Member }}
                        <li class="nav-item hide-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
                            <a class="nav-link" id="tabButtonSettings" data-toggle="tab" href="" role="tab">Settings</a>
                        </li>
                    {{else}}
                        <li class="nav-item" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Sign in to access the settings">
                            <a class="nav-link disabled" href="#" id="tabButtonSettings">Settings</a>
                        </li>
                    {{ end }}
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" id="more" href="" role="button">More</a>
                            <div class="dropdown-menu tab-collapse-menu">
                                <span class="dropdown-item" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This feature will be available in a future version">
                                    <a class="show-5 disabled cursor-not-clickable" data-toggle="tab" id="menuButtonActivity">Activity</a>
                                </span>
                                <a class="dropdown-item show-4" data-toggle="tab" id="menuButtonTestCases">Test Cases</a>
                                <a class="dropdown-item show-3" data-toggle="tab" id="menuButtonTestSequences">Test Sequences</a>
                                <a class="dropdown-item show-2" data-toggle="tab" id="menuButtonProtocols">Protocols</a>
                                <a class="dropdown-item show-1 " data-toggle="tab" id="menuButtonMembers">Members</a>
                            {{ if .Member }}
                                <span class="dropdown-item" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="This feature will be available in a future version">
                                    <a class="show-1" data-toggle="tab" id="menuButtonSettings">Settings</a>
                                </span>
                                {{ end }}
                            </div>
                        </li>
                        <li class="nav-item ml-auto">
                            <a class="nav-link disabled" id="tabButtonHelp" data-toggle="modal"
                               data-target="#helpModal" href="" role="tab">
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                            </a>

                        </li>
                        <li id="printerIcon" class="nav-item ml-0 d-none">
                            <a class="nav-link disabled" id="tabButtonPrint" onClick="printer();" role="tab" style="cursor: pointer;">
                                <i class="fa fa-print" aria-hidden="true"></i>
                            </a>

                        </li>
                    </ul>
                </nav>
                <!-- Tab panes -->
                <div class="tab-content mobile-full-width">
                    <div class="tab-pane active" id="tabarea" role="tabpanel">
                        {{template "tab-content" . }}
                    </div>
                </div>

                <script src="/static/js/project/project-tabs.js"></script>
        <script>
            initializeProjectTabClickListener();
        </script>
{{end}}


