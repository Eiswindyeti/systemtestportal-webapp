{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tools</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            In this view you can change the details of a test case.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Buttons</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Abort</span>
                            </button>
                        </td>
                        <td>Abort editing and get back to the test case details.</td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-success" type="button">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> Save</span>
                            </button>
                        </td>
                        <td>Confirm changes, save them and return back to the details.</td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Form Fields</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Case Name</strong>
                        </td>
                        <td>Enter a short but descriptive name for the test case.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Case Description</strong>
                        </td>
                        <td>Describe the purpose of that test case in some sentences.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Case Conditions</strong>
                        </td>
                        <td>State some conditions that must be fulfilled prior to test case execution.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Test Steps</strong>
                        </td>
                        <td>Add some test steps here.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Variants & Versions</strong>
                        </td>
                        <td>Select some versions under which the test case can be executed for each relevant variant.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Estimated Test Time (hh:mm)</strong>
                        </td>
                        <td>Enter an estimated amount of time that is needed to execute the whole test.</td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-testCase-save" tabindex="-1" role="dialog" aria-labelledby="modal-testCase-save-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-testCase-save-label">Save Test Case</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <!-- don't break the following two lines, otherwise the placeholder will not be displayed properly-->
                    <textarea class="form-control" id="inputCommitMessage" rows="4"
                              placeholder="Why did you edit the test case? (Optional)"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <div class="checkbox mr-auto">
                    <label><input type="checkbox" id="minorUpdate">
                        <abbr title="This is only a minor edit">Minor Change</abbr>
                    </label>
                </div>
                <button id="buttonSave" type="button" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-testStep" tabindex="-1" role="dialog" aria-labelledby="modal-testStep-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-testStep-label">Test Step</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body" id="testStep">
                <div class="form-group">
                            <textarea class="form-control" id="inputTestStepAction" rows="4"
                                      placeholder="What action shall be performed?"></textarea>
                </div>
                <div class="form-group">
                            <textarea class="form-control" id="inputTestStepExpectedResult" rows="4"
                                      placeholder="Which result is expected?"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button id="buttonSaveTestStep" type="button" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestCases">
    <form id="testCaseEdit">
        <nav class="navbar navbar-light action-bar p-3">
            <div class="input-group flex-nowrap">
                <button class="btn btn-secondary" id="buttonAbortEdit">
                    <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> Abort</span>
                </button>
                <button class="btn btn-success ml-2" data-toggle="modal" onclick="checkForChanges()" type="button">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> Save</span>
                </button>
            </div>
        </nav>
        <div class="row tab-side-bar-row">
            <div class="col-md-9 p-3">
                <h4 class="mb-3">Edit {{ .TestCase.Name }}</h4>
                <div class="form-group">
                    <label for="inputTestCaseName"><strong>Test Case Name</strong></label>
                    <input class="form-control" id="inputTestCaseName"
                           placeholder="Enter test case name" value="{{ .TestCase.Name }}" required>
                </div>
                {{ with .TestCaseVersion }}
                <div class="form-group">
                    <label for="inputTestCaseDescription"><strong>Test Case Description</strong></label>
                    <textarea class="form-control" id="inputTestCaseDescription" rows="4"
                              placeholder="Describe the test case">{{ .Description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="inputTestCasePreconditions"><strong>Test Case Conditions</strong></label>
                    <textarea class="form-control" id="inputTestCasePreconditions" rows="4"
                              placeholder="Enter preconditions">{{ .Preconditions }}</textarea>
                </div>
                <div class="form-group">
                    <label><strong>Test Steps</strong></label>
                    <button type="button" class="btn btn-sm btn-primary ml-auto mb-2" id="buttonAddTestStep"
                            data-toggle="modal" data-target="#modal-teststep-edit">
                        Add Test Step
                    </button>
                    {{ if .Steps }}
                    <ol class="list-group" id="testStepsAccordion" data-children=".li">
                        {{ range $index, $element := .Steps }}
                        <li class="list-group-item" id="testStep{{ $index }}">
                            <span id="ActionField" data-toggle="collapse" data-parent="#testStepsAccordion"
                                  href="#testStepsAccordion{{ $index }}"
                                  aria-expanded="true" aria-controls="testStepsAccordion{{ $index }}">
                                {{ .Action }}
                            </span>
                            <div id="testStepsAccordion{{ $index }}" class="collapse" role="tabpanel">
                                <span id="ExpectedResultField" class="text-muted">{{ .ExpectedResult }}</span>
                                <button type="button" class="btn btn-danger ml-2 list-line-item btn-sm buttonDeleteTestStep"
                                        id="{{ $index }}" >
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    <span class="d-none d-sm-inline"> Delete</span>
                                </button>
                                <button type="button" class="btn btn-danger ml-2 d-none list-line-item btn-sm buttonDeleteTestStepConfirm"
                                        id="{{ $index }}" data-toggle="modal"
                                        data-target="#deleteModal">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                    <span class="d-none d-sm-inline"> Confirm delete</span>
                                </button>
                                <button type="button" class="btn btn-primary ml-auto list-line-item btn-sm buttonEditTestStep"
                                        id="{{ $index }}">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    <span class="d-none d-sm-inline"> Edit</span>
                                </button>
                            </div>
                        </li>
                        {{ end }}
                    </ol>
                    {{ else }}
                    <p class="text-muted">No Test Steps</p>
                    <ol class="list-group" id="testStepsAccordion" data-children=".li"></ol>
                    {{ end }}
                    {{template "modal-teststep-edit" .}}
                </div>
                {{ end }}
            </div>
            <div class="col-md-3 p-3 tab-side-bar">
                <div class="form-group">
                    <label><strong>Applicability</strong>
                        <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal-manage-versions" >
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                        </button>
                    </label>
                    {{ if .Project.Variants }}
                    <div id="somevariants" class="form-group">
                    {{ else }}
                    <div id="somevariants" class="form-group d-none">
                    {{ end }}
                        <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                            <label for="inputTestCaseSUTVariants">
                                <strong>Variants</strong>
                            </label>
                        </div>
                        <div class="col-10 col-sm-11 col-md-12 col-lg-12">
                            <select class="custom-select mb-2" id="inputTestCaseSUTVariants">
                            </select>
                        </div>
                        <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                            <label for="inputTestCaseSUTVersions">
                                <strong>Versions</strong>
                            </label>
                        </div>
                        <div class="form-group col-10 col-sm-11 col-md-12 col-lg-12">
                            <select multiple class="form-control" id="inputTestCaseSUTVersions">
                            </select>
                        </div>
                    </div>
                    {{ if .Project.Variants }}
                    <div id="novariant" class="form-group d-none">
                    {{ else }}
                    <div id="novariant" class="form-group">
                    {{ end }}
                        <p class="text-muted">The system-under-test does not have any versions yet. Click on the icon above to manage the versions.</p>
                    </div>
                </div>
                <div class="form-group mt-4">
                    <label for="inputHours"><strong>Estimated Test Time (hh:mm)</strong></label>
                    <div class="row m-0">
                        <div class="col-lg-6 col-md-12 col-sm-3 col-6 p-0 pb-md-2">
                            <div class="input-group mb-2 mb-sm-0 ">
                                <input type="number" class="form-control pt-0 pb-0" id="inputHours" placeholder="h"
                                       min="0" oninput="checkHours(this)" step="1"
                                       {{ if or (gt .DurationHours 0) (gt .DurationMin 0) }}
                                       value="{{ .DurationHours }}" {{ end }}>
                                <div class="btn-group-vertical">
                                    <button type="button" id="hour-plus" class="input-group-addon btn btn-primary">
                                        <span>&#x2b;</span>
                                    </button>
                                    <button type="button" id="hour-minus" class="input-group-addon btn btn-primary">
                                        <span>&#x2212;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-3 col-6 p-0 pl-2 pl-md-0 pl-lg-2">
                            <div class="input-group mb-2 mb-sm-0 ">
                                <input type="number" class="form-control pt-0 pb-0" id="inputMinutes" placeholder="min"
                                       min="0" max="59" oninput="checkMins(this)" step="1"
                                       {{ if or (gt .DurationHours 0) (gt .DurationMin 0) }}
                                       value="{{ .DurationMin }}" {{ end }}>
                                <div class="btn-group-vertical">
                                    <button type="button" id="minute-plus" class="input-group-addon btn btn-primary">
                                        <span>&#x2b;</span>
                                    </button>
                                    <button type="button" id="minute-minus" class="input-group-addon btn btn-primary">
                                        <span>&#x2212;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{template "modal-manage-versions" . }}
        </div>
    </form>
    <script src="/static/js/project/testcases.js"></script>
    <script src="/static/js/util/common.js"></script>
    <script src="/static/js/project/sut-versions.js"></script>
    <script>
        assignButtonsTestCase();

        // Send a request to get the sut variants and versions of the test case
        var urlJson = getTestURL().appendSegment("json").toString();

        xmlhttpJSON = new XMLHttpRequest();
        xmlhttpJSON.open("GET", urlJson, true);
        xmlhttpJSON.send();
        xmlhttpJSON.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                testcase = JSON.parse(this.responseText);
                var testCaseVersionIndex = (testcase.TestCaseVersions).length - {{ .TestCaseVersion.VersionNr }};
                // Save the versions of the loaded test case in selected versions.
                // These versions are selected by default in the list of versions.
                selectedVersions = testcase.TestCaseVersions[testCaseVersionIndex].Variants;

                /*
                 * Deep copy the variants and versions of a test case. They are needed for adding deprecated sut-versions
                 * to the version list. Deprecated sut-versions are versions that no longer exist in the system under test
                 * but still exist in the test case
                */
                testCaseVersionData = jQuery.extend(true, {}, selectedVersions);

                // Update the version list with the versions of the selected variant
                updateVersionSelectionList(null);
            }
        };
    </script>
</div>
<div class="invisible" id="emptyTestStepElement">
<li class="list-group-item" id="testStep00000">
    <span id="ActionField" data-toggle="collapse" data-parent="#testStepsAccordion"
          href="#testStepsAccordion00000"
          aria-expanded="true" aria-controls="testStepsAccordion00000">
    </span>
    <div id="testStepsAccordion00000" class="collapse" role="tabpanel">
        <span id="ExpectedResultField" class="text-muted"></span>
        <button type="button" class="btn btn-danger ml-2 list-line-item btn-sm buttonDeleteTestStep">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
            <span class="d-none d-sm-inline">Delete</span>
        </button>
        <button type="button" class="btn btn-danger ml-2 d-none list-line-item btn-sm buttonDeleteTestStepConfirm"
                data-toggle="modal"
                data-target="#deleteModal">
            <i class="fa fa-check" aria-hidden="true"></i>
            <span class="d-none d-sm-inline"> Confirm delete</span>
        </button>
        <button type="button" class="btn btn-primary ml-auto list-line-item btn-sm buttonEditTestStep">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            <span class="d-none d-sm-inline"> Edit</span>
        </button>
    </div>
</li>
</div>
{{ end }}


