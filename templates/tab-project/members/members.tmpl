{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Member List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            <ul>
                                <li>See the list of members in this project.</li>
                            {{ if .Member }}
                                <li>Add new members (see button below).</li>
                            {{ end }}
                            {{ if .Owner }}
                                <li>Remove members (see button below).</li>
                            {{ end }}

                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Buttons</h5></td>
                    </tr>
                    {{ if .Member }}
                    <tr>
                        <td>
                            <button class="order-3 btn btn-primary align-middle">
                                + <span class="d-none d-sm-inline">Add member</span>
                            </button>
                        </td>
                        <td>Add a user as member to the project.</td>
                    </tr>
                    {{ end }}
                    {{ if .Owner }}
                        <tr>
                            <td>
                                <button class="order-3 btn btn-primary align-middle">
                                    <span class="fa fa-sign-out"></span> <span class="d-none d-sm-inline">Remove member</span>
                                </button>
                            </td>
                            <td>Remove a users membership in this project.</td>
                        </tr>
                    {{ end }}
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{ template "modal-member-assignment" . }}
{{ template "modal-member-remove" . }}
<div class="tab-card card" id="tabMembers">
    <nav class="navbar-light action-bar p-3">
        {{ if .Member }}
            <button class="order-3 btn btn-primary align-middle mr-2 float-left" id="buttonAddMember"
                    data-toggle="modal"  data-target="#modal-member-assignment">
                + <span class="d-none d-sm-inline">Add member</span>
            </button>
        {{ end }}
        {{ if .Owner }}
            <button class="order-3 btn btn-secondary align-middle float-left" id="buttonRemoveMember"
                    data-toggle="modal" data-target="#modal-member-remove">
                <span class="fa fa-sign-out"></span> <span class="d-none d-sm-inline">Remove member</span>
            </button>
        {{ end }}
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-md-12 p-3">
            <h4 class="mb-3">Members</h4>
            <ul class="list-group">
            {{ range .Members }}
                <li class="list-group-item memberLine" id="{{ .ID }}">
                {{ .DisplayName }}
                </li>
            {{end}}
            </ul>
        </div>
    </div>
    <script src="/static/js/util/common.js"></script>
    <script src='/static/js/modal/select-members.js'></script>
</div>
{{end}}