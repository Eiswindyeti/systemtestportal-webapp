// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

func TestUsersSQLList(t *testing.T) {
	type args struct {
		s Users
	}

	type result struct {
		us  []*user.User
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s: usersSQL{emptyEngine(t)},
			},
			w: result{
				us:  nil,
				err: nil,
			},
		},
		{
			name: "Store with users",
			a: args{
				s: usersSQL{engineWithSampleData(t)},
			},
			w: result{
				us: []*user.User{
					{
						Name:             "Franz",
						DisplayName:      "Frain",
						EMail:            "f.thor@example.org",
						RegistrationDate: time.Date(2017, time.July, 12, 9, 6, 28, 0, time.UTC).In(time.Local),
					},
					{
						Name:             "theo",
						DisplayName:      "Theodor Heuss",
						EMail:            "theo.heu@example.org",
						RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
					},
				},
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.us, got.err = tc.a.s.List()

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("usersSQL.List() = %v, want %v", got, tc.w)
			}
		})
	}
}

func TestUsersSQLGet(t *testing.T) {
	type args struct {
		s  Users
		id id.ActorID
	}

	type result struct {
		u   *user.User
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:  usersSQL{emptyEngine(t)},
				id: "theo",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "User not in store",
			a: args{
				s:  usersSQL{engineWithSampleData(t)},
				id: "Hans",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "User in store",
			a: args{
				s:  usersSQL{engineWithSampleData(t)},
				id: "theo",
			},
			w: result{
				u: &user.User{
					Name:             "theo",
					DisplayName:      "Theodor Heuss",
					EMail:            "theo.heu@example.org",
					RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
				},
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.u, got.ex, got.err = tc.a.s.Get(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("usersSQL.Get(%v) = %v, want %v", tc.a.id, got, tc.w)
			}
		})
	}
}

func TestUsersSQLGetByMail(t *testing.T) {
	type args struct {
		s    Users
		mail string
	}

	type result struct {
		u   *user.User
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:    usersSQL{emptyEngine(t)},
				mail: "theo.heu@example.org",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "User not in store",
			a: args{
				s:    usersSQL{engineWithSampleData(t)},
				mail: "hans.wurst@example.org",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "User in store",
			a: args{
				s:    usersSQL{engineWithSampleData(t)},
				mail: "theo.heu@example.org",
			},
			w: result{
				u: &user.User{
					Name:             "theo",
					DisplayName:      "Theodor Heuss",
					EMail:            "theo.heu@example.org",
					RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
				},
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.u, got.ex, got.err = tc.a.s.GetByMail(tc.a.mail)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("usersSQL.GetByMail(%v) = %v, want %v", tc.a.mail, got, tc.w)
			}
		})
	}
}

func TestUsersSQLValidate(t *testing.T) {
	type args struct {
		s        Users
		id       string
		password string
	}

	type result struct {
		u   *user.User
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store with name",
			a: args{
				s:  usersSQL{emptyEngine(t)},
				id: "theo",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Empty store with email",
			a: args{
				s:  usersSQL{emptyEngine(t)},
				id: "theo.heu@example.org",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "User id not in store",
			a: args{
				s:  usersSQL{engineWithSampleData(t)},
				id: "Hans",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "User email not in store",
			a: args{
				s:  usersSQL{engineWithSampleData(t)},
				id: "hans.wurst@example.org",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Wrong password for id in store",
			a: args{
				s:        usersSQL{engineWithSampleData(t)},
				id:       "theo",
				password: "test",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Wrong password for email in store",
			a: args{
				s:        usersSQL{engineWithSampleData(t)},
				id:       "theo.heu@example.org",
				password: "test",
			},
			w: result{
				u:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Correct password for id in store",
			a: args{
				s:        usersSQL{engineWithSampleData(t)},
				id:       "theo",
				password: "test1234",
			},
			w: result{
				u: &user.User{
					Name:             "theo",
					DisplayName:      "Theodor Heuss",
					EMail:            "theo.heu@example.org",
					RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
				},
				ex:  true,
				err: nil,
			},
		},
		{
			name: "Correct password for email in store",
			a: args{
				s:        usersSQL{engineWithSampleData(t)},
				id:       "theo.heu@example.org",
				password: "test1234",
			},
			w: result{
				u: &user.User{
					Name:             "theo",
					DisplayName:      "Theodor Heuss",
					EMail:            "theo.heu@example.org",
					RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
				},
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.u, got.ex, got.err = tc.a.s.Validate(tc.a.id, tc.a.password)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("usersSQL.Validate(%v) = %v, want %v", tc.a.id, got, tc.w)
			}
		})
	}
}

func TestUsersSQLAdd(t *testing.T) {
	type args struct {
		s Users
		u *user.PasswordUser
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name         string
		a            args
		w            result
		shouldCreate bool
	}{
		{
			name: "Empty store",
			a: args{
				s: usersSQL{emptyEngine(t)},
				u: &user.PasswordUser{
					User: user.User{
						Name:             "hans",
						DisplayName:      "Hans Wurst",
						EMail:            "hans.wurst@example.org",
						RegistrationDate: time.Now().Round(time.Second),
					},
					Password: "HALLO HELGA",
				},
			},
			w: result{
				err: nil,
			},
			shouldCreate: true,
		},
		{
			name: "Insert new user",
			a: args{
				s: usersSQL{emptyEngine(t)},
				u: &user.PasswordUser{
					User: user.User{
						Name:             "hans",
						DisplayName:      "Hans Wurst",
						EMail:            "hans.wurst@example.org",
						RegistrationDate: time.Now().Round(time.Second),
					},
					Password: "HALLO HELGA",
				},
			},
			w: result{
				err: nil,
			},
		},
		{
			name: "Update user",
			a: args{
				s: usersSQL{engineWithSampleData(t)},
				u: &user.PasswordUser{
					User: user.User{
						Name:             "theo",
						DisplayName:      "Theodor Heuss",
						EMail:            "theo.heu@example.org",
						RegistrationDate: time.Now().Round(time.Second),
					},
					Password: "te12345st",
				},
			},
			w: result{
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.err = tc.a.s.Add(tc.a.u)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("usersSQL.Add(%#v) = %v, want %v", tc.a.u, got, tc.w)
				t.Logf("%T", got.err)
			}

			if !tc.shouldCreate {
				return
			}

			au, ex, _ := tc.a.s.Get(tc.a.u.ID())
			if !ex {
				t.Error("User was not created")
				return
			}

			if !reflect.DeepEqual(tc.a.u.User, *au) {
				t.Error("Not all information was saved")
				t.Log(tc.a.u.User)
				t.Log(au)
			}
		})
	}
}
