// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

// Projects contains the dummy projects
var Projects = []project.Project{
	{
		Owner:      Users[0].ID(),
		Name:       "DuckDuckGo.com",
		Visibility: visibility.Public,
		Variants: map[string]*project.Variant{
			"Chrome": {
				Name: "Chrome",
				Versions: []project.Version{
					{Name: "v0.2"},
					{Name: "v0.3.1"},
					{Name: "v0.4.6"},
					{Name: "v42.5.25"},
				},
			},

			"Firefox": {
				Name: "Firefox",
				Versions: []project.Version{
					{Name: "v0.1.11"},
					{Name: "v0.2.49"},
					{Name: "v0.4.0"},
					{Name: "v0.4.6"},
				},
			},

			"Microsoft Edge": {
				Name: "Microsoft Edge",
				Versions: []project.Version{
					{Name: "v0.175"},
					{Name: "v0.204"},
					{Name: "v1000"},
					{Name: "v1019"},
				},
			},
		},

		Labels: []project.Label{
			{
				Name:        "High Priority",
				Description: "This test has a high priority",
			},
			{
				Name:        "Low Priority",
				Description: "This test has a low priority",
			},
			{
				Name:        "Elepant",
				Description: "This test has an elepant priority",
			},
			{
				Name:        "Verschiedene",
				Description: "Huehuehuehue",
			},
			{
				Name:        "Obstsalat",
				Description: "Schlupp",
			},
		},
		Members: map[id.ActorID]*user.User{
			id.ActorID("default"): {
				"DeFault",
				"default",
				"default@example.org",
				time.Now().Round(time.Second),
			},
			id.ActorID("admin"): {
				Name:        "admin",
				DisplayName: "admin",
				EMail:       "admin@example.org",
			},
			id.ActorID("alexanderkaiser"): {
				Name:        "alexanderkaiser",
				DisplayName: "Alexander Kaiser",
				EMail:       "alexander.kaiser@gmx.de",
			},
			id.ActorID("simoneraab"): {
				Name:        "simoneraab",
				DisplayName: "Simone Raab",
				EMail:       "simone.raab@gmail.com",
			},
		},
	},
}
