// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

// Cases contains the dummy cases
var Cases = []test.Case{
	{
		Project: Projects[0].ID(),
		Name:    "Change Theme",

		Labels: []project.Label{
			{
				Name:        "High Priority",
				Description: "This test has a high priority",
			},
			{
				Name:        "Low Priority",
				Description: "This test has a low priority",
			},
			{
				Name:        "Elepant",
				Description: "This test has a low priority",
			},
		},

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Change Theme", true),
				VersionNr: 1,

				Description:   "Change the theme of the user interface",
				Preconditions: "DuckDuckGo.com is opened. The third theme is not selected in the settings.",

				Duration: duration.NewDuration(0, 5, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Type \"SystemTestPortal\" into the search bar and press enter.",
						ExpectedResult: "The search results for \"SystemTestPortal\" appear in a list.",
					},
					{
						Index:          2,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:  3,
						Action: "Click on the third theme (the blue-green one).",
						ExpectedResult: "The user interface design changes. The URL addresses are displayed beneath " +
							"the title of the search result. The URL font color changes to green. The icons before " +
							"the URL disappear. The font gets bold.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Websites",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Websites", true),
				VersionNr: 1,

				Description:   "A default keyword-search",
				Preconditions: "DuckDuckGo.com is opened. Nothing is typed into the search bar.",

				Duration: duration.NewDuration(0, 5, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Versions: []project.Version{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: "Type \"SystemTestPortal\" into the search bar and press enter.",
						ExpectedResult: `"SystemTestPortal" is written into the search bar. The search results are ` +
							`listed. Under the search bar "Web" is selected as search result type.`,
					},
					{
						Index:          2,
						Action:         "Click on the search result that leads to systemtestportal.org.",
						ExpectedResult: "www.systemtestportal.org opens.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Images",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Images", true),
				VersionNr: 1,

				Description:   "A image search",
				Preconditions: "DuckDuckGo.com is opened. Nothing is typed into the search bar.",

				Duration: duration.NewDuration(0, 5, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Versions: []project.Version{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: "Type \"portal image\" into the search bar and press enter.",
						ExpectedResult: `A new layout opens with multiple images. Under the search bar "Images" is ` +
							`selected as search result type.`,
					},
					{
						Index: 2,
						Action: "Under the row to select the search result type there is a second row of filters. " +
							"Click on the last filter which says \"All Colors\" and select \"Black and White\".",
						ExpectedResult: "Instead of \"All Color\" there stands now \"Black and White\". " +
							"The search results change. All search results are predominantly black and white.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Definitions",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Definitions", true),
				VersionNr: 1,

				Description:   "A search for a definition",
				Preconditions: "DuckDuckGo.com is opened. Nothing is typed into the search bar.",

				Duration: duration.NewDuration(0, 3, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Versions: []project.Version{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: "Type \"test definition\" into the search bar and press enter.",
						ExpectedResult: "Under the search bar \"Definition\" is selected as search result type. " +
							"On the top is a highlighted area with definitions for \"test\". " +
							"Several search results of websites follow. ",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Change Language",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Change Language", true),
				VersionNr: 1,

				Description: "Change the language settings",
				Preconditions: "DuckDuckGo.com is opened. " +
					"\"Español de España\" is not selected in the language settings of DuckDuckGo.com.",

				Duration: duration.NewDuration(0, 7, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:          2,
						Action:         `Select "other Settings".`,
						ExpectedResult: "A list of adjustable settings is viewed.",
					},
					{
						Index:          3,
						Action:         `Select "Español de España" in the drop down menu on the right of "Language".`,
						ExpectedResult: `The site is translated into Spanish (e.g. Settings changes into "Ajustes").`,
					},
					{
						Index:  4,
						Action: `Scroll to the end of the settings list and click "Guardar y salir".`,
						ExpectedResult: "The site changes back to DuckDuckGo.com. The language viewed here is the " +
							"language you selected in your browser settings.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Install DuckDuckGo.com for Chrome",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Install DuckDuckGo.com for Chrome", true),
				VersionNr: 1,

				Description:   "Add DuckDuckGo.com as a search engine to your browser",
				Preconditions: "DuckDuckGo.com is opened.",

				Duration: duration.NewDuration(0, 8, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					}},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -2),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the arrow down in the bottom center. ",
						ExpectedResult: "The page scrolls down.",
					},
					{
						Index:          2,
						Action:         "Click on the button \"Add DuckDuckGo to Chrome\".",
						ExpectedResult: "The notification \"Add DuckDuckGo for Chrome?\" appears. ",
					},
					{
						Index:  3,
						Action: "Click \"Add extension\". ",
						ExpectedResult: `The notification "DuckDuckGo for Chrome has been added to Chrome..." ` +
							`appears. In the top right corner the icon of DuckDuckGo is viewed.`,
					},
					{
						Index:          4,
						Action:         "Press ALT + G.",
						ExpectedResult: "In the top right corner a pop up appears with a search bar inside.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Change Font Size",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Change Font Size", true),
				VersionNr: 1,

				Description: "Change the font size of the search engine",
				Preconditions: "DuckDuckGo.com is opened." +
					" \"Large\" is selected as font size in the settings.",

				Duration: duration.NewDuration(0, 7, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							0: {Name: "v0.4.6"},
							1: {Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							0: {Name: "v0.4.0"},
							1: {Name: "v0.4.6"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:          2,
						Action:         `Select "other Settings".`,
						ExpectedResult: "A list of adjustable settings is viewed.",
					},
					{
						Index:          3,
						Action:         `Click "Appearance" in the list on the right of the headline "Settings".`,
						ExpectedResult: "A list of other adjustable settings is viewed.",
					},
					{
						Index:          4,
						Action:         `Select "Largest" in the drop down menu on the right of "Font Size".`,
						ExpectedResult: "The font size increases.",
					},
					{
						Index:          5,
						Action:         `Scroll to the end of the settings list and click "Save and Exit".`,
						ExpectedResult: "The site changes back to DuckDuckGo.com.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Map",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Map", true),
				VersionNr: 1,

				Description: "Search a place and view it on a map",
				Preconditions: "DuckDuckGo.com is opened." +
					" Nothing is typed into the search bar.",

				Duration: duration.NewDuration(0, 5, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Versions: []project.Version{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: `Type "Stuttgart Map" into the search bar.`,
						ExpectedResult: "The search results are viewed. At the top is a small map showing stuttgart. " +
							"A list of websites follows.",
					},
					{
						Index:          2,
						Action:         `Select "OpenStreetMap" in the drop down in the bottom right corner of the map.`,
						ExpectedResult: "OpenStreetMap is written next to the small triangle of the drop down menu.",
					},
					{
						Index:          3,
						Action:         "Click on the map.",
						ExpectedResult: "The map opens in full screen mode.",
					},
					{
						Index:  4,
						Action: `Click on "Directions".`,
						ExpectedResult: "OpenStreetMap opens. The coordinates 48.7761, 9.1775 are entered as the " +
							"finish of the route.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Enable Autosuggestion",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Enable Autosuggestion", true),
				VersionNr: 1,

				Description:   "Enable autosuggestion in your settings",
				Preconditions: "DuckDuckGo.com is opened. Autosuggestion is disabled.",

				Duration: duration.NewDuration(0, 6, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							0: {Name: "v0.4.6"},
							1: {Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							0: {Name: "v0.4.0"},
							1: {Name: "v0.4.6"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         `Type "s" into the search bar.`,
						ExpectedResult: "No list of auto-suggestions appears under the search bar.",
					},
					{
						Index:          2,
						Action:         "Click on the menu-button (The three horizontal lines at the top right corner).",
						ExpectedResult: "The sidebar opens.",
					},
					{
						Index:          3,
						Action:         `Select "other Settings".`,
						ExpectedResult: "A list of adjustable settings is viewed.",
					},
					{
						Index:          4,
						Action:         `Click the button "Off" next to "Auto-Suggest".`,
						ExpectedResult: `The color of the button changes. The description of the button changes to "On".`,
					},
					{
						Index:          5,
						Action:         `Scroll to the end of the settings list and click "Save and Exit".`,
						ExpectedResult: "The site changes back to DuckDuckGo.com.",
					},
					{
						Index:          6,
						Action:         `Type "s" into the search bar.`,
						ExpectedResult: "A list of auto-suggestions appears under the search bar.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Search for Recipes",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Recipes", true),
				VersionNr: 2,

				Description: "A search for recipes",
				Preconditions: "DuckDuckGo.com is opened. " +
					" Nothing is typed into the search bar.",

				Duration: duration.NewDuration(0, 3, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Versions: []project.Version{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					},
				},

				Message:      "Correcting a spelling mistake",
				IsMinor:      true,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: `Type "Pizza recipes" into the search bar and press enter.`,
						ExpectedResult: `"Pizza recipes" is written into the search bar. The search results are ` +
							`listed. Under the search bar "Recipes" is selected as search result type.`,
					},
				},
			},
			{
				Case:      id.NewTestID(Projects[0].ID(), "Search for Recipes", true),
				VersionNr: 1,

				Description: "A search for receipts",
				Preconditions: "DuckDuckGo.com is opened." +
					" Nothing is typed into the search bar.",

				Duration: duration.NewDuration(0, 3, 0),
				Variants: map[string]*project.Variant{
					"Chrome": {
						Name: "Chrome",
						Versions: []project.Version{
							{Name: "v0.2"},
							{Name: "v0.3.1"},
							{Name: "v0.4.6"},
							{Name: "v42.5.25"},
						},
					},
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					},
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Versions: []project.Version{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					},
				},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(0, 0, -1),

				Steps: []test.Step{
					{
						Index:  1,
						Action: `Type "Pizza recipes" into the search bar and press enter.`,
						ExpectedResult: `"Pizza recipes" is written into the search bar. The search results are ` +
							`listed. Under the search bar "Recipes" is selected as search result type.`,
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Install DuckDuckGo.com for Firefox",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Install DuckDuckGo.com for Firefox", true),
				VersionNr: 1,

				Description:   "Add DuckDuckGo.com as a search engine to your browser",
				Preconditions: "DuckDuckGo.com is opened.",

				Duration: duration.NewDuration(0, 3, 0),
				Variants: map[string]*project.Variant{
					"Firefox": {
						Name: "Firefox",
						Versions: []project.Version{
							{Name: "v0.1.11"},
							{Name: "v0.2.49"},
							{Name: "v0.4.0"},
							{Name: "v0.4.6"},
						},
					}},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().Add(-time.Hour * 2),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the arrow down in the bottom center. ",
						ExpectedResult: "The page scrolls down.",
					},
					{
						Index:          2,
						Action:         `Click on the button "Add DuckDuckGo to Firefox".`,
						ExpectedResult: `The notification "Firefox blocked this website...." appears.`,
					},
					{
						Index:          3,
						Action:         `Click "Permit".`,
						ExpectedResult: `The notification "Add DuckDuckGo Plus? ..." appears.`,
					},
					{
						Index:  4,
						Action: `Click "Add".`,
						ExpectedResult: `The notification "DuckDuckGo for Chrome has been added to Chrome..." ` +
							`appears. In the top right corner the icon of DuckDuckGo is viewed.`,
					},
					{
						Index:          5,
						Action:         "Press ALT + G.",
						ExpectedResult: "In the top right corner a pop up appears with a search bar inside.",
					},
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Install DuckDuckGo.com for Microsoft Edge",

		TestCaseVersions: []test.CaseVersion{
			{
				Case:      id.NewTestID(Projects[0].ID(), "Install DuckDuckGo.com for Microsoft Edge", true),
				VersionNr: 1,

				Description:   "Add DuckDuckGo.com as a search engine to your browser",
				Preconditions: "DuckDuckGo.com is opened.",

				Duration: duration.NewDuration(0, 3, 0),
				Variants: map[string]*project.Variant{
					"Microsoft Edge": {
						Name: "Microsoft Edge",
						Versions: []project.Version{
							{Name: "v0.175"},
							{Name: "v0.204"},
							{Name: "v1000"},
							{Name: "v1019"},
						},
					}},

				Message:      "Initial test case created",
				IsMinor:      false,
				CreationDate: time.Now().Add(-time.Hour * 2),

				Steps: []test.Step{
					{
						Index:          1,
						Action:         "Click on the arrow down in the bottom center. ",
						ExpectedResult: "The page scrolls down.",
					},
					{
						Index:          2,
						Action:         `Click on the button "Add DuckDuckGo to Edge".`,
						ExpectedResult: `The site "Take Back Your Privacy! ..." is displayed.`,
					},
					{
						Index: 3,
						Action: `Follow the instructions on this site. Open a new tap and type "SystemTestPortal" ` +
							`into the search bar.`,
						ExpectedResult: `The search engine you used was DuckDuckGo (i.e the URL in the search bar ` +
							`starts with "https://duckduckgo.com/").`,
					},
				},
			},
		},
	},
}
