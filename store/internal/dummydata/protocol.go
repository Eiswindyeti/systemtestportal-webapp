// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

// CaseProtocols contains the protocols of cases
var CaseProtocols = []test.CaseExecutionProtocol{
	// Protocols for test case "Change theme"
	{
		TestVersion: Cases[0].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The search results are viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The sidebar opens.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The icons didn't disappear.",
				Result:           test.Fail,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[0].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Chrome",
		SUTVersion: "v0.4.6",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The search results are viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The sidebar opens.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The user interface changes into the blue-green design.",
				Result:           test.PassWithComment,
				Comment:          "The color is light green not dark green as it is supposed to.",
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.PassWithComment,
		Comment:         "Change the green color from light green to dark green.",
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[0].TestCaseVersions[0].ID(),
		ProtocolNr:  3,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The search results are viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The sidebar opens.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The user interface changes into the blue-green design.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result: test.Pass,

		OtherNeededTime: duration.NewDuration(0, 2, 0),
	},
	{
		TestVersion: Cases[0].TestCaseVersions[0].ID(),
		ProtocolNr:  4,

		SUTVariant: "Chrome",
		SUTVersion: "v42.5.25",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The search results are viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The sidebar opens.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The user interface changes into the blue-green design.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Search for Websites"
	{
		TestVersion: Cases[1].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Chrome",
		SUTVersion: "v0.2",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "as expected",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The website of \"SystemTestPortal\" is viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[1].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.6",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "as expected",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The website of \"SystemTestPortal\" is viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[1].TestCaseVersions[0].ID(),
		ProtocolNr:  3,

		SUTVariant: "Firefox",
		SUTVersion: "v0.2.49",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Change language"
	{
		TestVersion: Cases[4].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.PassWithComment,
				Comment:    `There is no "Español de España" to select. Only "Español"`,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.PassWithComment,
		Comment:         "check language selection",
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[4].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[4].TestCaseVersions[0].ID(),
		ProtocolNr:  3,

		SUTVariant: "Chrome",
		SUTVersion: "v42.5.25",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The language always changes to French, independent of which language is selected.",
				Result:           test.Fail,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.NotAssessed,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[4].TestCaseVersions[0].ID(),
		ProtocolNr:  4,

		SUTVariant: "Chrome",
		SUTVersion: "v42.5.25",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.NotAssessed,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Change font size"
	{
		TestVersion: Cases[6].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[6].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The font size decreases",
				Result:           test.Fail,
				Comment:          "The actions for Medium and Large, Small and Largest have to be swapped.",
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		Comment:         "The font size is implemented the opposite way around",
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[6].TestCaseVersions[0].ID(),
		ProtocolNr:  3,

		SUTVariant: "Chrome",
		SUTVersion: "v0.4.6",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The font size decreases",
				Result:           test.Fail,
				Comment:          "The actions for Medium and Large, Small and Largest have to be swapped.",
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		Comment:         "The font size is implemented the opposite way around",
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[6].TestCaseVersions[0].ID(),
		ProtocolNr:  4,

		SUTVariant: "Chrome",
		SUTVersion: "v42.5.25",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Enable auto-suggestion"
	{
		TestVersion: Cases[8].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[8].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.4.0",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[8].TestCaseVersions[0].ID(),
		ProtocolNr:  3,

		SUTVariant: "Chrome",
		SUTVersion: "v0.4.6",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[8].TestCaseVersions[0].ID(),
		ProtocolNr:  4,

		SUTVariant: "Chrome",
		SUTVersion: "v42.5.25",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Search for Images"
	{
		TestVersion: Cases[2].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.1.11",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "Web not Images is not selected under the search bar. A list of Websites appears. " +
					"There are no images as search results.",
				Result:     test.Fail,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.NotAssessed,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[2].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.1.11",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "Web not Images is not selected under the search bar.",
				Result:           test.Fail,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.NotAssessed,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[2].TestCaseVersions[0].ID(),
		ProtocolNr:  3,

		SUTVariant: "Firefox",
		SUTVersion: "v0.2.49",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The search results are good, but Web instead of Images is selected under the " +
					"search bar.",
				Result:     test.Fail,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.NotAssessed,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Install DuckDuckGo.com for Firefox"
	{
		TestVersion: Cases[10].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.2.49",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The page scrolls down",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The the notification appeared.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The the notification appeared.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The icon is viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The pop up appeared.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Install DuckDuckGo.com for Chrome"
	{
		TestVersion: Cases[5].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Chrome",
		SUTVersion: "v0.3.1",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The page scrolls down",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The the notification appeared.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The the notification appeared.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The icon is viewed.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The pop up appeared.",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Install DuckDuckGo.com for Microsoft Edge"
	{
		TestVersion: Cases[11].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Microsoft Edge",
		SUTVersion: "v0.175",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The page scrolls down",
				Result:           test.Pass,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The test case doesn't define the result for Microsoft Edge.",
				Result:           test.Fail,
				Comment:          "This is a problem in the test case not the SUT.",
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The website is displayed.",
				Result:           test.PassWithComment,
				Comment: "In this case you don't add DuckDuckGo as a plugin to your browser, but rather you set it " +
					"as a default search engine. This should be mentioned in the test case description.",
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.PassWithComment,
		Comment:         `Add "change default search engine" to the test case description (see comment on test step 2).`,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Search for Definitions"
	{
		TestVersion: Cases[3].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.1.11",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.PassWithComment,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.PassWithComment,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[3].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.2.49",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The search results are good, but Web is selected under the search bar. " +
					"Definitions does not appear in the row.",
				Result:     test.Fail,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Map"
	{
		TestVersion: Cases[7].TestCaseVersions[0].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.1.11",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[7].TestCaseVersions[0].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.2.49",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "Nothing happens.",
				Result:           test.Fail,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.NotAssessed,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[7].TestCaseVersions[0].ID(),
		ProtocolNr:  3,

		SUTVariant: "Firefox",
		SUTVersion: "v0.2.49",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
			{
				ObservedBehavior: "The coordinates aren't entered automatically.",
				Result:           test.Fail,
				NeededTime:       duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},

	// Protocols for test case "Search for Recipes"
	{
		TestVersion: Cases[9].TestCaseVersions[1].ID(),
		ProtocolNr:  1,

		SUTVariant: "Firefox",
		SUTVersion: "v0.1.11",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				Result:     test.Pass,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Pass,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
	{
		TestVersion: Cases[9].TestCaseVersions[1].ID(),
		ProtocolNr:  2,

		SUTVariant: "Firefox",
		SUTVersion: "v0.2.49",

		ExecutionDate: time.Now().Round(time.Second),

		StepProtocols: []test.StepExecutionProtocol{
			{
				ObservedBehavior: "The search results are good, but Web is selected under the search bar. Recipes " +
					"does not appear in the row.",
				Result:     test.Fail,
				NeededTime: duration.NewDuration(0, 5, 0),
			},
		},

		Result:          test.Fail,
		OtherNeededTime: duration.NewDuration(0, 5, 0),
	},
}

//SequenceProtocols returns the dummydata-sequenceProtocols
func SequenceProtocols() []test.SequenceExecutionProtocol {

	result := []test.SequenceExecutionProtocol{
		// Protocols for test sequence "Settings"
		{
			TestVersion: Sequences[1].SequenceVersions[0].ID(),
			ProtocolNr:  1,

			SUTVariant: "Firefox",
			SUTVersion: "v0.4.0",

			ExecutionDate: time.Now().Round(time.Second),
		},
		{
			TestVersion: Sequences[1].SequenceVersions[0].ID(),
			ProtocolNr:  2,

			SUTVariant: "Chrome",
			SUTVersion: "v42.5.25",

			ExecutionDate: time.Now().Round(time.Second),
		},

		// Protocols for test sequence "Searching"
		{
			TestVersion: Sequences[0].SequenceVersions[0].ID(),
			ProtocolNr:  1,

			SUTVariant: "Firefox",
			SUTVersion: "v0.1.11",

			ExecutionDate: time.Now().Round(time.Second),
		},
		{
			TestVersion: Sequences[0].SequenceVersions[0].ID(),
			ProtocolNr:  2,

			SUTVariant: "Firefox",
			SUTVersion: "v0.2.49",

			ExecutionDate: time.Now().Round(time.Second),
		},
		{
			TestVersion: Sequences[0].SequenceVersions[0].ID(),
			ProtocolNr:  3,

			SUTVariant: "Firefox",
			SUTVersion: "v0.2.49",

			ExecutionDate: time.Now().Round(time.Second),
		},
	}
	result[0].AddCaseExecutionProtocol(CaseProtocols[0])
	result[0].AddCaseExecutionProtocol(CaseProtocols[7])
	result[0].AddCaseExecutionProtocol(CaseProtocols[11])
	result[0].AddCaseExecutionProtocol(CaseProtocols[15])
	result[1].AddCaseExecutionProtocol(CaseProtocols[3])
	result[1].AddCaseExecutionProtocol(CaseProtocols[10])
	result[1].AddCaseExecutionProtocol(CaseProtocols[14])
	result[1].AddCaseExecutionProtocol(CaseProtocols[18])
	result[2].AddCaseExecutionProtocol(CaseProtocols[5])
	result[2].AddCaseExecutionProtocol(CaseProtocols[19])
	result[2].AddCaseExecutionProtocol(CaseProtocols[25])
	result[2].AddCaseExecutionProtocol(CaseProtocols[30])
	result[3].AddCaseExecutionProtocol(CaseProtocols[6])
	result[3].AddCaseExecutionProtocol(CaseProtocols[21])
	result[3].AddCaseExecutionProtocol(CaseProtocols[26])
	result[3].AddCaseExecutionProtocol(CaseProtocols[31])
	result[4].AddCaseExecutionProtocol(CaseProtocols[6])
	result[4].AddCaseExecutionProtocol(CaseProtocols[21])
	result[4].AddCaseExecutionProtocol(CaseProtocols[26])
	result[4].AddCaseExecutionProtocol(CaseProtocols[31])
	result[4].AddCaseExecutionProtocol(CaseProtocols[28])
	return result
}
