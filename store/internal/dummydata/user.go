// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// Users contains the dummy users
var Users = []user.PasswordUser{
	{
		User: user.User{
			Name:        "default",
			DisplayName: "DeFault",
			EMail:       "default@example.org",

			RegistrationDate: time.Now().Round(time.Second),
		},

		Password: "default",
	},
	{
		User: user.User{
			Name:        "admin",
			DisplayName: "admin",
			EMail:       "admin@example.org",
		},

		Password: "admin",
	},
	{
		User: user.User{
			Name:        "alexanderkaiser",
			DisplayName: "Alexander Kaiser",
			EMail:       "alexander.kaiser@gmx.de",
		},

		Password: "alexander",
	},
	{
		User: user.User{
			Name:        "simoneraab",
			DisplayName: "Simone Raab",
			EMail:       "simone.raab@gmail.com",
		},

		Password: "simone",
	},
	{
		User: user.User{
			Name:        "benweiss",
			DisplayName: "Ben Weiss",
			EMail:       "ben.weiss@yahoo.de",
		},

		Password: "ben",
	},
}
