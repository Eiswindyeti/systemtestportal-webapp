// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package dummydata

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

// Sequences contains the dummy sequences
var Sequences = []test.Sequence{
	{
		Project: Projects[0].ID(),
		Name:    "Searching",

		SequenceVersions: []test.SequenceVersion{
			{
				Testsequence: id.NewTestID(Projects[0].ID(), "Searching", false),
				VersionNr:    2,

				Description:   "This sequence tests the feature searching.",
				Preconditions: "DuckDuckGo.com is opened.  Nothing is typed into the search bar.",

				Message:      "Add test case \"Map\"",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(-4, -2, -1),
				SequenceInfo: test.SequenceInfo{
					Variants: map[string]*project.Variant{
						"Chrome": {
							Name: "Chrome",
							Versions: []project.Version{
								{Name: "v0.2"},
								{Name: "v0.3.1"},
								{Name: "v0.4.6"},
								{Name: "v42.5.25"},
							},
						},
						"Firefox": {
							Name: "Firefox",
							Versions: []project.Version{
								{Name: "v0.1.11"},
								{Name: "v0.2.49"},
								{Name: "v0.4.0"},
								{Name: "v0.4.6"},
							},
						},
						"Microsoft Edge": {
							Name: "Microsoft Edge",
							Versions: []project.Version{
								{Name: "v0.175"},
								{Name: "v0.204"},
								{Name: "v1000"},
								{Name: "v1019"},
							},
						},
					},
				},
				Cases: []test.Case{
					Cases[1],
					Cases[2],
					Cases[3],
					Cases[7],
					Cases[9],
				},
			},
			{
				Testsequence: id.NewTestID(Projects[0].ID(), "Searching", false),
				VersionNr:    1,

				Description:   "This sequence tests the feature searching.",
				Preconditions: "DuckDuckGo.com is opened.  Nothing is typed into the search bar.",
				SequenceInfo: test.SequenceInfo{
					Variants: map[string]*project.Variant{
						"Chrome": {
							Name: "Chrome",
							Versions: []project.Version{
								{Name: "v0.2"},
								{Name: "v0.3.1"},
								{Name: "v0.4.6"},
								{Name: "v42.5.25"},
							},
						},
						"Firefox": {
							Name: "Firefox",
							Versions: []project.Version{
								{Name: "v0.1.11"},
								{Name: "v0.2.49"},
								{Name: "v0.4.0"},
								{Name: "v0.4.6"},
							},
						},
						"Microsoft Edge": {
							Name: "Microsoft Edge",
							Versions: []project.Version{
								{Name: "v0.175"},
								{Name: "v0.204"},
								{Name: "v1000"},
								{Name: "v1019"},
							},
						},
					},
				},
				Message:      "Initial test sequence created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(-4, -2, -1),

				Cases: []test.Case{
					Cases[1],
					Cases[2],
					Cases[3],
					Cases[9],
				},
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Settings",

		SequenceVersions: []test.SequenceVersion{
			{
				Testsequence: id.NewTestID(Projects[0].ID(), "Settings", false),
				VersionNr:    2,

				Description:   "This sequence tests the settings.",
				Preconditions: "DuckDuckGo.com is opened.",
				SequenceInfo: test.SequenceInfo{
					Variants: map[string]*project.Variant{
						"Chrome": {
							Name: "Chrome",
							Versions: []project.Version{
								{Name: "v0.4.6"},
								{Name: "v42.5.25"},
							},
						},
						"Firefox": {
							Name: "Firefox",
							Versions: []project.Version{
								{Name: "v0.4.0"},
								{Name: "v0.4.6"},
							},
						},
					},
				},

				Message:      "Add test Cases",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(-4, -2, -1),

				Cases: []test.Case{
					Cases[0],
					Cases[4],
					Cases[6],
					Cases[8],
				},
			},
			{
				Testsequence: id.NewTestID(Projects[0].ID(), "Settings", false),
				VersionNr:    1,

				Description:   "This sequence tests the settings.",
				Preconditions: "DuckDuckGo.com is opened.",

				Message:      "Initial test sequence created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(-4, -2, -1),
			},
		},
	},
	{
		Project: Projects[0].ID(),
		Name:    "Install DuckDuckGo.com",

		SequenceVersions: []test.SequenceVersion{
			{
				Testsequence: id.NewTestID(Projects[0].ID(), "Install DuckDuckGo.com", false),
				VersionNr:    1,

				Description:   "This sequence tests the installation of DuckDuckGo.com on different browsers.",
				Preconditions: "DuckDuckGo.com is opened.",

				Message:      "Initial test sequence created",
				IsMinor:      false,
				CreationDate: time.Now().AddDate(-4, -2, -1),

				Cases: []test.Case{
					Cases[5],
					Cases[10],
					Cases[11],
				},
			},
		},
	},
}
