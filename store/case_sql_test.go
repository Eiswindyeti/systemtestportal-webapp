// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"encoding/json"
	"errors"
	"reflect"
	"testing"
	"time"

	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

func TestCasesSQLList(t *testing.T) {
	type args struct {
		s   Cases
		pID id.ProjectID
	}
	type result struct {
		tcs []*test.Case
		e   error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:   casesSQL{emptyEngine(t)},
				pID: id.NewProjectID("theo", "GoUnit"),
			},
			w: result{
				tcs: nil,
				e:   multierror.Append(errors.New(`no owner with name "theo"`)),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s:   casesSQL{engineWithSampleData(t)},
				pID: id.NewProjectID("Hans", "GoUnit"),
			},
			w: result{
				tcs: nil,
				e:   multierror.Append(errors.New(`no owner with name "Hans"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s:   casesSQL{engineWithSampleData(t)},
				pID: id.NewProjectID("theo", "Project 53"),
			},
			w: result{
				tcs: nil,
				e:   multierror.Append(errors.New(`no project with name "Project 53"`)),
			},
		},
		{
			name: "Project in store",
			a: args{
				s:   casesSQL{engineWithSampleData(t)},
				pID: id.NewProjectID("theo", "GoUnit"),
			},
			w: result{
				tcs: []*test.Case{
					{
						Project: id.NewProjectID("theo", "GoUnit"),
						Name:    "Complete test suite with rules",

						TestCaseVersions: []test.CaseVersion{
							{
								Case: id.NewTestID(
									id.NewProjectID("theo", "GoUnit"),
									"Complete test suite with rules",
									true,
								),
								VersionNr:     1,
								Description:   "A more complex suite of tests imposing restrictions based on rules",
								Preconditions: "",
								Duration: duration.Duration{
									Duration: time.Hour * 2,
								},

								Message: "Initial version",
								IsMinor: false,
								CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).
									In(time.Local),

								Steps: []test.Step{
									{
										Index:          1,
										Action:         `Click on the "Execute Test Suite" button`,
										ExpectedResult: `The application should display the execution screen`,
									},
									{
										Index:  2,
										Action: `Select the "Complex Test Suite"`,
										ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
											" list contains 3 rules",
									},
									{
										Index:          3,
										Action:         `Change the value of the "Iterations" rule to 15`,
										ExpectedResult: `The label displays the new value`,
									},
									{
										Index:          4,
										Action:         `Click on start execution`,
										ExpectedResult: `The application shows a progress screen`,
									},
									{
										Index:  5,
										Action: `Wait for the execution to finish`,
										ExpectedResult: "The execution should complete successfully in less than " +
											"90 minutes",
									},
								},

								Variants: map[string]*project.Variant{
									"Linux 32bit": {
										Name: "Linux 32bit",
										Versions: []project.Version{
											{Name: "v1.0.0-linux-x86"},
										},
									},
									"Linux 64bit": {
										Name: "Linux 64bit",
										Versions: []project.Version{
											{Name: "v1.0.0-linux-amd64"},
										},
									},
									"Windows 32bit": {
										Name: "Windows 32bit",
										Versions: []project.Version{
											{Name: "v1.0.0-win-x86"},
										},
									},
									"Windows 64bit": {
										Name: "Windows 64bit",
										Versions: []project.Version{
											{Name: "v1.0.0-win-amd64"},
										},
									},
								},

								Tester: map[string]*user.User{},
							},
						},

						Labels: []project.Label{
							{
								Name: "Long test",
								Description: "This test, including setup and tear down of the test environment, takes" +
									" longer than 120 minutes",
							},
						},
					},
					{
						Project: id.NewProjectID("theo", "GoUnit"),
						Name:    "Simple test suite",

						TestCaseVersions: []test.CaseVersion{
							{
								Case: id.NewTestID(
									id.NewProjectID("theo", "GoUnit"),
									"Simple test suite",
									true,
								),
								VersionNr:     2,
								Description:   "A simple execution of a test suite",
								Preconditions: "",
								Duration: duration.Duration{
									Duration: time.Hour / 2,
								},

								Message:      "Revise duration",
								IsMinor:      true,
								CreationDate: time.Date(2017, time.September, 5, 5, 0, 18, 0, time.UTC).In(time.Local),

								Steps: []test.Step{
									{
										Index:          1,
										Action:         `Click on the "Execute Test Suite" button`,
										ExpectedResult: `The application should display the execution screen`,
									},
									{
										Index:          2,
										Action:         `Select the "Simple Test Suite"`,
										ExpectedResult: "The test case list contains 4 test cases",
									},
									{
										Index:          3,
										Action:         `Click on start execution`,
										ExpectedResult: `The application shows a progress screen`,
									},
									{
										Index:  4,
										Action: `Wait for the execution to finish`,
										ExpectedResult: "The execution should complete successfully in less than " +
											"20 minutes",
									},
								},

								Variants: map[string]*project.Variant{
									"Linux 32bit": {
										Name: "Linux 32bit",
										Versions: []project.Version{
											{Name: "v0.1.0-linux-x86"},
											{Name: "v0.1.1-linux-x86"},
											{Name: "v1.0.0-linux-x86"},
										},
									},
									"Linux 64bit": {
										Name: "Linux 64bit",
										Versions: []project.Version{
											{Name: "v0.1.0-linux-amd64"},
											{Name: "v0.1.1-linux-amd64"},
											{Name: "v1.0.0-linux-amd64"},
										},
									},
									"Windows 32bit": {
										Name: "Windows 32bit",
										Versions: []project.Version{
											{Name: "v0.1.0-win-x86"},
											{Name: "v0.1.1-win-x86"},
											{Name: "v1.0.0-win-x86"},
										},
									},
									"Windows 64bit": {
										Name: "Windows 64bit",
										Versions: []project.Version{
											{Name: "v0.1.0-win-amd64"},
											{Name: "v0.1.1-win-amd64"},
											{Name: "v1.0.0-win-amd64"},
										},
									},
								},

								Tester: map[string]*user.User{
									"theo": {
										Name:        "theo",
										DisplayName: "Theodor Heuss",
										EMail:       "theo.heu@example.org",

										RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
									},
								},
							},
							{
								Case: id.NewTestID(
									id.NewProjectID("theo", "GoUnit"),
									"Simple test suite",
									true,
								),
								VersionNr:     1,
								Description:   "A simple execution of a test suite",
								Preconditions: "",
								Duration: duration.Duration{
									Duration: time.Hour,
								},

								Message:      "Initial version",
								IsMinor:      false,
								CreationDate: time.Date(2017, time.September, 5, 3, 30, 44, 0, time.UTC).In(time.Local),

								Steps: []test.Step{
									{
										Index:          1,
										Action:         `Click on the "Execute Test Suite" button`,
										ExpectedResult: `The application should display the execution screen`,
									},
									{
										Index:          2,
										Action:         `Select the "Simple Test Suite"`,
										ExpectedResult: "The test case list contains 4 test cases",
									},
									{
										Index:          3,
										Action:         `Click on start execution`,
										ExpectedResult: `The application shows a progress screen`,
									},
									{
										Index:          4,
										Action:         `Wait for the execution to finish`,
										ExpectedResult: "The execution should complete successfully",
									},
								},

								Variants: map[string]*project.Variant{
									"Linux 32bit": {
										Name: "Linux 32bit",
										Versions: []project.Version{
											{Name: "v0.1.0-linux-x86"},
											{Name: "v0.1.1-linux-x86"},
										},
									},
									"Linux 64bit": {
										Name: "Linux 64bit",
										Versions: []project.Version{
											{Name: "v0.1.0-linux-amd64"},
											{Name: "v0.1.1-linux-amd64"},
										},
									},
									"Windows 32bit": {
										Name: "Windows 32bit",
										Versions: []project.Version{
											{Name: "v0.1.0-win-x86"},
											{Name: "v0.1.1-win-x86"},
										},
									},
									"Windows 64bit": {
										Name: "Windows 64bit",
										Versions: []project.Version{
											{Name: "v0.1.0-win-amd64"},
											{Name: "v0.1.1-win-amd64"},
										},
									},
								},

								Tester: map[string]*user.User{},
							},
						},

						Labels: []project.Label{
							{
								Name:        "Improvement needed",
								Description: "This test has a low quality and should be improved",
							},
							{
								Name: "Short test",
								Description: "This test, including setup and tear down of the test environment, takes" +
									" no longer than 10 minutes",
							},
						},
					},
				},
				e: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.tcs, got.e = tc.a.s.List(tc.a.pID)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("casesSQL.List(%v) = (%v, %v), want (%v, %v)", tc.a.pID, got.tcs, got.e, tc.w.tcs, tc.w.e)
			}
		})
	}
}

func TestCasesSQLGet(t *testing.T) {
	type args struct {
		s  Cases
		id id.TestID
	}

	type result struct {
		tc  *test.Case
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:  casesSQL{emptyEngine(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example test case", true),
			},
			w: result{
				tc:  nil,
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "Another group"`)),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another new group", "Test project"), "Example test case", true),
			},
			w: result{
				tc:  nil,
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "Another new group"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Another Test project"), "Example test case", true),
			},
			w: result{
				tc:  nil,
				ex:  false,
				err: multierror.Append(errors.New(`no project with name "Another Test project"`)),
			},
		},
		{
			name: "Test case not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example unit test case", true),
			},
			w: result{
				tc:  nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Test case in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example test case", true),
			},
			w: result{
				tc: &test.Case{
					Project: id.NewProjectID("Another group", "Test project"),
					Name:    "Example test case",

					TestCaseVersions: []test.CaseVersion{
						{
							Case: id.NewTestID(
								id.NewProjectID("Another group", "Test project"),
								"Example test case",
								true,
							),
							VersionNr:     1,
							Description:   "An example for demonstrating the abilities of a test case",
							Preconditions: "",
							Duration: duration.Duration{
								Duration: time.Minute * 10,
							},

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2017, time.August, 15, 10, 15, 36, 0, time.UTC).In(time.Local),

							Steps: []test.Step{
								{
									Index:          1,
									Action:         "Open the application",
									ExpectedResult: "The application should display the start screen",
								},
							},

							Variants: map[string]*project.Variant{
								"Linux": {
									Name: "Linux",
									Versions: []project.Version{
										{Name: "Standard"},
									},
								},
								"Windows": {
									Name: "Windows",
									Versions: []project.Version{
										{Name: "Standard"},
									},
								},
							},

							Tester: map[string]*user.User{
								"theo": {
									Name:        "theo",
									DisplayName: "Theodor Heuss",
									EMail:       "theo.heu@example.org",

									RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
								},
							},
						},
					},

					Labels: []project.Label{
						{
							Name:        "Example label",
							Description: "First label for demonstration purposes",
						},
					},
				},
				ex:  true,
				err: nil,
			},
		},
		{
			name: "Get with deleted test case",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "AW-987"), "Deleted test case", true),
			},
			w: result{
				tc:  nil,
				ex:  false,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.tc, got.ex, got.err = tc.a.s.Get(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("casesSQL.Get(%v) = %#v, want %#v", tc.a.id, got, tc.w)
			}
		})
	}
}

func TestCasesSQLAdd(t *testing.T) {
	type args struct {
		s  Cases
		tc *test.Case
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name         string
		a            args
		w            result
		shouldCreate bool
	}{
		{
			name: "Empty store",
			a: args{
				s: casesSQL{emptyEngine(t)},
				tc: &test.Case{
					Project: id.NewProjectID("Another group", "Test project"),
					Name:    "New test case",

					TestCaseVersions: []test.CaseVersion{
						{
							Case: id.NewTestID(
								id.NewProjectID("Another group", "Test project"),
								"New test case",
								true,
							),
							VersionNr:     1,
							Description:   "A newly added test case",
							Preconditions: "",
							Duration:      duration.NewDuration(0, 2, 0),

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2018, time.January, 13, 7, 52, 19, 0, time.UTC).In(time.Local),

							Steps: []test.Step{
								{
									Index:          1,
									Action:         "Add a test case",
									ExpectedResult: "There is a new test case",
								},
							},
						},
					},
				},
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Another group"`)),
			},
			shouldCreate: false,
		},
		{
			name: "Add with owner not in store",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				tc: &test.Case{
					Project: id.NewProjectID("Another new group", "Test project"),
					Name:    "New test case",

					TestCaseVersions: []test.CaseVersion{
						{
							Case: id.NewTestID(
								id.NewProjectID("Another new group", "Test project"),
								"New test case",
								true,
							),
							VersionNr:     1,
							Description:   "A newly added test case",
							Preconditions: "",
							Duration:      duration.NewDuration(0, 2, 0),

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2018, time.January, 13, 7, 52, 19, 0, time.UTC).In(time.Local),

							Steps: []test.Step{
								{
									Index:          1,
									Action:         "Add a test case",
									ExpectedResult: "There is a new test case",
								},
							},
						},
					},
				},
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Another new group"`)),
			},
			shouldCreate: false,
		},
		{
			name: "Project not in store",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				tc: &test.Case{
					Project: id.NewProjectID("Another group", "Test new project"),
					Name:    "New test case",

					TestCaseVersions: []test.CaseVersion{
						{
							Case: id.NewTestID(
								id.NewProjectID("Another group", "Test new project"),
								"New test case",
								true,
							),
							VersionNr:     1,
							Description:   "A newly added test case",
							Preconditions: "",
							Duration:      duration.NewDuration(0, 2, 0),

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2018, time.January, 13, 7, 52, 19, 0, time.UTC).In(time.Local),

							Steps: []test.Step{
								{
									Index:          1,
									Action:         "Add a test case",
									ExpectedResult: "There is a new test case",
								},
							},

							Variants: map[string]*project.Variant{
								"Linux": {
									Name: "Linux",
									Versions: []project.Version{
										{Name: "Standard"},
									},
								},
								"Windows": {
									Name: "Windows",
									Versions: []project.Version{
										{Name: "Standard"},
									},
								},
							},
						},
					},
				},
			},
			w: result{
				err: multierror.Append(errors.New(`no project with name "Test new project"`)),
			},
			shouldCreate: false,
		},
		{
			name: "New test case",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				tc: &test.Case{
					Project: id.NewProjectID("Another group", "Test project"),
					Name:    "New test case",

					TestCaseVersions: []test.CaseVersion{
						{
							Case: id.NewTestID(
								id.NewProjectID("Another group", "Test project"),
								"New test case",
								true,
							),
							VersionNr:     1,
							Description:   "A newly added test case",
							Preconditions: "",
							Duration:      duration.NewDuration(0, 2, 0),

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2018, time.January, 13, 7, 52, 19, 0, time.UTC).In(time.Local),

							Steps: []test.Step{
								{
									Index:          1,
									Action:         "Add a test case",
									ExpectedResult: "There is a new test case",
								},
							},

							Variants: map[string]*project.Variant{},

							Tester: map[string]*user.User{},
						},
					},

					Labels: []project.Label{
						{
							Name:        "Example label",
							Description: "First label for demonstration purposes",
						},
					},
				},
			},
			w: result{
				err: nil,
			},
			shouldCreate: true,
		},
		{
			name: "Updated test case",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				tc: &test.Case{
					Project: id.NewProjectID("Another group", "Test project"),
					Name:    "Example test case",

					TestCaseVersions: []test.CaseVersion{
						{
							Case: id.NewTestID(
								id.NewProjectID("Another group", "Test project"),
								"Example test case",
								true,
							),
							VersionNr:     2,
							Description:   "An example for demonstrating the capabilities of a test case",
							Preconditions: "",
							Duration:      duration.NewDuration(0, 10, 0),

							Message:      "Minor wording changes",
							IsMinor:      true,
							CreationDate: time.Date(2017, time.December, 12, 13, 28, 17, 0, time.UTC).In(time.Local),

							Steps: []test.Step{
								{
									Index:          1,
									Action:         "Open the application",
									ExpectedResult: "The application should display the start screen",
								},
							},

							Variants: map[string]*project.Variant{
								"Linux": {
									Name: "Linux",
									Versions: []project.Version{
										{Name: "Premium"},
									},
								},
								"Windows": {
									Name: "Windows",
									Versions: []project.Version{
										{Name: "Standard"},
										{Name: "Premium"},
									},
								},
							},

							Tester: map[string]*user.User{
								"Franz": {
									Name:        "Franz",
									DisplayName: "Frain",
									EMail:       "f.thor@example.org",

									RegistrationDate: time.Date(2017, time.July, 12, 9, 6, 28, 0, time.UTC).
										In(time.Local),
								},
							},
						},
						{
							Case: id.NewTestID(
								id.NewProjectID("Another group", "Test project"),
								"Example test case",
								true,
							),
							VersionNr:     1,
							Description:   "An example for demonstrating the abilities of a test case",
							Preconditions: "",
							Duration:      duration.NewDuration(0, 10, 0),

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2017, time.August, 15, 10, 15, 36, 0, time.UTC).In(time.Local),

							Steps: []test.Step{
								{
									Index:          1,
									Action:         "Open the application",
									ExpectedResult: "The application should display the start screen",
								},
							},

							Variants: map[string]*project.Variant{
								"Linux": {
									Name: "Linux",
									Versions: []project.Version{
										{Name: "Standard"},
									},
								},
								"Windows": {
									Name: "Windows",
									Versions: []project.Version{
										{Name: "Standard"},
									},
								},
							},

							Tester: map[string]*user.User{
								"theo": {
									Name:        "theo",
									DisplayName: "Theodor Heuss",
									EMail:       "theo.heu@example.org",

									RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
								},
							},
						},
					},

					Labels: []project.Label{
						{
							Name:        "Unused label",
							Description: "This label is not assigned anywhere",
						},
					},
				},
			},
			w: result{
				err: nil,
			},
			shouldCreate: true,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.err = tc.a.s.Add(tc.a.tc)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf(`casesSQL.Add(%#v) = "%v", want "%v"`, tc.a.tc, got.err, tc.w.err)
				return
			}

			if !tc.shouldCreate {
				return
			}

			atc, ex, err := tc.a.s.Get(id.NewTestID(tc.a.tc.Project, tc.a.tc.Name, true))
			if err != nil {
				t.Errorf("Unexpected error when retrieving added test case %q", err)
			}

			if !ex {
				t.Errorf("Added test case does not exist")
			}

			if !reflect.DeepEqual(tc.a.tc, atc) {
				t.Errorf("Not all information was saved to the database")

				j, err := json.Marshal(atc)
				handleError(t, err)
				t.Log(string(j))
				j, err = json.Marshal(tc.a.tc)
				handleError(t, err)
				t.Log(string(j))
			}
		})
	}
}

func TestCasesSQLRename(t *testing.T) {
	type args struct {
		s Cases
		o id.TestID
		n id.TestID
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s: casesSQL{emptyEngine(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test project"),
					"Example test case",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test project"),
					"Example Test Case",
					true,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Another group"`)),
			},
		},
		{
			name: "Add with owner not in store",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another new group"), "Test project"),
					"Example test case",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another new group"), "Test project"),
					"Example Test Case",
					true,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Another new group"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test new project"),
					"Example test case",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test new project"),
					"Example Test Case",
					true,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no project with name "Test new project"`)),
			},
		},
		{
			name: "Test case not in store",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test project"),
					"Example new test case",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test project"),
					"Example Test Case",
					true,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no test case with name "Example new test case"`)),
			},
		},
		{
			name: "Move test case",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"Example test case",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "AW-987"),
					"Example Test Case",
					true,
				),
			},
			w: result{
				err: multierror.Append(
					errors.New(`unsupported operation: test cases currently can not be moved between projects`),
				),
			},
		},
		{
			name: "Successful rename",
			a: args{
				s: casesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test project"),
					"Example test case",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("Another group"), "Test project"),
					"Example Test Case",
					true,
				),
			},
			w: result{
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.err = tc.a.s.Rename(tc.a.o, tc.a.n)

			if !reflect.DeepEqual(got, tc.w) {
				t.Log(got.err)
				t.Log(tc.w.err)
				t.Errorf("casesSQL.Rename(%v, %v) = %v, want %v", tc.a.o, tc.a.n, got, tc.w)
				return
			}

			if tc.w.err != nil {
				return
			}

			_, ex, _ := tc.a.s.Get(tc.a.o)
			if ex {
				t.Error("Renamed test case can still be retrieved with old id")
				return
			}

			_, ex, _ = tc.a.s.Get(tc.a.n)
			if !ex {
				t.Error("Renamed test case can not be retrieved with new id")
				return
			}
		})
	}
}

func TestCasesSQLExists(t *testing.T) {
	type args struct {
		s  Cases
		id id.TestID
	}

	type result struct {
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Exists with empty store",
			a: args{
				s:  casesSQL{emptyEngine(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example test case", true),
			},
			w: result{
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "Another group"`)),
			},
		},
		{
			name: "Exists with owner not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another new group", "Test project"), "Example test case", true),
			},
			w: result{
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "Another new group"`)),
			},
		},
		{
			name: "Exists with project not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Another Test project"), "Example test case", true),
			},
			w: result{
				ex:  false,
				err: multierror.Append(errors.New(`no project with name "Another Test project"`)),
			},
		},
		{
			name: "Exists with test case not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example unit test case", true),
			},
			w: result{
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Exists with test case in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example test case", true),
			},
			w: result{
				ex:  true,
				err: nil,
			},
		},
		{
			name: "Exists with deleted test case",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "AW-987"), "Deleted test case", true),
			},
			w: result{
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.ex, got.err = tc.a.s.Exists(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("casesSQL.Exists(%v) = %#v, want %#v", tc.a.id, got, tc.w)
			}
		})
	}
}

func TestCasesSQLDelete(t *testing.T) {
	type args struct {
		s  Cases
		id id.TestID
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Delete with empty store",
			a: args{
				s:  casesSQL{emptyEngine(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example test case", true),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Another group"`)),
			},
		},
		{
			name: "Delete with owner not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another new group", "Test project"), "Example test case", true),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Another new group"`)),
			},
		},
		{
			name: "Delete with project not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Another Test project"), "Example test case", true),
			},
			w: result{
				err: multierror.Append(errors.New(`no project with name "Another Test project"`)),
			},
		},
		{
			name: "Delete with test case not in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example unit test case", true),
			},
			w: result{
				err: multierror.Append(errors.New(`no test case with name "Example unit test case"`)),
			},
		},
		{
			name: "Delete with test case in store",
			a: args{
				s:  casesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Another group", "Test project"), "Example test case", true),
			},
			w: result{
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			existed, err := tc.a.s.Exists(tc.a.id)
			t.Log("Test case to delete exists:", existed)
			if err != nil {
				t.Log(err)
			}

			var got result
			got.err = tc.a.s.Delete(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("casesSQL.Delete(%v) = %v, want %v", tc.a.id, got.err, tc.w.err)
				return
			}

			if _, ex, _ := tc.a.s.Get(tc.a.id); ex {
				t.Error("Deleted test case can still be retrieved with Get")
			}

			if ex, _ := tc.a.s.Exists(tc.a.id); ex != existed {
				t.Error("The id of the deleted test case is not locked anymore")
			}
		})
	}
}
