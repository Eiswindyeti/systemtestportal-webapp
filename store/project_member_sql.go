// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

type projectMemberRow struct {
	ID        int64
	ProjectID int64
	UserID    int64
}

func saveProjectMembers(s xorm.Interface, pr projectRow, nms map[id.ActorID]*user.User) error {
	err := deleteMemberRowsForProject(s, pr.ID)
	if err != nil {
		return err
	}

	var uIDs []id.ActorID
	for nm := range nms {
		uIDs = append(uIDs, nm)
	}

	urIDs, err := lookupUserRowIDs(s, uIDs...)
	if err != nil {
		return err
	}

	var mrs []projectMemberRow
	for _, urID := range urIDs {
		mr := projectMemberRow{ProjectID: pr.ID, UserID: urID}
		mrs = append(mrs, mr)
	}

	err = insertProjectMemberRows(s, mrs...)
	return err
}

func insertProjectMemberRows(s xorm.Interface, pmrs ...projectMemberRow) error {
	_, err := s.Table(projectMemberTable).Insert(&pmrs)
	return err
}

func deleteMemberRowsForProject(s xorm.Interface, prID int64) error {
	_, err := s.Table(projectMemberTable).Delete(&projectMemberRow{ProjectID: prID})
	return err
}

func listProjectMembers(s xorm.Interface, prID int64) (map[id.ActorID]*user.User, error) {
	mrs, err := listUserRowsForProject(s, prID)

	ms := make(map[id.ActorID]*user.User)
	for _, mr := range mrs {
		m := userFromRow(mr)
		ms[m.ID()] = m
	}

	return ms, err
}

func listUserRowsForProject(s xorm.Interface, prID int64) ([]userRow, error) {
	var uIDs []int64
	err := s.Table(projectMemberTable).Distinct(userField).In(projectField, prID).Find(&uIDs)
	if err != nil {
		return nil, err
	}

	var mrs []userRow
	err = s.Table(userTable).In(idField, uIDs).Find(&mrs)
	if err != nil {
		return nil, err
	}

	return mrs, nil
}
