// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"fmt"
	"math/rand"
	"testing"

	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
	migrate "github.com/rubenv/sql-migrate"
)

var (
	baseMigrations = migrate.MemoryMigrationSource{Migrations: mustFindMigrations(initMigrationSource())}
	dataMigrations = migrate.MemoryMigrationSource{Migrations: mustFindMigrations(initTestDataMigrationSource())}
)

func mustFindMigrations(s migrate.MigrationSource) []*migrate.Migration {
	ms, err := s.FindMigrations()
	if err != nil {
		panic(err)
	}

	return ms
}

func handleError(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatal("Unexpected error:", err)
	}
}

func emptyEngine(t *testing.T) *xorm.Engine {
	// Ugly hack to prevent race conditions with in-memory databases
	dbPath := fmt.Sprintf("file:db-%d?mode=memory&cache=shared&_foreign_keys=1", rand.Uint64())
	e, err := xorm.NewEngine("sqlite3", dbPath)
	handleError(t, err)

	e.ShowSQL(true)
	e.Logger().SetLevel(core.LOG_DEBUG)

	e.SetMapper(core.GonicMapper{})
	migrateTestDB(t, e)

	return e
}

// Copied from store/sql.go
//
// Used to set up the database schema for the tests
func migrateTestDB(t *testing.T, e *xorm.Engine) {
	runMigration(t, e, baseMigrations)
}

func engineWithSampleData(t *testing.T) *xorm.Engine {
	e := emptyEngine(t)

	runMigration(t, e, dataMigrations)

	return e
}

func runMigration(t *testing.T, e *xorm.Engine, migrations migrate.MigrationSource) {
	db := e.DB().DB

	_, err := migrate.Exec(db, "sqlite3", migrations, migrate.Up)
	handleError(t, err)
}

func initTestDataMigrationSource() migrate.MigrationSource {
	return combinedMigrationSource{
		sources: []migrate.MigrationSource{
			initMigrationSource(),
			migrate.FileMigrationSource{
				Dir: "testdata",
			},
		},
	}
}

type combinedMigrationSource struct {
	sources []migrate.MigrationSource
}

func (cms combinedMigrationSource) FindMigrations() ([]*migrate.Migration, error) {
	var migrations []*migrate.Migration

	for _, s := range cms.sources {
		ms, err := s.FindMigrations()
		if err != nil {
			return nil, err
		}

		migrations = append(migrations, ms...)
	}

	return migrations, nil
}
