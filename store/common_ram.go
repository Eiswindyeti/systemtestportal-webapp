// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import (
	"log"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/store/internal/dummydata"
)

var (
	userStore     = initUsersRAM()
	groupStore    = initGroupsRAM()
	projectStore  = initProjectsRAM()
	caseStore     = initCasesRAM()
	sequenceStore = initSequencesRAM()
)

// InitializeDatabase initializes the datastore
func InitializeDatabase() {
	initDummyUsers()
	initDummyProjects()
	initDummyCases()
	initDummySequences()
}

// GetActorExistenceChecker returns the implementation to use for ActorExistenceChecker
func GetActorExistenceChecker() id.ActorExistenceChecker {
	return &ownersRAM{userStore, groupStore}
}

// GetUserStore returns a store for users
func GetUserStore() Users {
	return userStore
}

// GetGroupStore returns a store for groups
func GetGroupStore() Groups {
	return groupStore
}

// GetProjectStore returns a store for projects
func GetProjectStore() Projects {
	return projectStore
}

// GetCaseStore returns a store for test cases
func GetCaseStore() Cases {
	return caseStore
}

// GetSequenceStore returns a store for test sequences
func GetSequenceStore() Sequences {
	return sequenceStore
}

func initDummyUsers() {
	for _, du := range dummydata.Users {
		u := new(user.PasswordUser)
		*u = du

		err := userStore.Add(u)
		if err != nil {
			log.Println(err)
		}
	}
}

func initDummyProjects() {
	for _, dp := range dummydata.Projects {
		p := new(project.Project)
		*p = dp

		err := projectStore.Add(p)
		if err != nil {
			log.Println(err)
		}
	}
}

func initDummyCases() {
	for _, dc := range dummydata.Cases {
		c := new(test.Case)
		*c = dc

		err := caseStore.Add(c)
		if err != nil {
			log.Println(err)
		}
	}
}

func initDummySequences() {
	for _, ds := range dummydata.Sequences {
		s := new(test.Sequence)
		*s = ds

		err := sequenceStore.Add(s)
		if err != nil {
			log.Println(err)
		}
	}
}
