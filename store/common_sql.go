// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/go-xorm/core"
	"github.com/go-xorm/xorm"
	migrate "github.com/rubenv/sql-migrate"
	// Blank import to provide sqlite database driver
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

const (
	ownerTable                 = "owners"
	userTable                  = "users"
	groupTable                 = "groups"
	projectTable               = "projects"
	caseTable                  = "test_cases"
	caseVersionTable           = "test_case_versions"
	stepTable                  = "test_steps"
	sequenceTable              = "test_sequences"
	sequenceVersionTable       = "test_sequence_versions"
	sequenceVersionCaseTable   = "test_sequence_version_test_cases"
	sutVariantTable            = "project_variants"
	sutVersionTable            = "project_versions"
	caseVersionSUTVersionTable = "test_case_version_project_versions"
	projectLabelTable          = "project_labels"
	caseLabelTable             = "test_case_labels"
	sequenceLabelTable         = "test_sequence_labels"
	projectMemberTable         = "project_members"
	caseVersionTesterTable     = "test_case_version_testers"
	sequenceVersionTesterTable = "test_sequence_version_testers"

	idField            = "id"
	nameField          = "name"
	versionNumberField = "version_nr"
	stepIndexField     = "step_index"
	caseIndexField     = "case_index"
	projectField       = "project_id"
	userField          = "user_id"

	errorNoAffectedRows = "invalid number of affected rows (%d) when updating"
)

var engine *xorm.Engine

// InitializeDatabase initializes the database connection and makes sure the database schema is up to date
func InitializeDatabase() {
	engine = initDBEngine()

	migrateDB(engine)
}

// GetActorExistenceChecker returns the implementation to use for ActorExistenceChecker
func GetActorExistenceChecker() id.ActorExistenceChecker {
	return ownersSQL{engine}
}

// GetUserStore returns a store for users
func GetUserStore() Users {
	return usersSQL{engine}
}

// GetGroupStore returns a store for groups
func GetGroupStore() Groups {
	return groupsSQL{engine}
}

// GetProjectStore returns a store for projects
func GetProjectStore() Projects {
	return projectsSQL{engine}
}

// GetCaseStore returns a store for test cases
func GetCaseStore() Cases {
	return casesSQL{engine}
}

// GetSequenceStore returns a store for test sequences
func GetSequenceStore() Sequences {
	return sequencesSQL{engine}
}

func initDataDir() string {
	dir := config.Get().DataDir
	err := os.MkdirAll(dir, 0700)
	log.Printf("Loading sqlite database from: %s", dir)

	if err != nil {
		panic(err)
	}

	return dir
}

func initDBEngine() *xorm.Engine {
	dir := initDataDir()

	dbPath := filepath.Join(dir, "stp.db")
	connURL := fmt.Sprintf("file:%s?_foreign_keys=1", dbPath)
	e, err := xorm.NewEngine("sqlite3", connURL)

	if err != nil {
		panic(err)
	}

	e.SetMapper(core.GonicMapper{})

	return e
}

func migrateDB(e *xorm.Engine) {
	migrations := initMigrationSource()

	db := e.DB().DB

	applied, err := migrate.Exec(db, "sqlite3", migrations, migrate.Up)

	log.Println("Applied", applied, "migrations")

	if err != nil {
		panic(err)
	}
}

func initMigrationSource() migrate.MigrationSource {
	migrationDir := filepath.Join(config.BasePath(), "migrations", "sqlite3")

	return &migrate.FileMigrationSource{
		Dir: migrationDir,
	}
}
