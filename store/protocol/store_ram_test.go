/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package protocol

import (
	"testing"
	"time"

	"errors"

	"reflect"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

var ownerID = id.NewActorID("user")
var projectID = id.NewProjectID(ownerID, "abcdef42")
var CaseID = id.NewTestID(projectID, "C18e5d4z", true)
var SequenceID = id.NewTestID(projectID, "S4e8dg852", false)

const SUTVersion = "1.1"
const SutVariant = "Win10"
const OverAllComment = "All OK"

var stepPrt1 = test.StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 4, 0),
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var stepPrt2 = test.StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 10, 0),
	ObservedBehavior: "",
	Result:           test.Pass,
	Comment:          "Hello?",
}
var stepPrt3 = test.StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 22, 0),
	ObservedBehavior: "Something happened",
	Result:           test.PassWithComment,
	Comment:          "What's up?",
}
var stepPrt4 = test.StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 0, 0),
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var stepPrt5 = test.StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 0, 0),
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var stepPrt6 = test.StepExecutionProtocol{
	NeededTime:       duration.NewDuration(0, 1, 0),
	ObservedBehavior: "",
	Result:           test.NotAssessed,
	Comment:          "",
}
var caseProtocolTemplate = test.CaseExecutionProtocol{
	TestVersion: id.NewTestVersionID(CaseID, 1),
	SUTVersion:  SUTVersion,
	SUTVariant:  SutVariant,

	ExecutionDate: time.Date(2000, 12, 24, 12, 0, 0, 0, time.UTC),
	StepProtocols: []test.StepExecutionProtocol{stepPrt1, stepPrt2, stepPrt3, stepPrt4, stepPrt5, stepPrt6},
	Result:        test.NotAssessed,
	Comment:       OverAllComment,
}

func caseProtocolMapContainsCaseProtocol(m caseProtocolMap, item test.CaseExecutionProtocol) bool {

	slice :=
		m[item.TestVersion.Owner()][item.TestVersion.Project()][item.TestVersion.Test()][item.TestVersion.TestVersion()]
	if slice == nil {
		return false
	}
	for _, r := range slice {
		if r.Equals(item) {
			return true
		}
	}
	return false
}
func sequenceProtocolMapContainsSequenceProtocol(m sequenceProtocolMap, item test.SequenceExecutionProtocol) bool {
	slice :=
		m[item.TestVersion.Owner()][item.TestVersion.Project()][item.TestVersion.Test()][item.TestVersion.TestVersion()]
	if slice == nil {
		return false
	}
	for _, r := range slice {
		if r.Equals(item) {
			return true
		}
	}
	return false
}

func sliceContainsCaseProtocol(slice []test.CaseExecutionProtocol, item test.CaseExecutionProtocol) bool {

	for _, r := range slice {
		if r.Equals(item) {
			return true
		}
	}
	return false
}
func sliceContainsSequenceProtocol(slice []test.SequenceExecutionProtocol, item test.SequenceExecutionProtocol) bool {

	for _, r := range slice {
		if r.Equals(item) {
			return true
		}
	}
	return false
}

func TestStoreRAM_AddCaseProtocol(t *testing.T) {

	caseProtocol := caseProtocolTemplate.DeepCopy()
	server := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	err := server.AddCaseProtocol(&caseProtocol)

	if err != nil {
		t.Error(err)
	}
	if !caseProtocolMapContainsCaseProtocol(server.caseExecutionProtocols, caseProtocol) {
		t.Error("Protocol wasn't saved correctly and couldn't be found in store.")
	}
}

func TestStoreRAM_AddSequenceProtocol(t *testing.T) {
	caseProtocol1 := caseProtocolTemplate.DeepCopy()
	caseProtocol2 := caseProtocolTemplate.DeepCopy()
	caseProtocol2.TestVersion = id.NewTestVersionID(caseProtocolTemplate.TestVersion.TestID, 2)
	sequenceProtocol := test.SequenceExecutionProtocol{
		TestVersion: id.NewTestVersionID(SequenceID, 1),
		CaseExecutionProtocols: []id.ProtocolID{
			caseProtocol1.ID(),
			caseProtocol2.ID(),
		},
	}
	server := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	err := server.AddSequenceProtocol(&sequenceProtocol)

	if err != nil {
		t.Error(err)
	}

	//search for sequenceProtocol
	if !sequenceProtocolMapContainsSequenceProtocol(server.sequenceExecutionProtocols, sequenceProtocol) {
		t.Error("Protocol wasn't saved correctly and couldn't be found in store.")
	}
}

func TestStoreRAM_AddSequenceProtocol2(t *testing.T) {
	caseProtocol1 := caseProtocolTemplate.DeepCopy()
	caseProtocol2 := caseProtocolTemplate.DeepCopy()
	caseProtocol2.TestVersion = id.NewTestVersionID(caseProtocolTemplate.TestVersion.TestID, 2)
	caseProtocol3 := caseProtocolTemplate.DeepCopy()
	caseProtocol2.TestVersion = id.NewTestVersionID(caseProtocolTemplate.TestVersion.TestID, 3)
	sequenceProtocol1 := test.SequenceExecutionProtocol{
		TestVersion: id.NewTestVersionID(SequenceID, 1),
		CaseExecutionProtocols: []id.ProtocolID{
			caseProtocol1.ID(),
		},
	}
	sequenceProtocol2 := test.SequenceExecutionProtocol{
		TestVersion: id.NewTestVersionID(SequenceID, 1),
		CaseExecutionProtocols: []id.ProtocolID{
			caseProtocol2.ID(),
			caseProtocol3.ID(),
		},
	}
	server := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	err1 := server.AddSequenceProtocol(&sequenceProtocol1)
	err2 := server.AddSequenceProtocol(&sequenceProtocol2)

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Error(err2)
	}

	//search for sequenceProtocol
	if !sequenceProtocolMapContainsSequenceProtocol(server.sequenceExecutionProtocols, sequenceProtocol1) {
		t.Error("Protocol 1 wasn't saved correctly and couldn't be found in store.")
	}
	if !sequenceProtocolMapContainsSequenceProtocol(server.sequenceExecutionProtocols, sequenceProtocol2) {
		t.Error("Protocol 2 wasn't saved correctly and couldn't be found in store.")
	}
}

func TestStoreRAM_GetCaseExecutionProtocols(t *testing.T) {
	caseProtocol := caseProtocolTemplate.DeepCopy()
	server := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	err1 := server.AddCaseProtocol(&caseProtocol)
	slice, err2 := server.GetCaseExecutionProtocols(CaseID)

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Errorf("No Protocols were found for projectID %+v and TestCaseID %+v.", projectID, CaseID)
	}
	if !sliceContainsCaseProtocol(slice, caseProtocol) {
		t.Error("Protocols couldn't be read from store.")
	}
}

func TestStoreRAM_GetCaseExecutionProtocolsForProject(t *testing.T) {
	caseProtocol := caseProtocolTemplate.DeepCopy()
	server := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	err1 := server.AddCaseProtocol(&caseProtocol)
	slice, err2 := server.GetCaseExecutionProtocolsForProject(projectID)

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Errorf("No Protocols were found for projectID %s.", projectID)
	}
	if !sliceContainsCaseProtocol(slice, caseProtocol) {
		t.Error("Protocols couldn't be read from store.")
	}
}

func TestStoreRAM_GetSequenceExecutionProtocols(t *testing.T) {
	caseProtocol1 := caseProtocolTemplate.DeepCopy()
	tempID := id.NewTestVersionID(caseProtocol1.TestVersion.TestID, 2)
	caseProtocol2 := caseProtocolTemplate.DeepCopy()
	caseProtocol2.TestVersion = tempID

	sequenceProtocol := test.SequenceExecutionProtocol{
		TestVersion: id.NewTestVersionID(SequenceID, 1),
		CaseExecutionProtocols: []id.ProtocolID{
			caseProtocol1.ID(),
			caseProtocol2.ID(),
		},
	}
	server := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	err1 := server.AddSequenceProtocol(&sequenceProtocol)
	sequenceSlice, err2 := server.GetSequenceExecutionProtocols(SequenceID)

	//search for sequenceProtocol
	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Errorf("No Protocols were found for projectID %+v and SequenceID %+v.", projectID, SequenceID)
	}
	if !sliceContainsSequenceProtocol(sequenceSlice, sequenceProtocol) {
		t.Error("Protocol wasn't saved correctly and couldn't be found in store.")
	}
}

func TestStoreRAM_HandleCaseRename(t *testing.T) {
	s := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	cep := caseProtocolTemplate.DeepCopy()
	if err := s.AddCaseProtocol(&cep); err != nil {
		t.Fatal(err)
	}

	var oldID id.TestID
	var newID id.TestID

	caseRenameErr := errors.New(`unsupported operation: test cases currently can not be moved between projects`)

	renameTestStructure := []struct {
		oldID       id.TestID
		newID       id.TestID
		expectedErr error
		errLog      interface{} // String or error
	}{
		{
			CaseID,
			id.NewTestID(id.NewProjectID(ownerID, "other project"), "COOL TEST CASE", true),
			caseRenameErr,
			"Expected error was not returned",
		},
		{
			id.NewTestID(id.NewProjectID("no owner", "other project"), "Cool Test Case", true),
			id.NewTestID(id.NewProjectID("no owner", "other project"), "COOL TEST CASE", true),
			nil,
			caseRenameErr,
		},
		{
			id.NewTestID(id.NewProjectID(ownerID, "other project"), "Cool Test Case", true),
			id.NewTestID(id.NewProjectID(ownerID, "other project"), "COOL TEST CASE", true),
			nil,
			caseRenameErr,
		},
		{
			id.NewTestID(projectID, "Cool Test Case", true),
			id.NewTestID(projectID, "COOL TEST CASE", true),
			nil,
			caseRenameErr,
		},
		{
			CaseID,
			id.NewTestID(projectID, "COOL TEST CASE", true),
			nil,
			caseRenameErr,
		},
	}

	for _, cRenameTest := range renameTestStructure {
		oldID = cRenameTest.oldID
		newID = cRenameTest.newID
		if err := s.HandleCaseRename(cRenameTest.oldID, cRenameTest.newID); !reflect.DeepEqual(err, cRenameTest.expectedErr) {
			t.Fatal(cRenameTest.errLog)
		}
	}

	ps, err := s.GetCaseExecutionProtocols(oldID)
	if err != nil {
		t.Fatal(err)
	}

	if len(ps) > 0 {
		t.Fatal("Protocols can still be retrieved with the old id")
	}

	ps, err = s.GetCaseExecutionProtocols(newID)
	if err != nil {
		t.Fatal(err)
	}

	if len(ps) == 0 {
		t.Fatal("Protocols can not be retrieved with the new id")
	}

	for _, p := range ps {
		if p.TestVersion.TestID != newID {
			t.Fatal("The test id of some protocols was not updated")
		}
	}
}

func TestStoreRAM_HandleSequenceRename(t *testing.T) {
	s := storeRAM{
		caseExecutionProtocols:     make(caseProtocolMap),
		sequenceExecutionProtocols: make(sequenceProtocolMap),
	}

	caseProtocol1 := caseProtocolTemplate.DeepCopy()
	caseProtocol2 := caseProtocolTemplate.DeepCopy()
	caseProtocol2.TestVersion = id.NewTestVersionID(caseProtocolTemplate.TestVersion.TestID, 2)
	sequenceProtocol := test.SequenceExecutionProtocol{
		TestVersion: id.NewTestVersionID(SequenceID, 1),
		CaseExecutionProtocols: []id.ProtocolID{
			caseProtocol1.ID(),
			caseProtocol2.ID(),
		},
	}

	if err := s.AddSequenceProtocol(&sequenceProtocol); err != nil {
		t.Fatal(err)
	}

	var oldID id.TestID
	var newID id.TestID

	seqRenameErr := errors.New(`unsupported operation: test sequences currently can not be moved between projects`)

	renameTestStructure := []struct {
		oldID       id.TestID
		newID       id.TestID
		expectedErr error
		errLog      interface{} // String or error
	}{
		{
			SequenceID,
			id.NewTestID(id.NewProjectID(ownerID, "other project"), "COOL TEST CASE", false),
			seqRenameErr,
			"Expected error was not returned",
		},
		{
			id.NewTestID(id.NewProjectID("no owner", "other project"), "Cool Test Case", false),
			id.NewTestID(id.NewProjectID("no owner", "other project"), "COOL TEST CASE", false),
			nil,
			seqRenameErr,
		},
		{
			id.NewTestID(id.NewProjectID(ownerID, "other project"), "Cool Test Case", false),
			id.NewTestID(id.NewProjectID(ownerID, "other project"), "COOL TEST CASE", false),
			nil,
			seqRenameErr,
		},
		{
			id.NewTestID(projectID, "Cool Test Case", false),
			id.NewTestID(projectID, "COOL TEST CASE", false),
			nil,
			seqRenameErr,
		},
		{
			SequenceID,
			id.NewTestID(projectID, "COOL TEST CASE", false),
			nil,
			seqRenameErr,
		},
	}

	for _, seqRnm := range renameTestStructure {
		oldID = seqRnm.oldID
		newID = seqRnm.newID
		if err := s.HandleSequenceRename(seqRnm.oldID, seqRnm.newID); !reflect.DeepEqual(err, seqRnm.expectedErr) {
			t.Fatal(seqRnm.errLog)
		}
	}

	ps, err := s.GetSequenceExecutionProtocols(oldID)
	if err != nil {
		t.Fatal(err)
	}

	if len(ps) > 0 {
		t.Fatal("Protocols can still be retrieved with the old id")
	}

	ps, err = s.GetSequenceExecutionProtocols(newID)
	if err != nil {
		t.Fatal(err)
	}

	if len(ps) == 0 {
		t.Fatal("Protocols can not be retrieved with the new id")
	}

	for _, p := range ps {
		if p.TestVersion.TestID != newID {
			t.Fatal("The test id of some protocols was not updated")
		}
	}
}
