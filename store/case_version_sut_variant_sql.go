// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

func saveSUTVariantsForCaseVersion(s xorm.Interface, prID int64, cvrID int64, svs map[string]*project.Variant) error {
	for _, sv := range svs {
		svrID, err := lookupSUTVariantRowID(s, prID, sv.Name)
		if err != nil {
			return err
		}

		err = saveSUTVersionsForCaseVersion(s, cvrID, svrID, sv.Versions)
		if err != nil {
			return err
		}
	}

	return nil
}

func listSUTVariantsForCaseVersion(s xorm.Interface, vrID int64) (map[string]*project.Variant, error) {
	svrs, err := listSUTVersionRowsForCaseVersion(s, vrID)
	if err != nil {
		return nil, err
	}

	vs := make(map[int64][]project.Version)
	for _, svr := range svrs {
		v := vs[svr.VariantID]
		sv := sutVersionFromRow(svr)
		v = append(v, sv)
		vs[svr.VariantID] = v
	}

	vars := make(map[string]*project.Variant)
	for id, v := range vs {
		svr, ex, err := getSUTVariantRowByID(s, id)
		if err != nil {
			return nil, err
		}

		if ex {
			variant := new(project.Variant)
			variant.Name = svr.Name
			variant.Versions = v
			vars[svr.Name] = variant
		}
	}

	return vars, nil
}

func getSUTVariantRowByID(s xorm.Interface, id int64) (sutVariantRow, bool, error) {
	var vr sutVariantRow
	ex, err := s.Table(sutVariantTable).ID(id).Get(&vr)
	if err != nil || !ex {
		return sutVariantRow{}, false, err
	}

	return vr, true, nil
}
