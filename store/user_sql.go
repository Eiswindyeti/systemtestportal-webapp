// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"golang.org/x/crypto/bcrypt"
)

type usersSQL struct {
	e *xorm.Engine
}

type userRow struct {
	ID               int64
	Name             string
	DisplayName      string
	Email            string
	RegistrationDate time.Time
}

func userFromRow(ur userRow) *user.User {
	return &user.User{
		Name:             ur.Name,
		DisplayName:      ur.DisplayName,
		EMail:            ur.Email,
		RegistrationDate: ur.RegistrationDate,
	}
}

type userPasswordRow struct {
	ID               int64
	Name             string
	DisplayName      string
	Email            string
	RegistrationDate time.Time
	PasswordHash     []byte
}

func rowFromPasswordUser(u *user.PasswordUser) userPasswordRow {
	return userPasswordRow{
		Name:             u.Name,
		DisplayName:      u.DisplayName,
		Email:            u.EMail,
		RegistrationDate: u.RegistrationDate,
	}
}

func (upr userPasswordRow) asUserRow() userRow {
	return userRow{
		ID:               upr.ID,
		Name:             upr.Name,
		DisplayName:      upr.DisplayName,
		Email:            upr.Email,
		RegistrationDate: upr.RegistrationDate,
	}
}

func (us usersSQL) List() ([]*user.User, error) {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	users, err := listUsers(s)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return users, s.Commit()
}

func (us usersSQL) Get(uID id.ActorID) (*user.User, bool, error) {
	return getUser(us.e, uID)
}

func (us usersSQL) GetByMail(mail string) (*user.User, bool, error) {
	return getUserByMail(us.e, mail)
}

func (us usersSQL) Add(u *user.PasswordUser) error {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	err := saveUser(s, u)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (us usersSQL) Validate(identifier, password string) (*user.User, bool, error) {
	s := us.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, false, err
	}

	u, ex, err := validateUser(s, identifier, password)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, false, err
	}

	return u, ex, s.Commit()
}

func saveUser(s xorm.Interface, u *user.PasswordUser) error {
	oupr, ex, err := getUserPasswordRow(s, u.ID())
	if err != nil {
		return err
	}

	nupr := rowFromPasswordUser(u)
	nupr.PasswordHash, err = bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	if !ex {
		err = insertUserRow(s, &nupr)
	} else {
		nupr.ID = oupr.ID
		err = updateUserRow(s, &nupr)
	}

	if err != nil {
		return err
	}

	return nil
}

func updateUserRow(s xorm.Interface, upr *userPasswordRow) error {
	ur := upr.asUserRow()
	err := updateOwnerForUser(s, &ur)
	if err != nil {
		return err
	}

	aff, err := s.Table(userTable).ID(upr.ID).Update(upr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func insertUserRow(s xorm.Interface, upr *userPasswordRow) error {
	ur := upr.asUserRow()
	err := insertOwnerForUser(s, &ur)
	if err != nil {
		return err
	}

	upr.ID = ur.ID
	_, err = s.Table(userTable).Insert(upr)

	return err
}

func listUsers(s xorm.Interface) ([]*user.User, error) {
	urs, err := listUserRows(s)

	var us []*user.User
	for _, ur := range urs {
		u := userFromRow(ur)
		us = append(us, u)
	}

	return us, err
}

func getUser(s xorm.Interface, uID id.ActorID) (*user.User, bool, error) {
	ur, ex, err := getUserRow(s, uID)
	if err != nil || !ex {
		return nil, false, err
	}

	return userFromRow(ur), true, nil
}

func getUserByMail(s xorm.Interface, mail string) (*user.User, bool, error) {
	ur, ex, err := getUserRowByMail(s, mail)
	if err != nil || !ex {
		return nil, false, err
	}

	return userFromRow(ur), true, nil
}

func validateUser(s xorm.Interface, id, pass string) (*user.User, bool, error) {
	upr, ex, err := getUserPasswordRowByIDAndMail(s, id)
	if err != nil || !ex {
		return nil, false, nil
	}

	err = bcrypt.CompareHashAndPassword(upr.PasswordHash, []byte(pass))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return nil, false, nil
	}

	if err != nil {
		return nil, false, err
	}

	return userFromRow(upr.asUserRow()), true, nil
}

func lookupUserRowIDs(s xorm.Interface, ids ...id.ActorID) ([]int64, error) {
	var rids []int64
	err := s.Table(userTable).Distinct(idField).In(nameField, ids).Find(&rids)
	if err != nil {
		return nil, err
	}

	return rids, nil
}

func listUserRows(s xorm.Interface) ([]userRow, error) {
	var urs []userRow
	err := s.Table(userTable).Asc(nameField).Find(&urs)
	if err != nil {
		return nil, err
	}

	return urs, nil
}

func getUserRow(s xorm.Interface, uID id.ActorID) (userRow, bool, error) {
	ur := userRow{Name: uID.Actor()}
	ex, err := s.Table(userTable).Get(&ur)
	if err != nil || !ex {
		return userRow{}, false, err
	}

	return ur, true, nil
}

func getUserRowByMail(s xorm.Interface, mail string) (userRow, bool, error) {
	ur := userRow{Email: mail}
	ex, err := s.Table(userTable).Get(&ur)
	if err != nil || !ex {
		return userRow{}, false, err
	}

	return ur, true, nil
}

func getUserPasswordRow(s xorm.Interface, id id.ActorID) (userPasswordRow, bool, error) {
	upr := userPasswordRow{Name: id.Actor()}
	ex, err := s.Table(userTable).Get(&upr)
	if err != nil || !ex {
		return userPasswordRow{}, false, err
	}

	return upr, true, nil
}

func getUserPasswordRowByIDAndMail(s xorm.Interface, id string) (userPasswordRow, bool, error) {
	upr := userPasswordRow{Name: id}
	ex, err := s.Table(userTable).Get(&upr)
	if err != nil {
		return userPasswordRow{}, false, err
	}

	if ex {
		return upr, true, nil
	}

	upr = userPasswordRow{Email: id}
	ex, err = s.Table(userTable).Get(&upr)
	if err != nil || !ex {
		return userPasswordRow{}, false, err

	}

	return upr, true, nil
}
