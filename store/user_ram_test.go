// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

var _ Users = &usersRAM{}

func TestInitUsersRAM(t *testing.T) {

	result := initUsersRAM()

	if result.emails == nil {
		t.Error("Email-List of server wasn't initialised. Expected map but was nil")
	}
	if result.list == nil {
		t.Error("User-list of server wasn't initialised. Expected map but was nil")
	}
}

func TestUsersRAM_Add(t *testing.T) {
	testUser := user.PasswordUser{
		Password: "password",
		User:     user.New("TestDisplay", "Username", "mail@test.de")}

	server := initUsersRAM()
	err := server.Add(&testUser)

	if err != nil {
		t.Error(err)
	}

	name, ok := server.emails[testUser.EMail]
	if !ok || name != testUser.Name {
		t.Errorf("EMail wasn't saved in database. Expected %s but was %s", testUser.Name, name)
	}
	resultUser, ok := server.list[testUser.Name]
	if !ok || resultUser != &testUser.User {
		t.Errorf("User wasn't saved in database. Expected %+v but was %+v", testUser, resultUser)
	}
}

func TestServer_AddUser_NilTest(t *testing.T) {
	server := initUsersRAM()
	err := server.Add(nil)

	if err != nil {
		t.Error(err)
	}
}

func TestServer_GetUser(t *testing.T) {
	testUser := user.PasswordUser{
		Password: "password",
		User:     user.New("TestDisplay", "Username", "mail@test.de")}

	server := initUsersRAM()
	err := server.Add(&testUser)

	//get by name
	result, ok, _ := server.Get(testUser.ID())
	if err != nil {
		t.Error(err)
	}
	if !ok {
		t.Errorf("No User found by ActorID %s.", testUser.ID())
	}
	if *result != testUser.User {
		t.Errorf("Wrong user found by AccountName %s. Expected %+v but was %+v", testUser.Name, testUser, result)
	}
	//get by Email
	result, ok, _ = server.GetByMail(testUser.EMail)
	if err != nil {
		t.Error(err)
	}
	if !ok {
		t.Errorf("No User found by Email %s.", testUser.EMail)
	}
	if *result != testUser.User {
		t.Errorf("Wrong user found by Email %s. Expected %+v but was %+v", testUser.EMail, testUser, result)
	}
}

func TestServer_GetUser_negative(t *testing.T) {
	testUser := user.PasswordUser{
		Password: "password",
		User:     user.New("TestDisplay", "Username", "mail@test.de")}

	server := initUsersRAM()
	err := server.Add(&testUser)
	result, ok, _ := server.Get("wrong")

	if err != nil {
		t.Error(err)
	}
	if ok {
		t.Errorf("Found user %+v but expected none", result)
	}
}

func TestServer_Validate_byMail(t *testing.T) {
	mail := "mail@test.de"
	password := "password"

	testUser := user.PasswordUser{
		Password: password,
		User:     user.New("TestDisplay", "Username", mail)}

	server := initUsersRAM()
	err1 := server.Add(&testUser)
	resultUser, ok, err2 := server.Validate(mail, password)

	if err1 != nil {
		t.Error(err1)
	}
	if *resultUser != testUser.User || !ok || err2 != nil {
		t.Errorf("Couldn't validate user %+v via %s and %s", testUser, mail, password)
	}
}

func TestServer_Validate_byUsername(t *testing.T) {
	name := "Username"
	password := "password"
	testUser := user.PasswordUser{
		Password: password,
		User:     user.New("TestDisplay", name, "mail@test.de")}

	server := initUsersRAM()
	err1 := server.Add(&testUser)
	resultUser, ok, err2 := server.Validate(name, password)

	if err1 != nil {
		t.Error(err1)
	}
	if *resultUser != testUser.User || !ok || err2 != nil {
		t.Errorf("Couldn't validate user %+v via %s and %s", testUser, name, password)
	}
}

func TestServer_Validate_negative_Password(t *testing.T) {
	password := "password"
	wrongPassword := "wrongPassword"
	testUser := user.PasswordUser{
		Password: password,
		User:     user.New("TestDisplay", "Username", "mail@test.de")}

	server := initUsersRAM()
	// server.Add encrypts "password" of the user
	if err := server.Add(&testUser); err != nil {
		// Adding to store should not return an error
		t.Error(err)
	}
	resultUser, ok, err := server.Validate("mail@test.de", wrongPassword)
	// Validation should not return an error if credentials do just mismatch
	if err != nil {
		t.Errorf("Validation of user %+v shouldn't have returned an error, "+
			"but it did return the error %s.", resultUser, err)
	}

	if ok {
		t.Errorf("Validated User, but password was wrong."+
			" Send %s but %s was correct so expected false but was true", wrongPassword, password)
	}
	if resultUser != nil {
		t.Errorf("Found wrong or no user. Expected %+v but was %+v.", testUser, resultUser)
	}
}

func TestServer_Validate_negative_Existence(t *testing.T) {
	testUser := user.PasswordUser{
		Password: "password",
		User:     user.New("TestDisplay", "Username", "mail@test.de")}

	server := initUsersRAM()
	err1 := server.Add(&testUser)
	resultUser, ok, err2 := server.Validate("wrong", "password")

	if err1 != nil {
		t.Error(err1)
	}
	if err2 != nil {
		t.Error(err2)
	}
	if ok {
		t.Errorf("Validated user %+v but expected none", resultUser)
	}
}

func TestServer_GetAllUser(t *testing.T) {
	server := initUsersRAM()
	result, _ := server.List()

	if len(server.list) != len(result) {
		t.Errorf("Result contains wrong number of users. Expected %d but was %d.", len(server.list), len(result))
	}
}
