// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"

	_ "github.com/mattn/go-sqlite3"
)

func TestGroupSQLStoreAdd(t *testing.T) {
	type args struct {
		s Groups
		g *group.Group
	}
	type result struct {
		e error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Simple add with empty store",
			a: args{
				s: groupsSQL{emptyEngine(t)},
				g: &group.Group{
					Name:         "New Group",
					Description:  "A new group added for testing purposes",
					Visibility:   visibility.Internal,
					CreationDate: time.Date(2017, time.November, 25, 17, 52, 43, 0, time.Local),
				},
			},
			w: result{
				e: nil,
			},
		},
		{
			name: "Simple add with existing groups in store",
			a: args{
				s: groupsSQL{engineWithSampleData(t)},
				g: &group.Group{
					Name:         "New Group",
					Description:  "A new group added for testing purposes",
					Visibility:   visibility.Private,
					CreationDate: time.Now(),
				},
			},
			w: result{
				e: nil,
			},
		},
		{
			name: "Simple update with existing groups in store",
			a: args{
				s: groupsSQL{engineWithSampleData(t)},
				g: &group.Group{
					Name:         "Another group",
					Description:  "Updated this group just for fun",
					Visibility:   visibility.Private,
					CreationDate: time.Now(),
				},
			},
			w: result{
				e: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.e = tc.a.s.Add(tc.a.g)

			if got != tc.w {
				ls, _ := tc.a.s.List()
				for _, g := range ls {
					t.Log(*g)
				}
				t.Errorf("sqlStore.Add(%v) = '%v', want %v", tc.a.g, got.e, tc.w.e)
			}
		})
	}
}

func TestGroupSQLStoreGet(t *testing.T) {
	type args struct {
		s    Groups
		name string
	}
	type result struct {
		g   *group.Group
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Simple get with empty store",
			a: args{
				s:    groupsSQL{emptyEngine(t)},
				name: "Group 1",
			},
			w: result{
				g:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Simple get with non-existing group in store",
			a: args{
				s:    groupsSQL{engineWithSampleData(t)},
				name: "New Group",
			},
			w: result{
				g:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Simple get with existing group in store",
			a: args{
				s:    groupsSQL{engineWithSampleData(t)},
				name: "First group",
			},
			w: result{
				g: &group.Group{
					Name:         "First group",
					Description:  "My first group",
					Visibility:   visibility.Public,
					CreationDate: time.Date(2017, time.August, 12, 17, 53, 2, 0, time.UTC).In(time.Local),
				},
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {

		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.g, got.ex, got.err = tc.a.s.Get(id.NewActorID(tc.a.name))

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sqlStore.Get(%#v) = (%v, %v, %v), want (%v, %v, %v)", tc.a.name, got.g, got.ex, got.err,
					tc.w.g, tc.w.ex, tc.w.err)
			}
		})
	}
}

func TestGroupSQLStoreList(t *testing.T) {
	type args struct {
		s Groups
	}
	type result struct {
		gs []*group.Group
		e  error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "List with empty store",
			a: args{
				s: groupsSQL{emptyEngine(t)},
			},
			w: result{
				gs: nil,
				e:  nil,
			},
		},
		{
			name: "List with groups in store",
			a: args{
				s: groupsSQL{engineWithSampleData(t)},
			},
			w: result{
				gs: []*group.Group{
					{
						Name:         "Another group",
						Description:  "An example group used for testing purposes",
						Visibility:   visibility.Internal,
						CreationDate: time.Date(2017, time.August, 15, 9, 27, 14, 0, time.UTC).In(time.Local),
					},
					{
						Name:         "First group",
						Description:  "My first group",
						Visibility:   visibility.Public,
						CreationDate: time.Date(2017, time.August, 12, 17, 53, 2, 0, time.UTC).In(time.Local),
					},
				},
				e: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.gs, got.e = tc.a.s.List()

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sqlStore.List() = (%v, %v), want (%v, %v)", got.gs, got.e, tc.w.gs, tc.w.e)
			}
		})
	}
}
