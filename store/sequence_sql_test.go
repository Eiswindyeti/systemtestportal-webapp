// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"encoding/json"
	"errors"
	"reflect"
	"testing"
	"time"

	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

func TestSequencesSQLExists(t *testing.T) {
	type args struct {
		s  Sequences
		id id.TestID
	}

	type result struct {
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:  sequencesSQL{emptyEngine(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				ex:  false,
				err: errors.New(`no owner with name "theo"`),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Hans", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				ex:  false,
				err: errors.New(`no owner with name "Hans"`),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "JUnit"), "All kinds of test suite", false),
			},
			w: result{
				ex:  false,
				err: errors.New(`no project with name "JUnit"`),
			},
		},
		{
			name: "Sequence not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All test suites", false),
			},
			w: result{
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Sequence in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.ex, got.err = tc.a.s.Exists(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sequencesSQL.Exists(%#v) = %v, want %v", tc.a.id, got, tc.w)
			}
		})
	}
}

func TestSequencesSQLGet(t *testing.T) {
	type args struct {
		s  Sequences
		id id.TestID
	}

	type result struct {
		ts  *test.Sequence
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:  sequencesSQL{emptyEngine(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				ts:  nil,
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "theo"`)),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Hans", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				ts:  nil,
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "Hans"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "JUnit"), "All kinds of test suite", false),
			},
			w: result{
				ts:  nil,
				ex:  false,
				err: multierror.Append(errors.New(`no project with name "JUnit"`)),
			},
		},
		{
			name: "Sequence not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All test suites", false),
			},
			w: result{
				ts:  nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Sequence in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				ts: &test.Sequence{
					Project: id.NewProjectID("theo", "GoUnit"),
					Name:    "All kinds of test suite",
					SequenceVersions: []test.SequenceVersion{
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "GoUnit"),
								"All kinds of test suite",
								false,
							),
							VersionNr:     2,
							Description:   "Execution of different types of test suites",
							Preconditions: "",

							Message:      "Add missing test case",
							IsMinor:      false,
							CreationDate: time.Date(2017, time.September, 5, 11, 35, 17, 0, time.UTC).In(time.Local),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr:     1,
											Description:   "A more complex suite of tests imposing restrictions based on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
														" list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"90 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name: "Long test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes longer than 120 minutes",
										},
									},
								},
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Simple test suite",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Simple test suite",
												true,
											),
											VersionNr:     2,
											Description:   "A simple execution of a test suite",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour / 2,
											},

											Message:      "Revise duration",
											IsMinor:      true,
											CreationDate: time.Date(2017, time.September, 5, 5, 0, 18, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:          2,
													Action:         `Select the "Simple Test Suite"`,
													ExpectedResult: "The test case list contains 4 test cases",
												},
												{
													Index:          3,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  4,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"20 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-x86"},
														{Name: "v0.1.1-linux-x86"},
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-amd64"},
														{Name: "v0.1.1-linux-amd64"},
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-x86"},
														{Name: "v0.1.1-win-x86"},
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-amd64"},
														{Name: "v0.1.1-win-amd64"},
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{
												"theo": {
													Name:        "theo",
													DisplayName: "Theodor Heuss",
													EMail:       "theo.heu@example.org",

													RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
												},
											},
										},
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Simple test suite",
												true,
											),
											VersionNr:     1,
											Description:   "A simple execution of a test suite",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 3, 30, 44, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:          2,
													Action:         `Select the "Simple Test Suite"`,
													ExpectedResult: "The test case list contains 4 test cases",
												},
												{
													Index:          3,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:          4,
													Action:         `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-x86"},
														{Name: "v0.1.1-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-amd64"},
														{Name: "v0.1.1-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-x86"},
														{Name: "v0.1.1-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-amd64"},
														{Name: "v0.1.1-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name:        "Improvement needed",
											Description: "This test has a low quality and should be improved",
										},
										{
											Name: "Short test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes no longer than 10 minutes",
										},
									},
								},
							},

							Tester: map[string]*user.User{
								"theo": {
									Name:        "theo",
									DisplayName: "Theodor Heuss",
									EMail:       "theo.heu@example.org",

									RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).
										In(time.Local),
								},
							},
						},
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "GoUnit"),
								"All kinds of test suite",
								false,
							),
							VersionNr:     1,
							Description:   "Test execution with different types of test suite",
							Preconditions: "",

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2017, time.September, 5, 11, 27, 43, 0, time.UTC).In(time.Local),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr:     1,
											Description:   "A more complex suite of tests imposing restrictions based on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
														" list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"90 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name: "Long test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes longer than 120 minutes",
										},
									},
								},
							},

							Tester: map[string]*user.User{
								"theo": {
									Name:        "theo",
									DisplayName: "Theodor Heuss",
									EMail:       "theo.heu@example.org",

									RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).
										In(time.Local),
								},
							},
						},
					},

					Labels: []project.Label{
						{
							Name:        "Improvement needed",
							Description: "This test has a low quality and should be improved",
						},
						{
							Name: "Long test",
							Description: "This test, including setup and tear down of the test environment, takes" +
								" longer than 120 minutes",
						},
					},
				},
				ex:  true,
				err: nil,
			},
		},
		{
			name: "Sequence with deleted test case",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "AW-987"), "Test sequence with deleted test case", false),
			},
			w: result{
				ts: &test.Sequence{
					Project: id.NewProjectID("theo", "AW-987"),
					Name:    "Test sequence with deleted test case",
					SequenceVersions: []test.SequenceVersion{
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "AW-987"),
								"Test sequence with deleted test case",
								false,
							),
							VersionNr:     1,
							Description:   "A test sequence containing a deleted test case",
							Preconditions: "",

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2017, time.November, 17, 18, 53, 26, 0, time.UTC).In(time.Local),

							Tester: map[string]*user.User{},
						},
					},
				},
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.ts, got.ex, got.err = tc.a.s.Get(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sequencesSQL.Get(%#v) = %v, want %v", tc.a.id, got, tc.w)
			}
		})
	}
}
func TestSequencesSQLList(t *testing.T) {
	type args struct {
		s  Sequences
		id id.ProjectID
	}

	type result struct {
		tss []*test.Sequence
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:  sequencesSQL{emptyEngine(t)},
				id: id.NewProjectID("theo", "GoUnit"),
			},
			w: result{
				tss: nil,
				err: multierror.Append(errors.New(`no owner with name "theo"`)),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewProjectID("Hans", "GoUnit"),
			},
			w: result{
				tss: nil,
				err: multierror.Append(errors.New(`no owner with name "Hans"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewProjectID("theo", "JUnit"),
			},
			w: result{
				tss: nil,
				err: multierror.Append(errors.New(`no project with name "JUnit"`)),
			},
		},
		{
			name: "Project in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewProjectID("theo", "GoUnit"),
			},
			w: result{
				tss: []*test.Sequence{
					{
						Project: id.NewProjectID("theo", "GoUnit"),
						Name:    "All kinds of test suite",
						SequenceVersions: []test.SequenceVersion{
							{
								Testsequence: id.NewTestID(
									id.NewProjectID("theo", "GoUnit"),
									"All kinds of test suite",
									false,
								),
								VersionNr:     2,
								Description:   "Execution of different types of test suites",
								Preconditions: "",

								Message:      "Add missing test case",
								IsMinor:      false,
								CreationDate: time.Date(2017, time.September, 5, 11, 35, 17, 0, time.UTC).In(time.Local),

								Cases: []test.Case{
									{
										Project: id.NewProjectID("theo", "GoUnit"),
										Name:    "Complete test suite with rules",

										TestCaseVersions: []test.CaseVersion{
											{
												Case: id.NewTestID(
													id.NewProjectID("theo", "GoUnit"),
													"Complete test suite with rules",
													true,
												),
												VersionNr:     1,
												Description:   "A more complex suite of tests imposing restrictions based on rules",
												Preconditions: "",
												Duration: duration.Duration{
													Duration: time.Hour * 2,
												},

												Message:      "Initial version",
												IsMinor:      false,
												CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

												Steps: []test.Step{
													{
														Index:          1,
														Action:         `Click on the "Execute Test Suite" button`,
														ExpectedResult: `The application should display the execution screen`,
													},
													{
														Index:  2,
														Action: `Select the "Complex Test Suite"`,
														ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
															" list contains 3 rules",
													},
													{
														Index:          3,
														Action:         `Change the value of the "Iterations" rule to 15`,
														ExpectedResult: `The label displays the new value`,
													},
													{
														Index:          4,
														Action:         `Click on start execution`,
														ExpectedResult: `The application shows a progress screen`,
													},
													{
														Index:  5,
														Action: `Wait for the execution to finish`,
														ExpectedResult: "The execution should complete successfully in less than " +
															"90 minutes",
													},
												},

												Variants: map[string]*project.Variant{
													"Linux 32bit": {
														Name: "Linux 32bit",
														Versions: []project.Version{
															{Name: "v1.0.0-linux-x86"},
														},
													},
													"Linux 64bit": {
														Name: "Linux 64bit",
														Versions: []project.Version{
															{Name: "v1.0.0-linux-amd64"},
														},
													},
													"Windows 32bit": {
														Name: "Windows 32bit",
														Versions: []project.Version{
															{Name: "v1.0.0-win-x86"},
														},
													},
													"Windows 64bit": {
														Name: "Windows 64bit",
														Versions: []project.Version{
															{Name: "v1.0.0-win-amd64"},
														},
													},
												},

												Tester: map[string]*user.User{},
											},
										},

										Labels: []project.Label{
											{
												Name: "Long test",
												Description: "This test, including setup and tear down of the test" +
													" environment, takes longer than 120 minutes",
											},
										},
									},
									{
										Project: id.NewProjectID("theo", "GoUnit"),
										Name:    "Simple test suite",

										TestCaseVersions: []test.CaseVersion{
											{
												Case: id.NewTestID(
													id.NewProjectID("theo", "GoUnit"),
													"Simple test suite",
													true,
												),
												VersionNr:     2,
												Description:   "A simple execution of a test suite",
												Preconditions: "",
												Duration: duration.Duration{
													Duration: time.Hour / 2,
												},

												Message:      "Revise duration",
												IsMinor:      true,
												CreationDate: time.Date(2017, time.September, 5, 5, 0, 18, 0, time.UTC).In(time.Local),

												Steps: []test.Step{
													{
														Index:          1,
														Action:         `Click on the "Execute Test Suite" button`,
														ExpectedResult: `The application should display the execution screen`,
													},
													{
														Index:          2,
														Action:         `Select the "Simple Test Suite"`,
														ExpectedResult: "The test case list contains 4 test cases",
													},
													{
														Index:          3,
														Action:         `Click on start execution`,
														ExpectedResult: `The application shows a progress screen`,
													},
													{
														Index:  4,
														Action: `Wait for the execution to finish`,
														ExpectedResult: "The execution should complete successfully in less than " +
															"20 minutes",
													},
												},

												Variants: map[string]*project.Variant{
													"Linux 32bit": {
														Name: "Linux 32bit",
														Versions: []project.Version{
															{Name: "v0.1.0-linux-x86"},
															{Name: "v0.1.1-linux-x86"},
															{Name: "v1.0.0-linux-x86"},
														},
													},
													"Linux 64bit": {
														Name: "Linux 64bit",
														Versions: []project.Version{
															{Name: "v0.1.0-linux-amd64"},
															{Name: "v0.1.1-linux-amd64"},
															{Name: "v1.0.0-linux-amd64"},
														},
													},
													"Windows 32bit": {
														Name: "Windows 32bit",
														Versions: []project.Version{
															{Name: "v0.1.0-win-x86"},
															{Name: "v0.1.1-win-x86"},
															{Name: "v1.0.0-win-x86"},
														},
													},
													"Windows 64bit": {
														Name: "Windows 64bit",
														Versions: []project.Version{
															{Name: "v0.1.0-win-amd64"},
															{Name: "v0.1.1-win-amd64"},
															{Name: "v1.0.0-win-amd64"},
														},
													},
												},

												Tester: map[string]*user.User{
													"theo": {
														Name:        "theo",
														DisplayName: "Theodor Heuss",
														EMail:       "theo.heu@example.org",

														RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0,
															time.UTC).In(time.Local),
													},
												},
											},
											{
												Case: id.NewTestID(
													id.NewProjectID("theo", "GoUnit"),
													"Simple test suite",
													true,
												),
												VersionNr:     1,
												Description:   "A simple execution of a test suite",
												Preconditions: "",
												Duration: duration.Duration{
													Duration: time.Hour,
												},

												Message:      "Initial version",
												IsMinor:      false,
												CreationDate: time.Date(2017, time.September, 5, 3, 30, 44, 0, time.UTC).In(time.Local),

												Steps: []test.Step{
													{
														Index:          1,
														Action:         `Click on the "Execute Test Suite" button`,
														ExpectedResult: `The application should display the execution screen`,
													},
													{
														Index:          2,
														Action:         `Select the "Simple Test Suite"`,
														ExpectedResult: "The test case list contains 4 test cases",
													},
													{
														Index:          3,
														Action:         `Click on start execution`,
														ExpectedResult: `The application shows a progress screen`,
													},
													{
														Index:          4,
														Action:         `Wait for the execution to finish`,
														ExpectedResult: "The execution should complete successfully",
													},
												},

												Variants: map[string]*project.Variant{
													"Linux 32bit": {
														Name: "Linux 32bit",
														Versions: []project.Version{
															{Name: "v0.1.0-linux-x86"},
															{Name: "v0.1.1-linux-x86"},
														},
													},
													"Linux 64bit": {
														Name: "Linux 64bit",
														Versions: []project.Version{
															{Name: "v0.1.0-linux-amd64"},
															{Name: "v0.1.1-linux-amd64"},
														},
													},
													"Windows 32bit": {
														Name: "Windows 32bit",
														Versions: []project.Version{
															{Name: "v0.1.0-win-x86"},
															{Name: "v0.1.1-win-x86"},
														},
													},
													"Windows 64bit": {
														Name: "Windows 64bit",
														Versions: []project.Version{
															{Name: "v0.1.0-win-amd64"},
															{Name: "v0.1.1-win-amd64"},
														},
													},
												},

												Tester: map[string]*user.User{},
											},
										},

										Labels: []project.Label{
											{
												Name:        "Improvement needed",
												Description: "This test has a low quality and should be improved",
											},
											{
												Name: "Short test",
												Description: "This test, including setup and tear down of the test" +
													" environment, takes no longer than 10 minutes",
											},
										},
									},
								},

								Tester: map[string]*user.User{
									"theo": {
										Name:        "theo",
										DisplayName: "Theodor Heuss",
										EMail:       "theo.heu@example.org",

										RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).
											In(time.Local),
									},
								},
							},
							{
								Testsequence: id.NewTestID(
									id.NewProjectID("theo", "GoUnit"),
									"All kinds of test suite",
									false,
								),
								VersionNr:     1,
								Description:   "Test execution with different types of test suite",
								Preconditions: "",

								Message:      "Initial version",
								IsMinor:      false,
								CreationDate: time.Date(2017, time.September, 5, 11, 27, 43, 0, time.UTC).In(time.Local),

								Cases: []test.Case{
									{
										Project: id.NewProjectID("theo", "GoUnit"),
										Name:    "Complete test suite with rules",

										TestCaseVersions: []test.CaseVersion{
											{
												Case: id.NewTestID(
													id.NewProjectID("theo", "GoUnit"),
													"Complete test suite with rules",
													true,
												),
												VersionNr:     1,
												Description:   "A more complex suite of tests imposing restrictions based on rules",
												Preconditions: "",
												Duration: duration.Duration{
													Duration: time.Hour * 2,
												},

												Message:      "Initial version",
												IsMinor:      false,
												CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

												Steps: []test.Step{
													{
														Index:          1,
														Action:         `Click on the "Execute Test Suite" button`,
														ExpectedResult: `The application should display the execution screen`,
													},
													{
														Index:  2,
														Action: `Select the "Complex Test Suite"`,
														ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
															" list contains 3 rules",
													},
													{
														Index:          3,
														Action:         `Change the value of the "Iterations" rule to 15`,
														ExpectedResult: `The label displays the new value`,
													},
													{
														Index:          4,
														Action:         `Click on start execution`,
														ExpectedResult: `The application shows a progress screen`,
													},
													{
														Index:  5,
														Action: `Wait for the execution to finish`,
														ExpectedResult: "The execution should complete successfully in less than " +
															"90 minutes",
													},
												},

												Variants: map[string]*project.Variant{
													"Linux 32bit": {
														Name: "Linux 32bit",
														Versions: []project.Version{
															{Name: "v1.0.0-linux-x86"},
														},
													},
													"Linux 64bit": {
														Name: "Linux 64bit",
														Versions: []project.Version{
															{Name: "v1.0.0-linux-amd64"},
														},
													},
													"Windows 32bit": {
														Name: "Windows 32bit",
														Versions: []project.Version{
															{Name: "v1.0.0-win-x86"},
														},
													},
													"Windows 64bit": {
														Name: "Windows 64bit",
														Versions: []project.Version{
															{Name: "v1.0.0-win-amd64"},
														},
													},
												},

												Tester: map[string]*user.User{},
											},
										},

										Labels: []project.Label{
											{
												Name: "Long test",
												Description: "This test, including setup and tear down of the test" +
													" environment, takes longer than 120 minutes",
											},
										},
									},
								},

								Tester: map[string]*user.User{
									"theo": {
										Name:        "theo",
										DisplayName: "Theodor Heuss",
										EMail:       "theo.heu@example.org",

										RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).
											In(time.Local),
									},
								},
							},
						},

						Labels: []project.Label{
							{
								Name:        "Improvement needed",
								Description: "This test has a low quality and should be improved",
							},
							{
								Name: "Long test",
								Description: "This test, including setup and tear down of the test environment, takes" +
									" longer than 120 minutes",
							},
						},
					},
				},
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.tss, got.err = tc.a.s.List(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sequencesSQL.List(%#v) = %v, want %v", tc.a.id, got, tc.w)

			}
		})
	}
}

func TestSequencesSQLAdd(t *testing.T) {
	type args struct {
		s  Sequences
		ts *test.Sequence
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name         string
		a            args
		w            result
		shouldCreate bool
	}{
		{
			name: "Empty store",
			a: args{
				s: sequencesSQL{emptyEngine(t)},
				ts: &test.Sequence{
					Project: id.NewProjectID("theo", "GoUnit"),
					Name:    "New test sequence",
					SequenceVersions: []test.SequenceVersion{
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "GoUnit"),
								"New test sequence",
								false,
							),
							VersionNr:     1,
							Description:   "A newly created test sequence",
							Preconditions: "",

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Now().Round(time.Second),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr: 1,
											Description: "A more complex suite of tests imposing restrictions based" +
												" on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message: "Initial version",
											IsMinor: false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).
												In(time.Local),

											Steps: []test.Step{
												{
													Index:  1,
													Action: `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution` +
														` screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n" +
														"- The test rule list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in" +
														" less than 90 minutes",
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "theo"`)),
			},
			shouldCreate: false,
		},
		{
			name: "Owner not in store",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				ts: &test.Sequence{
					Project: id.NewProjectID("Hans", "GoUnit"),
					Name:    "New test sequence",
					SequenceVersions: []test.SequenceVersion{
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("Hans", "GoUnit"),
								"New test sequence",
								false,
							),
							VersionNr:     1,
							Description:   "A newly created test sequence",
							Preconditions: "",

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Now().Round(time.Second),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("Hans", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("Hans", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr: 1,
											Description: "A more complex suite of tests imposing restrictions based" +
												" on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message: "Initial version",
											IsMinor: false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).
												In(time.Local),

											Steps: []test.Step{
												{
													Index:  1,
													Action: `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution` +
														` screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n" +
														"- The test rule list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in" +
														" less than 90 minutes",
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Hans"`)),
			},
			shouldCreate: false,
		},
		{
			name: "Project not in store",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				ts: &test.Sequence{
					Project: id.NewProjectID("theo", "JUnit"),
					Name:    "New test sequence",
					SequenceVersions: []test.SequenceVersion{
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "JUnit"),
								"New test sequence",
								false,
							),
							VersionNr:     1,
							Description:   "A newly created test sequence",
							Preconditions: "",

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Now().Round(time.Second),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "JUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "JUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr: 1,
											Description: "A more complex suite of tests imposing restrictions based" +
												" on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message: "Initial version",
											IsMinor: false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).
												In(time.Local),

											Steps: []test.Step{
												{
													Index:  1,
													Action: `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution` +
														` screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n" +
														"- The test rule list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in" +
														" less than 90 minutes",
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
			w: result{
				err: multierror.Append(errors.New(`no project with name "JUnit"`)),
			},
			shouldCreate: false,
		},
		{
			name: "Insert new sequence",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				ts: &test.Sequence{
					Project: id.NewProjectID("theo", "GoUnit"),
					Name:    "New test sequence",
					SequenceVersions: []test.SequenceVersion{
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "GoUnit"),
								"New test sequence",
								false,
							),
							VersionNr:     1,
							Description:   "A newly created test sequence",
							Preconditions: "",

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Now().Round(time.Second),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr:     1,
											Description:   "A more complex suite of tests imposing restrictions based on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
														" list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"90 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name: "Long test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes longer than 120 minutes",
										},
									},
								},
							},

							Tester: map[string]*user.User{},
						},
					},

					Labels: []project.Label{
						{
							Name: "Long test",
							Description: "This test, including setup and tear down of the test environment, takes" +
								" longer than 120 minutes",
						},
					},
				},
			},
			w: result{
				err: nil,
			},
			shouldCreate: true,
		},
		{
			name: "Update a sequence",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				ts: &test.Sequence{
					Project: id.NewProjectID("theo", "GoUnit"),
					Name:    "All kinds of test suite",
					SequenceVersions: []test.SequenceVersion{
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "GoUnit"),
								"All kinds of test suite",
								false,
							),
							VersionNr:     3,
							Description:   "Execution of different types of test suites",
							Preconditions: "The test system should have been started",

							Message:      "Add precondition",
							IsMinor:      false,
							CreationDate: time.Now().Round(time.Second),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr:     1,
											Description:   "A more complex suite of tests imposing restrictions based on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
														" list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"90 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name: "Long test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes longer than 120 minutes",
										},
									},
								},
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Simple test suite",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Simple test suite",
												true,
											),
											VersionNr:     2,
											Description:   "A simple execution of a test suite",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour / 2,
											},

											Message:      "Revise duration",
											IsMinor:      true,
											CreationDate: time.Date(2017, time.September, 5, 5, 0, 18, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:          2,
													Action:         `Select the "Simple Test Suite"`,
													ExpectedResult: "The test case list contains 4 test cases",
												},
												{
													Index:          3,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  4,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"20 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-x86"},
														{Name: "v0.1.1-linux-x86"},
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-amd64"},
														{Name: "v0.1.1-linux-amd64"},
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-x86"},
														{Name: "v0.1.1-win-x86"},
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-amd64"},
														{Name: "v0.1.1-win-amd64"},
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{
												"theo": {
													Name:        "theo",
													DisplayName: "Theodor Heuss",
													EMail:       "theo.heu@example.org",

													RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
												},
											},
										},
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Simple test suite",
												true,
											),
											VersionNr:     1,
											Description:   "A simple execution of a test suite",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 3, 30, 44, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:          2,
													Action:         `Select the "Simple Test Suite"`,
													ExpectedResult: "The test case list contains 4 test cases",
												},
												{
													Index:          3,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:          4,
													Action:         `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-x86"},
														{Name: "v0.1.1-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-amd64"},
														{Name: "v0.1.1-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-x86"},
														{Name: "v0.1.1-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-amd64"},
														{Name: "v0.1.1-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name:        "Improvement needed",
											Description: "This test has a low quality and should be improved",
										},
										{
											Name: "Short test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes no longer than 10 minutes",
										},
									},
								},
							},

							Tester: map[string]*user.User{
								"Franz": {
									Name:        "Franz",
									DisplayName: "Frain",
									EMail:       "f.thor@example.org",

									RegistrationDate: time.Date(2017, time.July, 12, 9, 6, 28, 0, time.UTC).
										In(time.Local),
								},
							},
						},
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "GoUnit"),
								"All kinds of test suite",
								false,
							),
							VersionNr:     2,
							Description:   "Execution of different types of test suites",
							Preconditions: "",

							Message:      "Add missing test case",
							IsMinor:      false,
							CreationDate: time.Date(2017, time.September, 5, 11, 35, 17, 0, time.UTC).In(time.Local),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr:     1,
											Description:   "A more complex suite of tests imposing restrictions based on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
														" list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"90 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name: "Long test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes longer than 120 minutes",
										},
									},
								},
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Simple test suite",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Simple test suite",
												true,
											),
											VersionNr:     2,
											Description:   "A simple execution of a test suite",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour / 2,
											},

											Message:      "Revise duration",
											IsMinor:      true,
											CreationDate: time.Date(2017, time.September, 5, 5, 0, 18, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:          2,
													Action:         `Select the "Simple Test Suite"`,
													ExpectedResult: "The test case list contains 4 test cases",
												},
												{
													Index:          3,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  4,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"20 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-x86"},
														{Name: "v0.1.1-linux-x86"},
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-amd64"},
														{Name: "v0.1.1-linux-amd64"},
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-x86"},
														{Name: "v0.1.1-win-x86"},
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-amd64"},
														{Name: "v0.1.1-win-amd64"},
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{
												"theo": {
													Name:        "theo",
													DisplayName: "Theodor Heuss",
													EMail:       "theo.heu@example.org",

													RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
												},
											},
										},
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Simple test suite",
												true,
											),
											VersionNr:     1,
											Description:   "A simple execution of a test suite",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 3, 30, 44, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:          2,
													Action:         `Select the "Simple Test Suite"`,
													ExpectedResult: "The test case list contains 4 test cases",
												},
												{
													Index:          3,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:          4,
													Action:         `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-x86"},
														{Name: "v0.1.1-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-linux-amd64"},
														{Name: "v0.1.1-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-x86"},
														{Name: "v0.1.1-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v0.1.0-win-amd64"},
														{Name: "v0.1.1-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name:        "Improvement needed",
											Description: "This test has a low quality and should be improved",
										},
										{
											Name: "Short test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes no longer than 10 minutes",
										},
									},
								},
							},

							Tester: map[string]*user.User{
								"theo": {
									Name:        "theo",
									DisplayName: "Theodor Heuss",
									EMail:       "theo.heu@example.org",

									RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
								},
							},
						},
						{
							Testsequence: id.NewTestID(
								id.NewProjectID("theo", "GoUnit"),
								"All kinds of test suite",
								false,
							),
							VersionNr:     1,
							Description:   "Test execution with different types of test suite",
							Preconditions: "",

							Message:      "Initial version",
							IsMinor:      false,
							CreationDate: time.Date(2017, time.September, 5, 11, 27, 43, 0, time.UTC).In(time.Local),

							Cases: []test.Case{
								{
									Project: id.NewProjectID("theo", "GoUnit"),
									Name:    "Complete test suite with rules",

									TestCaseVersions: []test.CaseVersion{
										{
											Case: id.NewTestID(
												id.NewProjectID("theo", "GoUnit"),
												"Complete test suite with rules",
												true,
											),
											VersionNr:     1,
											Description:   "A more complex suite of tests imposing restrictions based on rules",
											Preconditions: "",
											Duration: duration.Duration{
												Duration: time.Hour * 2,
											},

											Message:      "Initial version",
											IsMinor:      false,
											CreationDate: time.Date(2017, time.September, 5, 10, 16, 29, 0, time.UTC).In(time.Local),

											Steps: []test.Step{
												{
													Index:          1,
													Action:         `Click on the "Execute Test Suite" button`,
													ExpectedResult: `The application should display the execution screen`,
												},
												{
													Index:  2,
													Action: `Select the "Complex Test Suite"`,
													ExpectedResult: "- The test case list contains 25 test cases\n- The test rule" +
														" list contains 3 rules",
												},
												{
													Index:          3,
													Action:         `Change the value of the "Iterations" rule to 15`,
													ExpectedResult: `The label displays the new value`,
												},
												{
													Index:          4,
													Action:         `Click on start execution`,
													ExpectedResult: `The application shows a progress screen`,
												},
												{
													Index:  5,
													Action: `Wait for the execution to finish`,
													ExpectedResult: "The execution should complete successfully in less than " +
														"90 minutes",
												},
											},

											Variants: map[string]*project.Variant{
												"Linux 32bit": {
													Name: "Linux 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-x86"},
													},
												},
												"Linux 64bit": {
													Name: "Linux 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-linux-amd64"},
													},
												},
												"Windows 32bit": {
													Name: "Windows 32bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-x86"},
													},
												},
												"Windows 64bit": {
													Name: "Windows 64bit",
													Versions: []project.Version{
														{Name: "v1.0.0-win-amd64"},
													},
												},
											},

											Tester: map[string]*user.User{},
										},
									},

									Labels: []project.Label{
										{
											Name: "Long test",
											Description: "This test, including setup and tear down of the test" +
												" environment, takes longer than 120 minutes",
										},
									},
								},
							},

							Tester: map[string]*user.User{
								"theo": {
									Name:        "theo",
									DisplayName: "Theodor Heuss",
									EMail:       "theo.heu@example.org",

									RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).
										In(time.Local),
								},
							},
						},
					},

					Labels: []project.Label{
						{
							Name: "Deprecated",
							Description: "This test is no longer applicable to new versions and only kept around for" +
								" historic reasons",
						},
						{
							Name: "Long test",
							Description: "This test, including setup and tear down of the test environment, takes" +
								" longer than 120 minutes",
						},
					},
				},
			},
			w: result{
				err: nil,
			},
			shouldCreate: true,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.err = tc.a.s.Add(tc.a.ts)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sequencesSQL.Add(%#v) = %v, want %v", tc.a.ts, got.err, tc.w.err)
				return
			}

			if !tc.shouldCreate {
				return
			}

			atc, ex, err := tc.a.s.Get(tc.a.ts.ID())
			if err != nil {
				t.Error("Error when trying to retrieve created test sequence", err)
			}

			if !ex {
				t.Error("Created test sequence does not exist")
			}

			if !reflect.DeepEqual(tc.a.ts, atc) {
				t.Error("Not all information saved in the test sequence could be retrieved")

				j, err := json.Marshal(atc)
				handleError(t, err)
				t.Log(string(j))
				j, err = json.Marshal(tc.a.ts)
				handleError(t, err)
				t.Log(string(j))
			}
		})
	}
}

func TestSequencesSQLRename(t *testing.T) {
	type args struct {
		s Sequences
		o id.TestID
		n id.TestID
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s: sequencesSQL{emptyEngine(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"All kinds of test suite",
					false,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"All types of test suites",
					false,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "theo"`)),
			},
		},
		{
			name: "Add with owner not in store",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("Hans"), "GoUnit"),
					"All kinds of test suite",
					false,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("Hans"), "GoUnit"),
					"All types of test suites",
					false,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Hans"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "JUnit"),
					"All kinds of test suite",
					false,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "JUnit"),
					"All types of test suites",
					false,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no project with name "JUnit"`)),
			},
		},
		{
			name: "Test case not in store",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"All kind of test suite",
					false,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"All types of test suites",
					false,
				),
			},
			w: result{
				err: multierror.Append(errors.New(`no test case with name "All kind of test suite"`)),
			},
		},
		{
			name: "Move test case",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"All kinds of test suite",
					false,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "AW-987"),
					"All types of test suites",
					false,
				),
			},
			w: result{
				err: multierror.Append(
					errors.New(`unsupported operation: test sequences currently can not be moved between projects`),
				),
			},
		},
		{
			name: "Successful rename",
			a: args{
				s: sequencesSQL{engineWithSampleData(t)},
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"All kinds of test suite",
					false,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("theo"), "GoUnit"),
					"All types of test suites",
					false,
				),
			},
			w: result{
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.err = tc.a.s.Rename(tc.a.o, tc.a.n)

			if !reflect.DeepEqual(got, tc.w) {
				t.Log(got.err)
				t.Log(tc.w.err)
				t.Errorf("sequencesSQL.Rename(%v, %v) = %v, want %v", tc.a.o, tc.a.n, got, tc.w)
				return
			}

			if tc.w.err != nil {
				return
			}

			_, ex, _ := tc.a.s.Get(tc.a.o)
			if ex {
				t.Error("Renamed test sequence can still be retrieved with old id")
				return
			}

			_, ex, _ = tc.a.s.Get(tc.a.n)
			if !ex {
				t.Error("Renamed test sequence can not be retrieved with new id")
				return
			}
		})
	}
}

func TestSequenceSQLDelete(t *testing.T) {
	type args struct {
		s  Sequences
		id id.TestID
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:  sequencesSQL{emptyEngine(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "theo"`)),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("Hans", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				err: multierror.Append(errors.New(`no owner with name "Hans"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "JUnit"), "All kinds of test suite", false),
			},
			w: result{
				err: multierror.Append(errors.New(`no project with name "JUnit"`)),
			},
		},
		{
			name: "Sequence not in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All test suites", false),
			},
			w: result{
				err: multierror.Append(errors.New(`no test sequence with name "All test suites"`)),
			},
		},
		{
			name: "Sequence in store",
			a: args{
				s:  sequencesSQL{engineWithSampleData(t)},
				id: id.NewTestID(id.NewProjectID("theo", "GoUnit"), "All kinds of test suite", false),
			},
			w: result{
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			existed, err := tc.a.s.Exists(tc.a.id)
			t.Log("Test sequence to delete exists:", existed)

			if err != nil {
				t.Log(err)
			}

			var got result
			got.err = tc.a.s.Delete(tc.a.id)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sequencesSQL.Delete(%v) = %v, want %v", tc.a.id, got.err, tc.w.err)
				return
			}

			if _, ex, _ := tc.a.s.Get(tc.a.id); ex {
				t.Error("Deleted test sequence can still be retrieved with Get")
			}
		})
	}
}
