-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO users (id, name, display_name, email, password_hash, registration_date)
VALUES
    (
        1, 'theo', 'Theodor Heuss', 'theo.heu@example.org',
            '$2a$10$qMh342t10pNFUoQFvm5AC.LsmKZN8HOfFoMp.Z8Oep/Hy.7ZcFpC.', '2017-07-23 18:59:23'
    ),
    (
        4, 'Franz', 'Frain', 'f.thor@example.org',
            '$2a$10$9rUxmlStYwW/riP3HGAeyuniely43n50NEGYiMHbkoLByLMTIFoV2', '2017-07-12 09:06:28'
    );

-- +migrate Down
DELETE FROM users;