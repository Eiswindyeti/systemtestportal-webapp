-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO projects (id, owner_id, name, description, visibility, creation_date)
VALUES
    (1, 3, 'Test project', 'A project used for testing purposes', 1, '2017-08-15 09:55:48'),
    (2, 1, 'GoUnit', 'Unit testing go code', 1, '2017-09-05 03:26:17'),
    (3, 1, 'AW-987', 'Automation framework for windows operating systems', 3, '2017-10-30 23:10:54');

-- +migrate Down
DELETE FROM projects;