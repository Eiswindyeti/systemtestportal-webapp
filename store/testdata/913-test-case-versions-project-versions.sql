-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO test_case_version_project_versions (test_case_version, project_version)
VALUES
    (2,  1),
    (2,  2),
    (2,  3),
    (2,  4),
    (2,  7),
    (2,  8),
    (2,  9),
    (2, 10),
    (1,  5),
    (1,  6),
    (3,  1),
    (3,  2),
    (3,  3),
    (3,  4),
    (3,  7),
    (3,  8),
    (3,  9),
    (3, 10),
    (3, 35),
    (3, 36),
    (3, 37),
    (3, 38),
    (4, 35),
    (4, 36),
    (4, 37),
    (4, 38);

-- +migrate Down
DELETE FROM test_case_version_project_versions;