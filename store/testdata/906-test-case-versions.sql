-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO test_case_versions (
    id, test_case_id, version_nr, description, preconditions, duration, message, is_minor, creation_date
) VALUES
    (
        1, 1, 1, 'An example for demonstrating the abilities of a test case', '', 600000000000, 'Initial version', 0, 
            '2017-08-15 10:15:36'
    ),
    (2, 2, 1, 'A simple execution of a test suite', '', 3600000000000, 'Initial version', 0, '2017-09-05 03:30:44'),
    (3, 2, 2, 'A simple execution of a test suite', '', 1800000000000, 'Revise duration', 1, '2017-09-05 05:00:18'),
    (
        4, 3, 1, 'A more complex suite of tests imposing restrictions based on rules', '', 7200000000000, 
        'Initial version', 0, '2017-09-05 10:16:29'
    );

-- +migrate Down
DELETE FROM test_case_versions;