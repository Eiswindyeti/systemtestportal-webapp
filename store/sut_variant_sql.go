// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"fmt"

	"github.com/go-xorm/xorm"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	errorNoSUTVariant = "no variant with name %#v"
)

type sutVariantRow struct {
	ID        int64
	ProjectID int64
	Name      string
}

func sutVariantFromRow(vr sutVariantRow) *project.Variant {
	return &project.Variant{
		Name: vr.Name,
	}
}

func rowFromSUTVariant(v *project.Variant) sutVariantRow {
	return sutVariantRow{
		Name: v.Name,
	}
}

func saveSUTVariants(s xorm.Interface, prID int64, nvs map[string]*project.Variant) error {
	ovs, err := listSUTVariantRows(s, prID)
	if err != nil {
		return err
	}

	var dvrs []sutVariantRow
	for _, ovr := range ovs {
		if _, ok := nvs[ovr.Name]; !ok {
			dvrs = append(dvrs, ovr)
		}
	}

	err = deleteSUTVariantRows(s, dvrs...)
	if err != nil {
		return err
	}

	for _, nv := range nvs {
		err = saveSUTVariant(s, prID, *nv)
		if err != nil {
			return err
		}
	}

	return nil
}

func saveSUTVariant(s xorm.Interface, prID int64, v project.Variant) error {
	ovr := sutVariantRow{ProjectID: prID, Name: v.Name}
	ex, err := s.Table(sutVariantTable).Get(&ovr)
	if err != nil {
		return err
	}

	nvr := rowFromSUTVariant(&v)
	nvr.ProjectID = prID
	if !ex {
		err = insertSUTVariantRow(s, &nvr)
	} else {
		nvr.ID = ovr.ID
		err = updateSUTVariantRow(s, &nvr)
	}

	if err != nil {
		return err
	}

	err = saveSUTVersions(s, nvr.ID, v.Versions)

	return err
}

func insertSUTVariantRow(s xorm.Interface, vr *sutVariantRow) error {
	_, err := s.Table(sutVariantTable).Insert(vr)
	return err
}

func updateSUTVariantRow(s xorm.Interface, vr *sutVariantRow) error {
	aff, err := s.Table(sutVariantTable).ID(vr.ID).Update(vr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func deleteSUTVariantRows(s xorm.Interface, vrs ...sutVariantRow) error {
	var ids []int64
	for _, vr := range vrs {
		ids = append(ids, vr.ID)
	}

	_, err := s.Table(sutVariantTable).In(idField, ids).Delete(&sutVariantRow{})
	return err
}

func listSUTVariants(s xorm.Interface, prID int64) (map[string]*project.Variant, error) {
	vrs, err := listSUTVariantRows(s, prID)
	if err != nil {
		return nil, err
	}

	vs := make(map[string]*project.Variant)
	for _, vr := range vrs {
		v := sutVariantFromRow(vr)
		v.Versions, err = listSUTVersions(s, vr.ID)
		if err != nil {
			return nil, err
		}

		vs[v.Name] = v
	}

	return vs, nil
}

func listSUTVariantRows(s xorm.Interface, prID int64) ([]sutVariantRow, error) {
	var vrs []sutVariantRow
	err := s.Table(sutVariantTable).Find(&vrs, &sutVariantRow{ProjectID: prID})
	if err != nil {
		return nil, err
	}

	return vrs, nil
}

func lookupSUTVariantRowID(s xorm.Interface, prID int64, name string) (int64, error) {
	svr := sutVariantRow{ProjectID: prID, Name: name}
	ex, err := s.Table(sutVariantTable).Get(&svr)
	if err != nil {
		return 0, err
	}

	if !ex {
		return 0, fmt.Errorf(errorNoSUTVariant, name)
	}

	return svr.ID, nil
}
