// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	caseVersionField    = "test_case_version"
	projectVersionField = "project_version"
)

type caseVersionSUTVersionRow struct {
	ID              int64
	TestCaseVersion int64
	ProjectVersion  int64
}

func saveSUTVersionsForCaseVersion(s xorm.Interface, cvrID, svrID int64, nvs []project.Version) error {
	var names []string
	for _, nv := range nvs {
		names = append(names, nv.Name)
	}

	svIDs, err := lookupSUTVersionRowIDs(s, svrID, names...)
	if err != nil {
		return err
	}

	var cvsvrs []caseVersionSUTVersionRow
	for _, svID := range svIDs {
		cvsvrs = append(cvsvrs, caseVersionSUTVersionRow{TestCaseVersion: cvrID, ProjectVersion: svID})
	}

	return insertCaseVersionSUTVersionRows(s, cvsvrs...)
}

func insertCaseVersionSUTVersionRows(s xorm.Interface, cvsvrs ...caseVersionSUTVersionRow) error {
	_, err := s.Table(caseVersionSUTVersionTable).Insert(&cvsvrs)
	return err
}

func listSUTVersionRowsForCaseVersion(s xorm.Interface, vrID int64) ([]sutVersionRow, error) {
	var ids []int64
	err := s.Table(caseVersionSUTVersionTable).Distinct(projectVersionField).In(caseVersionField, vrID).Find(&ids)
	if err != nil {
		return nil, err
	}

	var svrs []sutVersionRow
	err = s.Table(sutVersionTable).In(idField, ids).Asc(idField).Find(&svrs)
	if err != nil {
		return nil, err
	}

	return svrs, nil
}
