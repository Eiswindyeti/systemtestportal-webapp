// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import "gitlab.com/stp-team/systemtestportal-webapp/domain/id"

type ownersRAM struct {
	*usersRAM
	*groupsRAM
}

func (s *ownersRAM) Exists(id id.ActorID) (bool, error) {
	if s == nil {
		panic("Trying existence check on nil store")
	}

	_, uOK, err := s.usersRAM.Get(id)
	if err != nil {
		return false, err
	}

	if uOK {
		return uOK, nil
	}

	return s.groupsRAM.Exists(id)
}
