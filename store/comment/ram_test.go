/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package comment

import (
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

var time1 = time.Now()
var time2 = time1.Add(time.Second * 2)

var testUser = user.User{
	DisplayName: "u-2",
	Name:        "u-2",
}
var pID = id.NewProjectID(
	id.ActorID(testUser.Name),
	"p-1")

var testID = id.NewTestID(
	pID,
	"o-1",
	true)

var otherTestID = id.NewTestID(
	pID,
	"o-2",
	false)

var commentStore = &ramStore{
	comments: map[string]map[string]map[string]map[bool][]*comment.Comment{
		testID.Owner(): {
			testID.Project(): {
				testID.Test(): {
					testID.IsCase(): {
						{
							ID:           testID,
							Text:         "A Comment",
							CreationDate: time1,
							Author:       testUser,
						},
						{
							ID:           testID,
							Text:         "Comment from same user",
							CreationDate: time2,
							Author:       testUser,
						},
						{
							ID:   otherTestID,
							Text: "A different Comment",
							Author: user.User{
								DisplayName: "u-s",
								Name:        "u-s",
							},
						},
					},
				},
				otherTestID.Test(): {
					otherTestID.IsCase(): {
						{
							ID:   otherTestID,
							Text: "Another Comment",
							Author: user.User{
								DisplayName: "u-1",
								Name:        "u-1",
							},
						},
					},
				},
			},
		},
	},
}

var _ Store = initRAMStore()

func TestRamStoreNilMethodCalls(t *testing.T) {
	var emptyStore *ramStore
	t.Run("Add", func(t *testing.T) {
		defer expectPanic(t)
		emptyStore.Add(&comment.Comment{ID: testID})
	})
	t.Run("Add", func(t *testing.T) {
		defer expectPanic(t)
		initRAMStore().Add(nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		emptyStore.Get(testID)
	})
}

func TestRamStoreAdd(t *testing.T) {
	type args struct {
		c *comment.Comment
	}
	tests := []struct {
		name string
		s    *ramStore
		args args
		want error
	}{
		{
			name: "New container",
			s:    initRAMStore(),
			args: args{
				c: &comment.Comment{
					ID: id.NewTestID(
						id.NewProjectID(
							id.ActorID("u-3"),
							testID.Project()),
						testID.Test(),
						testID.IsCase()),
				},
			},
			want: nil,
		},
		{
			name: "New project",
			s:    initRAMStore(),
			args: args{
				c: &comment.Comment{
					ID: id.NewTestID(
						id.NewProjectID(
							id.ActorID(testID.Owner()),
							"p-3"),
						testID.Test(),
						testID.IsCase()),
				},
			},
			want: nil,
		},
		{
			name: "New test",
			s:    initRAMStore(),
			args: args{
				c: &comment.Comment{
					ID: id.NewTestID(
						testID.ProjectID,
						"o-3",
						testID.IsCase()),
				},
			},
			want: nil,
		},
		{
			name: "New case or sequence ",
			s:    initRAMStore(),
			args: args{
				c: &comment.Comment{
					ID: id.NewTestID(
						testID.ProjectID,
						testID.Test(),
						!testID.IsCase()),
				},
			},
			want: nil,
		},
		{
			name: "Existing container, project, test and sequence",
			s:    initRAMStore(),
			args: args{
				c: &comment.Comment{
					ID: otherTestID,
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.s.Add(tt.args.c)
			if got != tt.want {
				t.Errorf("comment ramStore.Add()  = %v, want %v", got, tt.want)
			}
		})
	}

}

func TestRamStoreGet(t *testing.T) {
	type args struct {
		id id.TestID
	}
	type want struct {
		c  []*comment.Comment
		ok bool
	}
	tests := []struct {
		name string
		s    *ramStore
		args args
		want want
	}{
		{
			name: "Unknown container",
			s:    commentStore,
			args: args{
				id: id.NewTestID(
					id.NewProjectID(
						id.ActorID("u-3"),
						testID.Project()),
					testID.Test(),
					testID.IsCase()),
			},
			want: want{
				c:  nil,
				ok: false,
			},
		},
		{
			name: "Unknown project",
			s:    commentStore,
			args: args{
				id: id.NewTestID(
					id.NewProjectID(
						id.ActorID(testID.Owner()),
						"p-3"),
					testID.Test(),
					testID.IsCase()),
			},
			want: want{
				c:  nil,
				ok: false,
			},
		},
		{
			name: "Unknown test",
			s:    commentStore,
			args: args{
				id: id.NewTestID(
					testID.ProjectID,
					"o-3",
					testID.IsCase()),
			},
			want: want{
				c:  nil,
				ok: false,
			},
		},
		{
			name: "Unknown test sequence or case",
			s:    commentStore,
			args: args{
				id: id.NewTestID(
					testID.ProjectID,
					testID.Test(),
					false,
				),
			},
			want: want{
				c:  nil,
				ok: false,
			},
		},
		{
			name: "Known container, project, test and sequence or case",
			s:    commentStore,
			args: args{
				id: testID,
			},
			want: want{
				c:  commentStore.comments[testID.Owner()][testID.Project()][testID.Test()][testID.IsCase()],
				ok: true,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotC, gotOK := tt.s.Get(tt.args.id)
			if gotOK != tt.want.ok || !commentsEqual(gotC, tt.want.c) {
				t.Errorf("Get(%v) = (%v, %v), want (%v, %v)", tt.args.id, gotC, gotOK, tt.want.c, tt.want.ok)
			}
		})
	}
}

func commentsEqual(c1, c2 []*comment.Comment) bool {
	for i, c := range c1 {
		if c2[i] != c {
			return false
		}
	}
	return true
}

func expectPanic(t *testing.T) {
	if recover() == nil {
		t.Error("Expected panic did not occur")
	}
}
