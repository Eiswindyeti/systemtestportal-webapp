// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"encoding/json"
	"errors"
	"reflect"
	"testing"
	"time"

	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

func TestProjectsSQLStoreAdd(t *testing.T) {
	type args struct {
		s Projects
		p *project.Project
	}
	type result struct {
		e error
	}

	tcs := []struct {
		name         string
		a            args
		w            result
		shouldCreate bool
	}{
		{
			name: "Empty store",
			a: args{
				s: projectsSQL{emptyEngine(t)},
				p: &project.Project{
					Owner:        "theo",
					Name:         "New project",
					Description:  "A newly added project",
					Visibility:   visibility.Public,
					CreationDate: time.Now(),

					Variants: map[string]*project.Variant{
						"Standard": {
							Name: "Standard",
							Versions: []project.Version{
								{Name: "v0.1"},
							},
						},
					},

					Members: map[id.ActorID]*user.User{
						"theo": {
							Name:        "theo",
							DisplayName: "Theodor Heuss",
							EMail:       "theo.heu@example.org",

							RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
						},
					},
				},
			},
			w: result{
				e: multierror.Append(errors.New(`no owner with name "theo"`)),
			},
			shouldCreate: false,
		},
		{
			name: "Owner not in store",
			a: args{
				s: projectsSQL{engineWithSampleData(t)},
				p: &project.Project{
					Owner:        "peter",
					Name:         "New project",
					Description:  "A newly added project",
					Visibility:   visibility.Public,
					CreationDate: time.Now(),

					Variants: map[string]*project.Variant{
						"Standard": {
							Name: "Standard",
							Versions: []project.Version{
								{Name: "v0.1"},
							},
						},
					},

					Members: map[id.ActorID]*user.User{
						"theo": {
							Name:        "theo",
							DisplayName: "Theodor Heuss",
							EMail:       "theo.heu@example.org",

							RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
						},
					},
				},
			},
			w: result{
				e: multierror.Append(errors.New(`no owner with name "peter"`)),
			},
			shouldCreate: false,
		},
		{
			name: "Insert new project",
			a: args{
				s: projectsSQL{engineWithSampleData(t)},
				p: &project.Project{
					Owner:        "theo",
					Name:         "New project",
					Description:  "A newly added project",
					Visibility:   visibility.Public,
					CreationDate: time.Now().Round(time.Second),

					Variants: map[string]*project.Variant{
						"Standard": {
							Name: "Standard",
							Versions: []project.Version{
								{Name: "v0.1"},
							},
						},
					},

					Labels: []project.Label{
						{
							Name:        "New label",
							Description: "A new label",
						},
					},

					Members: map[id.ActorID]*user.User{
						"theo": {
							Name:        "theo",
							DisplayName: "Theodor Heuss",
							EMail:       "theo.heu@example.org",

							RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
						},
					},
				},
			},
			w: result{
				e: nil,
			},
			shouldCreate: true,
		},
		{
			name: "Update existing project",
			a: args{
				s: projectsSQL{engineWithSampleData(t)},
				p: &project.Project{
					Owner:        "Another group",
					Name:         "Test project",
					Description:  "An update of our test project",
					Visibility:   visibility.Private,
					CreationDate: time.Date(2017, time.August, 15, 9, 55, 48, 0, time.UTC).In(time.Local),

					Variants: map[string]*project.Variant{
						"Windows": {
							Name: "Windows",
							Versions: []project.Version{
								{Name: "Standard"},
							},
						},

						"Linux": {
							Name: "Linux",
							Versions: []project.Version{
								{Name: "Standard"},
							},
						},
						"Linux x64": {
							Name: "Linux x64",
							Versions: []project.Version{
								{Name: "Standard"},
							},
						},
						"Linux ARM": {
							Name: "Linux ARM",
							Versions: []project.Version{
								{Name: "Standard"},
							},
						},
					},

					Labels: []project.Label{
						{
							Name:        "Example label",
							Description: "First label used for demonstration purposes",
						},
						{
							Name:        "New label",
							Description: "A newly created label",
						},
					},

					Members: map[id.ActorID]*user.User{
						"Franz": {
							Name:        "Franz",
							DisplayName: "Frain",
							EMail:       "f.thor@example.org",

							RegistrationDate: time.Date(2017, time.July, 12, 9, 6, 28, 0, time.UTC).In(time.Local),
						},
					},
				},
			},
			w: result{
				e: nil,
			},
			shouldCreate: true,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.e = tc.a.s.Add(tc.a.p)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sqlStore.Add(%v) = %v, want %v", tc.a.p, got.e, tc.w.e)
			}

			if !tc.shouldCreate {
				return
			}

			ap, ex, _ := tc.a.s.Get(tc.a.p.ID())
			if !ex {
				t.Error("Project was not created")
			}

			if !reflect.DeepEqual(tc.a.p, ap) {
				t.Error("Not all information was saved")
			}
		})
	}
}

func TestProjectsSQLStoreGet(t *testing.T) {
	type args struct {
		s   Projects
		pID id.ProjectID
	}
	type result struct {
		p   *project.Project
		ex  bool
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:   projectsSQL{emptyEngine(t)},
				pID: id.NewProjectID("Another group", "Test project"),
			},
			w: result{
				p:   nil,
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "Another group"`)),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s:   projectsSQL{engineWithSampleData(t)},
				pID: id.NewProjectID("peter", "Test project"),
			},
			w: result{
				p:   nil,
				ex:  false,
				err: multierror.Append(errors.New(`no owner with name "peter"`)),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s:   projectsSQL{engineWithSampleData(t)},
				pID: id.NewProjectID("Another group", "project 2"),
			},
			w: result{
				p:   nil,
				ex:  false,
				err: nil,
			},
		},
		{
			name: "Project in store",
			a: args{
				s:   projectsSQL{engineWithSampleData(t)},
				pID: id.NewProjectID("Another group", "Test project"),
			},
			w: result{
				p: &project.Project{
					Owner:        "Another group",
					Name:         "Test project",
					Description:  "A project used for testing purposes",
					Visibility:   visibility.Public,
					CreationDate: time.Date(2017, time.August, 15, 9, 55, 48, 0, time.UTC).In(time.Local),

					Variants: map[string]*project.Variant{
						"Windows": {
							Name: "Windows",
							Versions: []project.Version{
								{Name: "Standard"},
								{Name: "Premium"},
							},
						},
						"Linux": {
							Name: "Linux",
							Versions: []project.Version{
								{Name: "Standard"},
								{Name: "Premium"},
							},
						},
						"Mac OS": {
							Name: "Mac OS",
						},
					},

					Labels: []project.Label{
						{
							Name:        "Example label",
							Description: "First label for demonstration purposes",
						},
						{
							Name:        "Unused label",
							Description: "This label is not assigned anywhere",
						},
					},

					Members: map[id.ActorID]*user.User{
						"theo": {
							Name:        "theo",
							DisplayName: "Theodor Heuss",
							EMail:       "theo.heu@example.org",

							RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
						},
					},
				},
				ex:  true,
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.p, got.ex, got.err = tc.a.s.Get(tc.a.pID)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("sqlStore.Get(%v) = (%v, %v, %v), want (%v, %v, %v)", tc.a.pID, got.p, got.ex, got.err, tc.w.p,
					tc.w.ex, tc.w.err)
			}
		})
	}
}

func TestProjectsSQLStoreList(t *testing.T) {
	type args struct {
		s Projects
		o string
	}

	type result struct {
		ps []*project.Project
		e  error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s: projectsSQL{emptyEngine(t)},
				o: "theo",
			},
			w: result{
				ps: nil,
				e:  multierror.Append(errors.New(`no owner with name "theo"`)),
			},
		},
		{
			name: "Owner not in store",
			a: args{
				s: projectsSQL{engineWithSampleData(t)},
				o: "peter",
			},
			w: result{
				ps: nil,
				e:  multierror.Append(errors.New(`no owner with name "peter"`)),
			},
		},
		{
			name: "Owner in store",
			a: args{
				s: projectsSQL{engineWithSampleData(t)},
				o: "theo",
			},
			w: result{
				ps: []*project.Project{
					{
						Owner:        "theo",
						Name:         "AW-987",
						Description:  "Automation framework for windows operating systems",
						Visibility:   visibility.Private,
						CreationDate: time.Date(2017, time.October, 30, 23, 10, 54, 0, time.UTC).In(time.Local),

						Variants: map[string]*project.Variant{
							"Windows XP": {
								Name: "Windows XP",
								Versions: []project.Version{
									{Name: "v1.0.0-x86"},
									{Name: "v1.0.0-x64"},
								},
							},
							"Windows 7": {
								Name: "Windows 7",
								Versions: []project.Version{
									{Name: "v1.0.0-x86"},
									{Name: "v1.0.0-x64"},
								},
							},
							"Windows 8": {
								Name: "Windows 8",
								Versions: []project.Version{
									{Name: "v1.0.0-x86"},
									{Name: "v1.0.0-x64"},
								},
							},
							"Windows 10": {
								Name: "Windows 10",
								Versions: []project.Version{
									{Name: "v1.0.0-x86"},
									{Name: "v1.0.0-x64"},
								},
							},
						},

						Members: map[id.ActorID]*user.User{
							"theo": {
								Name:        "theo",
								DisplayName: "Theodor Heuss",
								EMail:       "theo.heu@example.org",

								RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
							},
						},
					},
					{
						Owner:        "theo",
						Name:         "GoUnit",
						Description:  "Unit testing go code",
						Visibility:   visibility.Public,
						CreationDate: time.Date(2017, time.September, 5, 3, 26, 17, 0, time.UTC).In(time.Local),

						Variants: map[string]*project.Variant{
							"Windows 32bit": {
								Name: "Windows 32bit",
								Versions: []project.Version{
									{Name: "v0.1.0-win-x86"},
									{Name: "v0.1.1-win-x86"},
									{Name: "v0.1.2-win-x86"},
									{Name: "v0.2.0-win-x86"},
									{Name: "v0.2.1-win-x86"},
									{Name: "v0.3.0-win-x86"},
									{Name: "v1.0.0-win-x86"},
									{Name: "v1.0.1-win-x86"},
									{Name: "v1.1.0-win-x86"},
								},
							},
							"Windows 64bit": {
								Name: "Windows 64bit",
								Versions: []project.Version{
									{Name: "v0.1.0-win-amd64"},
									{Name: "v0.1.1-win-amd64"},
									{Name: "v0.1.2-win-amd64"},
									{Name: "v0.2.0-win-amd64"},
									{Name: "v0.2.1-win-amd64"},
									{Name: "v0.3.0-win-amd64"},
									{Name: "v1.0.0-win-amd64"},
									{Name: "v1.0.1-win-amd64"},
									{Name: "v1.1.0-win-amd64"},
								},
							},
							"Linux 32bit": {
								Name: "Linux 32bit",
								Versions: []project.Version{
									{Name: "v0.1.0-linux-x86"},
									{Name: "v0.1.1-linux-x86"},
									{Name: "v0.1.2-linux-x86"},
									{Name: "v0.2.0-linux-x86"},
									{Name: "v0.2.1-linux-x86"},
									{Name: "v0.3.0-linux-x86"},
									{Name: "v1.0.0-linux-x86"},
									{Name: "v1.0.1-linux-x86"},
									{Name: "v1.1.0-linux-x86"},
								},
							},
							"Linux 64bit": {
								Name: "Linux 64bit",
								Versions: []project.Version{
									{Name: "v0.1.0-linux-amd64"},
									{Name: "v0.1.1-linux-amd64"},
									{Name: "v0.1.2-linux-amd64"},
									{Name: "v0.2.0-linux-amd64"},
									{Name: "v0.2.1-linux-amd64"},
									{Name: "v0.3.0-linux-amd64"},
									{Name: "v1.0.0-linux-amd64"},
									{Name: "v1.0.1-linux-amd64"},
									{Name: "v1.1.0-linux-amd64"},
								},
							},
						},

						Labels: []project.Label{
							{
								Name: "Deprecated",
								Description: "This test is no longer applicable to new versions and only kept around" +
									" for historic reasons",
							},
							{
								Name:        "Improvement needed",
								Description: "This test has a low quality and should be improved",
							},
							{
								Name: "Long test",
								Description: "This test, including setup and tear down of the test environment, takes" +
									" longer than 120 minutes",
							},
							{
								Name: "Short test",
								Description: "This test, including setup and tear down of the test environment, takes" +
									" no longer than 10 minutes",
							},
						},

						Members: map[id.ActorID]*user.User{
							"theo": {
								Name:        "theo",
								DisplayName: "Theodor Heuss",
								EMail:       "theo.heu@example.org",

								RegistrationDate: time.Date(2017, time.July, 23, 18, 59, 23, 0, time.UTC).In(time.Local),
							},
						},
					},
				},
				e: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.ps, got.e = tc.a.s.List(tc.a.o)

			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("projectsSQL.List(%v) = (%v, %v), want (%v, %v)", tc.a.o, got.ps, got.e, tc.w.ps, tc.w.e)

				gotJSON, err := json.Marshal(got.ps)
				handleError(t, err)
				wantJSON, err := json.Marshal(tc.w.ps)
				handleError(t, err)
				t.Log(string(gotJSON))
				t.Log(string(wantJSON))
			}
		})
	}
}

func TestProjectsSQLDelete(t *testing.T) {
	type args struct {
		s  Projects
		id id.ProjectID
	}

	type result error

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Empty store",
			a: args{
				s:  projectsSQL{emptyEngine(t)},
				id: id.NewProjectID("theo", "GoUnit"),
			},
			w: multierror.Append(errors.New(`no owner with name "theo"`)),
		},
		{
			name: "Owner not in store",
			a: args{
				s:  projectsSQL{engineWithSampleData(t)},
				id: id.NewProjectID("Hans", "GoUnit"),
			},
			w: multierror.Append(errors.New(`no owner with name "Hans"`)),
		},
		{
			name: "Project not in store",
			a: args{
				s:  projectsSQL{engineWithSampleData(t)},
				id: id.NewProjectID("theo", "JUnit"),
			},
			w: multierror.Append(errors.New(`no project with name "JUnit"`)),
		},
		{
			name: "Project in store",
			a: args{
				s:  projectsSQL{engineWithSampleData(t)},
				id: id.NewProjectID("theo", "GoUnit"),
			},
			w: nil,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			ex, _ := tc.a.s.Exists(tc.a.id)

			var got result = tc.a.s.Delete(tc.a.id)
			if !reflect.DeepEqual(got, tc.w) {
				t.Errorf("projectsSQL.Delete(%v) = %v, want %v", tc.a.id, got, tc.w)
				return
			}

			if !ex {
				return
			}

			if ex, _ := tc.a.s.Exists(tc.a.id); ex {
				t.Error("Project does still exist")
			}
		})
	}
}
