// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import (
	"errors"
	"fmt"
	"sync"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type casesRAM struct {
	sync.RWMutex
	cases map[string]map[string]map[string]*test.Case
}

func initCasesRAM() *casesRAM {
	return &casesRAM{
		cases: make(map[string]map[string]map[string]*test.Case),
	}
}

func (s *casesRAM) List(projectID id.ProjectID) ([]*test.Case, error) {
	if s == nil {
		panic("trying to list test cases in a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.cases[projectID.Owner()]
	if !ok {
		return nil, nil
	}

	var l []*test.Case
	for _, tc := range pm[projectID.Project()] {
		l = append(l, tc)
	}
	return l, nil
}

func (s *casesRAM) Add(tc *test.Case) error {
	if s == nil {
		panic("trying to add a test case to a nil store")
	}

	if tc == nil {
		panic("can not add a nil test case to the store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.cases[tc.Project.Owner()]
	if !ok {
		pm = make(map[string]map[string]*test.Case)
		s.cases[tc.Project.Owner()] = pm
	}

	tcm, ok := pm[tc.Project.Project()]
	if !ok {
		tcm = make(map[string]*test.Case)
		pm[tc.Project.Project()] = tcm
	}

	tcm[tc.Name] = tc
	return nil
}

func (s *casesRAM) Get(tcID id.TestID) (*test.Case, bool, error) {
	if s == nil {
		panic("trying to get a test case from a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.cases[tcID.Owner()]
	if !ok {
		return nil, false, nil
	}

	tcm, ok := pm[tcID.Project()]
	if !ok {
		return nil, false, nil
	}

	tc, ok := tcm[tcID.Test()]
	return tc, ok, nil
}

func (s *casesRAM) Exists(tcID id.TestID) (bool, error) {
	if s == nil {
		panic("trying to get a test case from a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.cases[tcID.Owner()]
	if !ok {
		return false, nil
	}

	tcm, ok := pm[tcID.Project()]
	if !ok {
		return false, nil
	}

	_, ok = tcm[tcID.Test()]
	return ok, nil
}

func (s *casesRAM) Rename(old, new id.TestID) error {
	if s == nil {
		panic("trying to rename a test case in a nil store")
	}

	if old.ProjectID != new.ProjectID {
		return errors.New(`unsupported operation: test cases currently can not be moved between projects`)
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.cases[old.Owner()]
	if !ok {
		return fmt.Errorf("no owner with name %#v", old.Owner())
	}

	cm, ok := pm[old.Project()]
	if !ok {
		return fmt.Errorf("no project with name %#v", old.Project())
	}

	c, ok := cm[old.Test()]
	if !ok {
		return fmt.Errorf("no test case with name %#v", old.Test())
	}

	c.Rename(new.Test())
	for _, cv := range c.TestCaseVersions {
		cv.Case = new
	}

	cm[new.Test()] = c
	delete(cm, old.Test())

	return nil
}

func (s *casesRAM) Delete(tcID id.TestID) error {
	if s == nil {
		panic("trying to delete a test case from a nil store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.cases[tcID.Owner()]
	if !ok {
		return nil
	}

	tcm, ok := pm[tcID.Project()]
	if !ok {
		return nil
	}

	delete(tcm, tcID.Test())
	return nil
}
