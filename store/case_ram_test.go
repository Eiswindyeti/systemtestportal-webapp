// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

var testCaseStore = &casesRAM{
	cases: map[string]map[string]map[string]*test.Case{
		"user-1": {
			"p0001": {
				"test-case-1": {
					Name: "Test Case 1",
				},
				"test-case-2": {
					Name: "Test Case 2",
				},
			},
			"awesome-project": {
				"login": {
					Name: "Login",
				},
				"logout": {
					Name: "Login",
				},
			},
			"another-project": {
				"Execute test": {
					Name: "Anöther Project",
				},
			},
		},
	},
}

func casesRAMWithExampleData() *casesRAM {
	return &casesRAM{
		cases: map[string]map[string]map[string]*test.Case{
			"user-1": {
				"p0001": {
					"test-case-1": {
						Name: "Test Case 1",
					},
					"test-case-2": {
						Name: "Test Case 2",
					},
				},
				"awesome-project": {
					"login": {
						Name: "Login",
					},
					"logout": {
						Name: "Login",
					},
				},
				"another-project": {
					"Execute test": {
						Name: "Anöther Project",
					},
				},
			},
		},
	}
}

var _ Cases = initCasesRAM()

func TestCasesRAMNilMethodCalls(t *testing.T) {
	var empty *casesRAM
	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List(id.NewProjectID("user-1", "awesome-project"))
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add(&test.Case{Name: "login-2", Project: id.NewProjectID("user-1", "awesome-project")})
	})
	t.Run("Add (nil test case)", func(t *testing.T) {
		defer expectPanic(t)
		initCasesRAM().Add(nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get(id.NewTestID(id.NewProjectID("user-1", "awesome-project"), "login", true))
	})
	t.Run("Delete", func(t *testing.T) {
		defer expectPanic(t)
		empty.Delete(id.NewTestID(id.NewProjectID("user-1", "awesome-project"), "login", true))
	})
	t.Run("Rename", func(t *testing.T) {
		defer expectPanic(t)
		empty.Rename(
			id.NewTestID(id.NewProjectID("user-1", "awesome-project"), "login", true),
			id.NewTestID(id.NewProjectID("user-1", "awesome-project"), "Login", true),
		)
	})
}

func TestCasesRAMList(t *testing.T) {
	type args struct {
		pID id.ProjectID
	}
	tests := []struct {
		name string
		s    *casesRAM
		args args
		want []*test.Case
	}{
		{
			name: "Known container and project",
			s:    testCaseStore,
			args: args{
				pID: id.NewProjectID("user-1", "awesome-project"),
			},
			want: []*test.Case{
				testCaseStore.cases["user-1"]["awesome-project"]["login"],
				testCaseStore.cases["user-1"]["awesome-project"]["logout"],
			},
		},
		{
			name: "Unknown container",
			s:    testCaseStore,
			args: args{
				pID: id.NewProjectID("user-2", "p0001"),
			},
			want: nil,
		},
		{
			name: "Unknown project",
			s:    testCaseStore,
			args: args{
				pID: id.NewProjectID("user-1", "unknown"),
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := tt.s.List(tt.args.pID); !containsCasesUnordered(got, tt.want) {
				t.Errorf("caseRAMStore.List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCasesRAMAdd(t *testing.T) {
	type args struct {
		tc *test.Case
	}
	tests := []struct {
		name string
		s    *casesRAM
		args args
		want error
	}{
		{
			name: "New container",
			s:    initCasesRAM(),
			args: args{
				tc: &test.Case{
					Name:    "Cool Test Case",
					Project: id.NewProjectID("user-1", "cool-project"),
				},
			},
			want: nil,
		},
		{
			name: "New project",
			s: &casesRAM{
				cases: map[string]map[string]map[string]*test.Case{
					"user-1": {},
				},
			},
			args: args{
				tc: &test.Case{
					Name:    "Cool Test Case",
					Project: id.NewProjectID("user-1", "cool-project"),
				},
			},
			want: nil,
		},
		{
			name: "Existing container and project",
			s: &casesRAM{
				cases: map[string]map[string]map[string]*test.Case{
					"user-1": {
						"cool-project": {},
					},
				},
			},
			args: args{
				tc: &test.Case{
					Name:    "Cool Test Case",
					Project: id.NewProjectID("user-1", "cool-project"),
				},
			},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Add(tt.args.tc); got != tt.want {
				t.Errorf("caseRAMStore.Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCasesRAMGet(t *testing.T) {
	type args struct {
		tcID id.TestID
	}
	type want struct {
		tc *test.Case
		ok bool
	}
	tests := []struct {
		name string
		s    *casesRAM
		args args
		want want
	}{
		{
			name: "Unknown container",
			s:    testCaseStore,
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-2", "p0001"), "test-case-2", true),
			},
			want: want{
				tc: nil,
				ok: false,
			},
		},
		{
			name: "Unknown project",
			s:    testCaseStore,
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-1", "p0002"), "test-case-2", true),
			},
			want: want{
				tc: nil,
				ok: false,
			},
		},
		{
			name: "Unknown test case",
			s:    testCaseStore,
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-3", true),
			},
			want: want{
				tc: nil,
				ok: false,
			},
		},
		{
			name: "Known test case",
			s:    testCaseStore,
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-2", true),
			},
			want: want{
				tc: testCaseStore.cases["user-1"]["p0001"]["test-case-2"],
				ok: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTC, gotOK, gotError := tt.s.Get(tt.args.tcID)
			if gotTC != tt.want.tc || gotOK != tt.want.ok || gotError != nil {
				t.Errorf("Get(%+v) = (%v, %v, %v), want (%v, %v, nil)",
					tt.args.tcID, gotTC, gotOK, gotError, tt.want.tc, tt.want.ok)
			}

		})
	}
}

func TestCasesRAMDelete(t *testing.T) {
	type args struct {
		tcID id.TestID
	}
	tests := []struct {
		name string
		s    *casesRAM
		args args
	}{
		{
			name: "Empty store",
			s:    initCasesRAM(),
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-1", true),
			},
		},
		{
			name: "Unknown user",
			s:    casesRAMWithExampleData(),
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-53", "p0001"), "test-case-1", true),
			},
		},
		{
			name: "Unknown project",
			s:    casesRAMWithExampleData(),
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-1", "p0002"), "test-case-1", true),
			},
		},
		{
			name: "Unknown test case",
			s:    casesRAMWithExampleData(),
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-2", true),
			},
		},
		{
			name: "Known test case",
			s:    casesRAMWithExampleData(),
			args: args{
				tcID: id.NewTestID(id.NewProjectID("user-1", "p0001"), "test-case-1", true),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.s.Delete(tt.args.tcID)

			if _, found, _ := tt.s.Get(tt.args.tcID); found {
				t.Errorf("Test case was not deleted")
			}
		})
	}
}

func TestCasesRAMAddAndGet(t *testing.T) {
	type args struct {
		tc *test.Case
	}
	tests := []struct {
		name string
		s    *casesRAM
		args args
	}{
		{
			name: "Basic example",
			s:    initCasesRAM(),
			args: args{
				tc: &test.Case{
					Name:    "New Test Case",
					Project: id.NewProjectID("user-1", "p0001"),
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotError := tt.s.Add(tt.args.tc)

			if gotError != nil {
				t.Errorf("Add(%+v) = %v, want nil", tt.args.tc, gotError)
			}

			gotTC, gotOK, gotError := tt.s.Get(tt.args.tc.ID())

			if !gotOK || gotTC != tt.args.tc || gotError != nil {
				t.Errorf("Get(%+v) = (%v, %v, %v), want (%v, %v, nil)",
					tt.args.tc.ID(), gotTC, gotOK, gotError, tt.args.tc, true)
			}
		})
	}
}

func TestCasesRAMRename(t *testing.T) {
	type args struct {
		s Cases
		o id.TestID
		n id.TestID
	}

	type result struct {
		err error
	}

	tcs := []struct {
		name string
		a    args
		w    result
	}{
		{
			name: "Owner not in store",
			a: args{
				s: casesRAMWithExampleData(),
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-2"), "p0001"),
					"test-case-1",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-2"), "p0001"),
					"test-case-11",
					true,
				),
			},
			w: result{
				err: errors.New(`no owner with name "user-2"`),
			},
		},
		{
			name: "Project not in store",
			a: args{
				s: casesRAMWithExampleData(),
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0002"),
					"test-case-1",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0002"),
					"test-case-11",
					true,
				),
			},
			w: result{
				err: errors.New(`no project with name "p0002"`),
			},
		},
		{
			name: "Test case not in store",
			a: args{
				s: casesRAMWithExampleData(),
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0001"),
					"test-case-11",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0001"),
					"test-case-12",
					true,
				),
			},
			w: result{
				err: errors.New(`no test case with name "test-case-11"`),
			},
		},
		{
			name: "Move between projects",
			a: args{
				s: casesRAMWithExampleData(),
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0001"),
					"test-case-1",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0002"),
					"test-case-1",
					true,
				),
			},
			w: result{
				err: errors.New(`unsupported operation: test cases currently can not be moved between projects`),
			},
		},
		{
			name: "Successful rename",
			a: args{
				s: casesRAMWithExampleData(),
				o: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0001"),
					"test-case-1",
					true,
				),
				n: id.NewTestID(
					id.NewProjectID(id.NewActorID("user-1"), "p0001"),
					"test-case-11",
					true,
				),
			},
			w: result{
				err: nil,
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			var got result
			got.err = tc.a.s.Rename(tc.a.o, tc.a.n)

			if !reflect.DeepEqual(got, tc.w) {
				t.Log(got.err)
				t.Log(tc.w.err)
				t.Errorf("casesSQL.Rename(%v, %v) = %v, want %v", tc.a.o, tc.a.n, got, tc.w)
				return
			}

			if tc.w.err != nil {
				return
			}

			_, ex, _ := tc.a.s.Get(tc.a.o)
			if ex {
				t.Error("Renamed test case can still be retrieved with old id")
				return
			}

			_, ex, _ = tc.a.s.Get(tc.a.n)
			if !ex {
				t.Error("Renamed test case can not be retrieved with new id")
				return
			}
		})
	}
}

func containsCasesUnordered(got, want []*test.Case) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
