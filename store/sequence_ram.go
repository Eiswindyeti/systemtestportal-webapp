// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import (
	"sync"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type sequencesRAM struct {
	sync.RWMutex
	sequences map[string]map[string]map[string]*test.Sequence
}

func initSequencesRAM() *sequencesRAM {
	return &sequencesRAM{
		sequences: make(map[string]map[string]map[string]*test.Sequence),
	}
}

func (s *sequencesRAM) List(pID id.ProjectID) ([]*test.Sequence, error) {
	if s == nil {
		panic("trying to list test sequences in a nil store")
	}

	s.RLock()
	defer s.RUnlock()
	pm, ok := s.sequences[pID.Owner()]
	if !ok {
		return nil, nil
	}

	var l []*test.Sequence
	for _, ts := range pm[pID.Project()] {
		l = append(l, ts)
	}
	return l, nil
}

func (s *sequencesRAM) Add(ts *test.Sequence) error {
	if s == nil {
		panic("trying to add a test sequence to a nil store")
	}

	if ts == nil {
		panic("trying to add a nil test sequence to the store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.sequences[ts.Project.Owner()]
	if !ok {
		pm = make(map[string]map[string]*test.Sequence)
		s.sequences[ts.Project.Owner()] = pm
	}

	tsm, ok := pm[ts.Project.Project()]
	if !ok {
		tsm = make(map[string]*test.Sequence)
		pm[ts.Project.Project()] = tsm
	}

	tsm[ts.Name] = ts
	return nil
}

func (s *sequencesRAM) Get(tsID id.TestID) (*test.Sequence, bool, error) {
	if s == nil {
		panic("trying to get a test sequence from a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.sequences[tsID.Owner()]
	if !ok {
		return nil, false, nil
	}

	tsm, ok := pm[tsID.Project()]
	if !ok {
		return nil, false, nil
	}

	ts, ok := tsm[tsID.Test()]
	return ts, ok, nil
}

func (s *sequencesRAM) Exists(tsID id.TestID) (bool, error) {
	if s == nil {
		panic("trying to get a test sequence from a nil store")
	}

	s.RLock()
	defer s.RUnlock()

	pm, ok := s.sequences[tsID.Owner()]
	if !ok {
		return false, nil
	}

	tsm, ok := pm[tsID.Project()]
	if !ok {
		return false, nil
	}

	_, ok = tsm[tsID.Test()]
	return ok, nil
}

func (s *sequencesRAM) Rename(old, new id.TestID) error {
	if s == nil {
		panic("trying to rename a test case in a nil store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.sequences[old.Owner()]
	if !ok {
		return nil
	}

	sm, ok := pm[old.Project()]
	if !ok {
		return nil
	}

	seq, ok := sm[old.Test()]
	if !ok {
		return nil
	}

	seq.Rename(new.Test())
	for _, sv := range seq.SequenceVersions {
		sv.Testsequence = new
	}

	sm[new.Test()] = seq
	delete(sm, old.Test())

	return nil
}

func (s *sequencesRAM) Delete(tsID id.TestID) error {
	if s == nil {
		panic("trying to delete a test sequence from a nil store")
	}

	s.Lock()
	defer s.Unlock()

	pm, ok := s.sequences[tsID.Owner()]
	if !ok {
		return nil
	}

	tsm, ok := pm[tsID.Project()]
	if !ok {
		return nil
	}

	delete(tsm, tsID.Test())
	return nil
}
