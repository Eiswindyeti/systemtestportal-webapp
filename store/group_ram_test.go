// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

var _ Groups = initGroupsRAM()

func TestGroupsRAMNilMethodCalls(t *testing.T) {
	var empty *groupsRAM
	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List()
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add(&group.Group{Name: "new-group"})
	})
	t.Run("Add (nil group)", func(t *testing.T) {
		defer expectPanic(t)
		initGroupsRAM().Add(nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get("new-group")
	})
}

func TestGroupsRAMList(t *testing.T) {
	store := &groupsRAM{
		groups: map[string]*group.Group{
			"test": {
				Name:       "Test",
				Visibility: visibility.Private,
			},
			"totally-real-group": {
				Name:       "Totally real group",
				Visibility: visibility.Public,
			},
		},
	}
	tests := []struct {
		name string
		s    *groupsRAM
		want []*group.Group
	}{
		{
			name: "Empty store",
			s:    initGroupsRAM(),
			want: []*group.Group{},
		},
		{
			name: "Example store",
			s:    store,
			want: []*group.Group{
				store.groups["test"], store.groups["totally-real-group"],
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := tt.s.List(); !containsGroupsUnordered(got, tt.want) {
				t.Errorf("ramStore.List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGroupsRAMAdd(t *testing.T) {
	type args struct {
		g *group.Group
	}

	testGroup := group.Group{Name: "Test", Visibility: visibility.Private}

	tests := []struct {
		name string
		s    *groupsRAM
		args args
		want error
	}{
		{
			name: "Correct case",
			s:    initGroupsRAM(),
			args: args{g: &testGroup},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Add(tt.args.g); got != tt.want {
				t.Errorf("ramStore.Add(%v) = %v, want %v", tt.args.g, got, tt.want)
			}
		})
	}
}

func TestGroupsRAMGet(t *testing.T) {
	store := &groupsRAM{
		groups: map[string]*group.Group{
			"test": {
				Name:       "Test",
				Visibility: visibility.Private,
			},
			"totally-real-group": {
				Name:       "Totally real group",
				Visibility: visibility.Public,
			},
		},
	}
	type args struct {
		id id.ActorID
	}
	type want struct {
		g  *group.Group
		ok bool
	}
	tests := []struct {
		name string
		s    *groupsRAM
		args args
		want want
	}{
		{
			name: "Group not found",
			s:    initGroupsRAM(),
			args: args{id: "test"},
			want: want{},
		},
		{
			name: "Group found",
			s:    store,
			args: args{id: "test"},
			want: want{g: store.groups["test"], ok: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotG, gotOK, gotError := tt.s.Get(tt.args.id)

			if gotG != tt.want.g || gotOK != tt.want.ok || gotError != nil {
				t.Errorf("ramStore.Get(%v) = (%v, %v, %v), want (%v, %v, nil)", tt.args.id, gotG, gotOK,
					gotError, tt.want.g, tt.want.ok)
			}
		})
	}
}

func TestGroupsRAMAddAndGet(t *testing.T) {
	type args struct {
		store *groupsRAM
		g     group.Group
	}

	tcs := []struct {
		name string
		args args
	}{
		{
			name: "Basic example",
			args: args{
				store: initGroupsRAM(),
				g:     group.NewGroup("Test Group", "This is a test group", visibility.Private, nil),
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			tc.args.store.Add(&tc.args.g)
			gotG, gotOK, _ := tc.args.store.Get(tc.args.g.ID())

			if !gotOK {
				t.Errorf("Get after Add failed because the project was not found")
			}

			if gotOK && gotG != &tc.args.g {
				t.Errorf("Get after Add failed, because the project addresses were different: got %v, want %v", gotG, &tc.args.g)
			}
		})
	}
}

func containsGroupsUnordered(got, want []*group.Group) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
