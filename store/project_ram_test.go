// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build !sqlite

package store

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

var u1Projects = map[string]*project.Project{
	"p0001":           {Name: "P0001"},
	"awesome-project": {Name: "Awesome Project"},
	"another-project": {Name: "Anöther Project"},
}

var pS = &projectsRAM{
	projects: map[string]map[string]*project.Project{
		"user-1": u1Projects,
	},
}

var _ Projects = initProjectsRAM()

func TestProjectsRAMNilMethodCalls(t *testing.T) {
	var empty *projectsRAM

	t.Run("List", func(t *testing.T) {
		defer expectPanic(t)
		empty.List("user-1")
	})
	t.Run("Add (nil store)", func(t *testing.T) {
		defer expectPanic(t)
		empty.Add(&project.Project{Name: "awesome-project", Owner: "user-1"})
	})
	t.Run("Add (nil project)", func(t *testing.T) {
		defer expectPanic(t)
		initProjectsRAM().Add(nil)
	})
	t.Run("Get", func(t *testing.T) {
		defer expectPanic(t)
		empty.Get(id.NewProjectID("user-1", "awesome-project"))
	})
	t.Run("Delete", func(t *testing.T) {
		defer expectPanic(t)
		empty.Delete(id.NewProjectID("user-1", "awesome-project"))
	})
}

func TestProjectsRAMList(t *testing.T) {
	type args struct {
		store *projectsRAM
		cID   string
	}
	tests := []struct {
		name string
		args args
		want []*project.Project
	}{
		{
			name: "Unknown container",
			args: args{
				store: pS,
				cID:   "user-2",
			},
			want: nil,
		},
		{
			name: "Known container",
			args: args{store: pS, cID: "user-1"},
			want: []*project.Project{
				u1Projects["p0001"],
				u1Projects["awesome-project"],
				u1Projects["another-project"],
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {

			gotList, gotError := tc.args.store.List(tc.args.cID)

			if gotError != nil {
				t.Errorf("Unexpected error in List(%#v): %s", tc.args.cID, gotError)
			}

			if !containsProjectsUnordered(gotList, tc.want) {
				t.Errorf("List(%#v) = %v, want %v", tc.args.cID, gotList, tc.want)
			}
		})
	}
}

func TestProjectsRAMListAll(t *testing.T) {
	type args struct {
		store *projectsRAM
	}
	tests := []struct {
		name string
		args args
		want []*project.Project
	}{
		{
			name: "Filled store",
			args: args{store: pS},
			want: []*project.Project{
				u1Projects["p0001"],
				u1Projects["awesome-project"],
				u1Projects["another-project"],
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			gotList, gotError := tc.args.store.ListAll()

			if gotError != nil {
				t.Errorf("Unexpected error in ListAll(): %s", gotError)
			}

			if !containsProjectsUnordered(gotList, tc.want) {
				t.Errorf("ListAll() = %v, want %v", gotList, tc.want)
			}
		})
	}
}

func TestProjectsRAMAdd(t *testing.T) {
	type args struct {
		store   *projectsRAM
		project *project.Project
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "New container",
			args: args{
				store:   initProjectsRAM(),
				project: &project.Project{Name: "Cool Project", Owner: "user-1"},
			},
			want: nil,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := tc.args.store.Add(tc.args.project)

			if got != tc.want {
				t.Errorf("Add(%v) = %v, want %v", tc.args.project, got, tc.want)
			}
		})
	}
}

func TestProjectsRAMGet(t *testing.T) {
	type args struct {
		store *projectsRAM
		pID   id.ProjectID
	}
	type want struct {
		p  *project.Project
		ok bool
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "Unknown container",
			args: args{store: pS, pID: id.NewProjectID("user-2", "p0001")},
			want: want{p: nil, ok: false},
		},
		{
			name: "Unknown project",
			args: args{store: pS, pID: id.NewProjectID("user-1", "cool-project")},
			want: want{p: nil, ok: false},
		},
		{
			name: "Known project",
			args: args{store: pS, pID: id.NewProjectID("user-1", "p0001")},
			want: want{p: u1Projects["p0001"], ok: true},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {

			gotP, gotOK, gotError := tc.args.store.Get(tc.args.pID)

			if gotP != tc.want.p || gotOK != tc.want.ok || gotError != nil {
				t.Errorf("Get(%v) = (%v, %v), want (%v, %v)", tc.args.pID, gotP, gotOK, tc.want.p, tc.want.ok)
			}
		})
	}
}

func TestRAMStoreAddAndGet(t *testing.T) {
	type args struct {
		store *projectsRAM
		p     project.Project
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "Basic example",
			args: args{
				store: initProjectsRAM(),
				p:     project.NewProject("Test Project", "user-1", "This is a test project", visibility.Private),
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			tc.args.store.Add(&tc.args.p)
			gotP, gotOK, _ := tc.args.store.Get(tc.args.p.ID())

			if !gotOK {
				t.Errorf("Get after Add failed because the project was not found")
			}

			if gotOK && gotP != &tc.args.p {
				t.Errorf("Get after Add failed, because the project addresses were different: got %v, want %v", gotP, &tc.args.p)
			}
		})
	}
}

func TestProjectsRAMDelete(t *testing.T) {
	type args struct {
		store *projectsRAM
		pID   id.ProjectID
	}
	type want struct {
		ok    bool
		error error
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "Unknown container",
			args: args{store: pS, pID: id.NewProjectID("user-2", "p0001")},
			want: want{ok: false, error: nil},
		},
		{
			name: "Unknown project",
			args: args{store: pS, pID: id.NewProjectID("user-1", "cool-project")},
			want: want{ok: false, error: nil},
		},
		{
			name: "Known project",
			args: args{store: pS, pID: id.NewProjectID("user-1", "p0001")},
			want: want{ok: false, error: nil},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {

			gotError := tc.args.store.Delete(tc.args.pID)

			if gotError != nil {
				t.Errorf("Delete(%v) = (%v), want (%v)",
					tc.args.pID, gotError, tc.want.error)
			}
		})
	}
}

func containsProjectsUnordered(got, want []*project.Project) bool {
outer:
	for _, w := range want {
		for _, g := range got {
			if w == g {
				continue outer
			}
		}
		return false
	}
	return true
}
