// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"fmt"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"

	"github.com/go-xorm/xorm"
)

const (
	errorNoOwner = "no owner with name %#v"
)

type ownerRow struct {
	ID   int64
	Name string
}

func ownerRowFromGroupRow(g groupRow) ownerRow {
	return ownerRow{
		ID:   g.ID,
		Name: g.Name,
	}
}

func ownerRowFromUserRow(ur userRow) ownerRow {
	return ownerRow{
		ID:   ur.ID,
		Name: ur.Name,
	}
}

type ownersSQL struct {
	e *xorm.Engine
}

func (s ownersSQL) Exists(id id.ActorID) (bool, error) {
	_, ex, err := getOwnerRowID(s.e, id)
	return ex, err
}

func insertOwnerForGroup(s xorm.Interface, gr *groupRow) error {
	or := ownerRowFromGroupRow(*gr)
	_, err := s.Table(ownerTable).Insert(&or)
	gr.ID = or.ID

	return err
}

func updateOwnerForGroup(s xorm.Interface, gr *groupRow) error {
	or := ownerRowFromGroupRow(*gr)
	aff, err := s.Table(ownerTable).ID(or.ID).Update(&or)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func insertOwnerForUser(s xorm.Interface, ur *userRow) error {
	or := ownerRowFromUserRow(*ur)
	_, err := s.Table(ownerTable).Insert(&or)
	ur.ID = or.ID

	return err
}

func updateOwnerForUser(s xorm.Interface, ur *userRow) error {
	or := ownerRowFromUserRow(*ur)
	aff, err := s.Table(ownerTable).ID(or.ID).Update(&or)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func lookupOwnerRowID(s xorm.Interface, aID id.ActorID) (int64, error) {
	id, ex, err := getOwnerRowID(s, aID)

	if err != nil {
		return 0, err
	}

	if !ex {
		return 0, fmt.Errorf(errorNoOwner, aID)
	}

	return id, nil
}

func getOwnerRowID(s xorm.Interface, aID id.ActorID) (int64, bool, error) {
	or := ownerRow{Name: aID.Actor()}
	ex, err := s.Table(ownerTable).Get(&or)

	if err != nil {
		return 0, false, err
	}

	return or.ID, ex, nil
}

func listOwnerRows(s xorm.Interface) ([]ownerRow, error) {
	var ors []ownerRow
	err := s.Table(ownerTable).Asc(nameField).Find(&ors)
	if err != nil {
		return nil, err
	}

	return ors, nil
}
