// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

// +build sqlite

package store

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
	multierror "github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

const (
	errorNoProject = "no project with name %#v"
)

type projectsSQL struct {
	e *xorm.Engine
}

type projectRow struct {
	ID           int64
	OwnerID      int64
	Name         string
	Description  string
	Visibility   int
	CreationDate time.Time
}

func projectFromRow(pr projectRow) *project.Project {
	return &project.Project{
		Name:         pr.Name,
		Description:  pr.Description,
		Visibility:   visibility.Visibility(pr.Visibility),
		CreationDate: pr.CreationDate,
	}
}

func rowFromProject(p *project.Project) projectRow {
	return projectRow{
		Name:         p.Name,
		Description:  p.Description,
		Visibility:   int(p.Visibility),
		CreationDate: p.CreationDate,
	}
}

func (psSQL projectsSQL) List(owner string) ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listProjects(s, id.NewActorID(owner))
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return ps, s.Commit()
}

func (psSQL projectsSQL) ListAll() ([]*project.Project, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	ps, err := listAllProjects(s)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return ps, s.Commit()
}

func (psSQL projectsSQL) Add(p *project.Project) error {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	err := saveProject(s, p)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (psSQL projectsSQL) Get(pID id.ProjectID) (*project.Project, bool, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, false, err
	}

	p, ex, err := getProject(s, pID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, false, err
	}

	return p, ex, s.Commit()
}

func (psSQL projectsSQL) Exists(pID id.ProjectID) (bool, error) {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return false, err
	}

	_, ex, err := getProjectRow(s, pID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return false, err
	}

	return ex, s.Commit()
}

func (psSQL projectsSQL) Delete(pID id.ProjectID) error {
	s := psSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return err
	}

	if err := deleteProject(s, pID); err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func saveProject(s xorm.Interface, p *project.Project) error {
	orID, err := lookupOwnerRowID(s, p.Owner)
	if err != nil {
		return err
	}

	npr := rowFromProject(p)
	npr.OwnerID = orID

	oprID, ex, err := getProjectRowID(s, p.ID())
	if err != nil {
		return err
	}

	if ex {
		npr.ID = oprID
		err = updateProjectRow(s, &npr)
	} else {
		err = insertProjectRow(s, &npr)
	}

	if err != nil {
		return err
	}

	err = saveSUTVariants(s, npr.ID, p.Variants)
	if err != nil {
		return err
	}

	err = saveProjectLabels(s, npr.ID, p.Labels)
	if err != nil {
		return err
	}

	err = saveProjectMembers(s, npr, p.Members)

	return err
}

func updateProjectRow(s xorm.Interface, pr *projectRow) error {
	aff, err := s.Table(projectTable).ID(pr.ID).Update(pr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func insertProjectRow(s xorm.Interface, pr *projectRow) error {
	_, err := s.Table(projectTable).Insert(pr)
	return err
}

func deleteProject(s xorm.Interface, pID id.ProjectID) error {
	pr, ex, err := getProjectRow(s, pID)
	if err != nil {
		return err
	}

	if !ex {
		return fmt.Errorf(errorNoProject, pID.Project())
	}

	_, err = s.Table(projectTable).Delete(&pr)
	return err
}

func listProjects(s xorm.Interface, aID id.ActorID) ([]*project.Project, error) {
	orID, err := lookupOwnerRowID(s, aID)
	if err != nil {
		return nil, err
	}

	prs, err := listProjectRows(s, orID)
	if err != nil {
		return nil, err
	}

	ps, err := buildProjects(s, aID, prs)

	return ps, err
}

func listAllProjects(s xorm.Interface) ([]*project.Project, error) {
	ors, err := listOwnerRows(s)
	if err != nil {
		return nil, err
	}

	var ps []*project.Project
	for _, or := range ors {
		var prs []projectRow
		prs, err = listProjectRows(s, or.ID)
		if err != nil {
			return nil, err
		}

		var p []*project.Project
		p, err = buildProjects(s, id.NewActorID(or.Name), prs)
		if err != nil {
			return nil, err
		}

		ps = append(ps, p...)
	}

	return ps, nil
}

func getProject(s xorm.Interface, pID id.ProjectID) (*project.Project, bool, error) {
	pr, ex, err := getProjectRow(s, pID)
	if err != nil || !ex {
		return nil, false, err
	}

	p, err := buildProject(s, pr)
	if err != nil {
		return nil, false, err
	}

	p.Owner = pID.ActorID

	return p, true, nil
}

func buildProjects(s xorm.Interface, aID id.ActorID, prs []projectRow) ([]*project.Project, error) {
	var ps []*project.Project
	for _, pr := range prs {
		p, err := buildProject(s, pr)
		if err != nil {
			return nil, err
		}

		p.Owner = aID
		ps = append(ps, p)
	}

	return ps, nil
}

func buildProject(s xorm.Interface, pr projectRow) (*project.Project, error) {
	var err error

	p := projectFromRow(pr)
	p.Variants, err = listSUTVariants(s, pr.ID)
	if err != nil {
		return nil, err
	}

	p.Labels, err = listProjectLabels(s, pr.ID)
	if err != nil {
		return nil, err
	}

	p.Members, err = listProjectMembers(s, pr.ID)
	if err != nil {
		return nil, err
	}

	return p, nil
}

func lookupProjectRowID(s xorm.Interface, pID id.ProjectID) (int64, error) {
	prID, ex, err := getProjectRowID(s, pID)

	if err != nil {
		return 0, err
	}

	if !ex {
		return 0, fmt.Errorf(errorNoProject, pID.Project())
	}

	return prID, nil
}

func getProjectRowID(s xorm.Interface, pID id.ProjectID) (int64, bool, error) {
	pr, ex, err := getProjectRow(s, pID)
	return pr.ID, ex, err
}

func listProjectRows(s xorm.Interface, orID int64) ([]projectRow, error) {
	var prs []projectRow
	err := s.Table(projectTable).Find(&prs, &projectRow{OwnerID: orID})
	if err != nil {
		return nil, err
	}

	return prs, nil
}

func getProjectRow(s xorm.Interface, pID id.ProjectID) (projectRow, bool, error) {
	oID, err := lookupOwnerRowID(s, pID.ActorID)
	if err != nil {
		return projectRow{}, false, err
	}

	pr := projectRow{OwnerID: oID, Name: pID.Project()}
	ex, err := s.Table(projectTable).Get(&pr)
	if err != nil || !ex {
		return projectRow{}, false, err
	}

	return pr, true, nil
}
