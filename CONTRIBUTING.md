# Workflow

In the following guide placeholders in code blocks will be enclosed by angle brackets `<>`. These should be replaced with the respective value.

## Set up the project

1.  Install Git on your system
1.  Configure your Git username and email with
    
    ```bash
    git config user.name "<your username>"
    git config user.email "<your email>"
    ```

1.  Download and install Go as detailed [here](https://golang.org/doc/install).
1.  Clone the repository into `$GOPATH/src/gitlab.com/stp-team/systemtestportal-webapp/`. In git bash this would be done with

    ```bash
    cd $GOPATH
    mkdir -p src/gitlab.com/stp-team/
    cd src/gitlab.com/stp-team
    git clone git@gitlab.com:stp-team/systemtestportal-webapp systemtestportal-webapp
    cd /systemtestportal-webapp
    ```

## Working on the project

1.  Choose an issue and assign yourself. If there are people already assigned to the issue, please talk to them before adding yourself to the assignees. In this case you might also want to skip the next step.

    ![issue_page_assign](/uploads/f883ce2c1313bc517d01e7bf44840479/issue_page_assign.png)

1.  Next create a branch and merge request for the issue
    
    ![issue_page_merge_request](/uploads/0f57fc088868367867e6299a1138a53d/issue_page_merge_request.png)

1.  Update your local project and switch to the new branch. Again in git bash this would be done by

    ```bash
    # Assuming you are in the project directory
    git pull --ff-only
    git checkout <branch>
    ```

1.  Make your changes and use `git add` and `git commit` to track them

1.  Integrate your local changes with the upstream changes and publish the result

    ```bash
    git pull --rebase
    git push
    ```

1.  When you think the branch is ready for review, remove the `WIP` tag from the merge request title and assign someone to review your changes. This can be done with the slash commands `/wip` and `/assign <user>` in a comment.
1.  The assigned person will review your changes and come back to you if there are problems or they have questions. If they think your changes are okay, they will accept the merge request and merge the branch into `master`.
1.  After that you can remove your local feature branch with
    
    ```bash
    git branch -d <branch>
    ```
    
## Guidelines

### Naming conventions

Please have a look at the [naming conventions](https://gitlab.com/stp-team/systemtestportal-webapp/wikis/contribute/Naming-conventions) in the wiki.