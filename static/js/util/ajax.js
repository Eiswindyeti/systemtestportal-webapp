/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

/* Ajax requests */

// Ajax Request to a target with params. It changes the browser history to the target. Returns a tab fragment.
//target should be an already URL-encoded string
function ajaxRequestFragment (event, target, params, requestType) {
    ajaxRequestFragmentWithHistory (event, target, params, requestType, target);
}

// Ajax Request to a target with params. It changes the browser history to the historyText parameter.
// Returns a tab fragment.
//target should be an already URL-encoded string
function ajaxRequestFragmentWithHistory (event, target, params, requestType, historyText) {
    event.preventDefault();
    target = encode(target);
    target = updateQueryStringParameter(target, "fragment", true);

    // hides the printer icon, as many fragements don't have a print functionality (yet)
    $("#printerIcon").addClass("d-none");

    // sends the ajax request
    var posting = $.ajax({
        url: target,
        type: requestType,
        data: params
    });

    // swap old with new fragment
    posting.done(function(response) {
        window.history.pushState('data', '', historyText);
        $('#tabarea').empty().append(response);
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        return true;
    }).fail(function (response) {
        $( "#modalPlaceholder" ).empty().append(response.responseText);
        $('#errorModal').modal('show');
    });
}

// prepares target
function encode(target){
    var index = target.lastIndexOf('?');
    var params = "";
    if (index !== -1) {
        target = target.substr(0, index);
        params = target.substr(index+1, target.length);
    }
    var uri = target.split("/");
    for (i = 0; i < uri.length; i++){
        uri[i] = encodeURIComponent(uri[i]);
    }
    return uri.join("/") + params;
}


// prepares the URL with the parameters
function updateQueryStringParameter(uri, key, value) {
    var regex = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
    if (uri.match(regex)) {
        return uri.replace(regex, '$1' + key + "=" + value + '$2');
    } else {
        var hash =  '';
        var index = uri.lastIndexOf('#');
        if( index !== -1 ){
            hash = uri.slice(index, uri.length);
            uri = uri.slice(0, index);
        }
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        return uri + separator + key + "=" + value + hash;
    }
}

// Ajax Request sends data to the server.
function ajaxSendDataToServer(event, target, paramData) {
    event.preventDefault();
    var posting = $.ajax({
        url: target,
        type: "PUT",
        data: paramData
    });
    posting.done(function (response) {
        return true;
    }).fail(function (response) {
        return true;
    });

}

// Requests a tab fragment
function requestTab(event, tab, image, segmentNumber) {
    // stop form from submitting normally
    event.preventDefault();

    // prepare target URL
    var url = currentURL();
    var requestURL = url.takeFirstSegments(segmentNumber).appendSegment(tab).toString() + "/";
    var posting = $.get(requestURL + "?fragment=true");

    // get result
    posting.done(function (response) {
        if (url.segments[segmentNumber] !== tab || url.segments[segmentNumber + 1] !== "") {
            history.pushState('data', '', requestURL);
        }
        $("#modalPlaceholder").empty();
        $('#tabarea').empty().append(response);
        if (image === true) {
            $(".nav-link.active").removeClass("active");
            $("#tabButtonTestCases").addClass("active");
            $("#menuButtonTestCases").addClass("active");
        } else {
            updateTabs(tab);
        }
    }).fail(function (response) {
        $("#modalPlaceholder").empty().append(response.responseText);
        $('#errorModal').modal('show');
    });
}