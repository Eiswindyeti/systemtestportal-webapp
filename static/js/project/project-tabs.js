/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");
$.getScript("/static/js/util/ajax.js");

/* attach a handler to the tab buttons */
function initializeProjectTabClickListener() {
    // DASHBOARD
    //$("#tabButtonDashboard").click(function(event) {requestTab(event,"dashboard")});
    // ACTIVITY
    // $("#tabButtonActivity").click(function(event) {requestTab(event,"activity")});
    // TESTCASES
    $("#tabButtonTestCases").click(function (event) {
        requestTab(event, "testcases", false, 3)
    });
    $("#menuButtonTestCases").click(function (event) {
        requestTab(event, "testcases", false, 3)
    });
    // TESTSEQUENCES
    $("#tabButtonTestSequences").click(function (event) {
        requestTab(event, "testsequences", false, 3)
    });
    $("#menuButtonTestSequences").click(function (event) {
        requestTab(event, "testsequences", false, 3)
    });
    // PROTOCOLS
    $("#tabButtonProtocols").click(function (event) {
        requestTab(event, "protocols", false, 3)
    });
    $("#menuButtonProtocols").click(function (event) {
        requestTab(event, "protocols", false, 3)
    });
    // MEMBERS
     $("#tabButtonMembers").click(function(event) {
         requestTab(event,"members", false, 3)
     });
    // SETTINGS
    $("#tabButtonSettings").click(function (event) {
        if ($('#tabButtonSettings').hasClass("disabled")) {
            return false;
        } else {
            requestTab(event, "settings", false, 3);
        }
    });
    // HEADER redirects to dashboard (temporary to the testcases)
    $("#projectImage").click(function (event) {
        requestTab(event, "testcases", true, 3)
    });

}

// updates tab highlight on tab swap
function updateTabs(tab) {
    $(".tab-collapse-menu").children().removeClass("active");
    if(tab === "testcases") {
        $("#menuButtonTestCases").addClass("active");
    } else if (tab === "testsequences") {
        $("#menuButtonTestSequences").addClass("active");
    } else if (tab === "protocols") {
        $("#menuButtonProtocols").addClass("active");
    } else if (tab === "members") {
        $("#menuButtonMembers").addClass("active");
    }
}

// initializes tab highlighting and timestamps on pageload
$(document).ready(function () {
    var url = currentURL();
    if (url.segments[3] === "testcases") {
        $("#tabButtonTestCases").addClass("active");
        $("#menuButtonTestCases").addClass("active");
    } else if (url.segments[3]==="testsequences") {
        $("#tabButtonTestSequences").addClass("active");
        $("#menuButtonTestSequences").addClass("active");
    } else if (url.segments[3] === "protocols") {
        $("#tabButtonProtocols").addClass("active");
        $("#menuButtonProtocols").addClass("active");
    } else if (url.segments[3] === "members") {
        $("#tabButtonMembers").addClass("active");
        $("#menuButtonMembers").addClass("active");
    }


    var timeAgo = $(".timeago");
    var timeString = timeAgo.text();
    timeString = timeString.replace(" ", 'T');
    timeString = timeString.replace(" ", '');
    timeString = timeString.replace(" CET", '');
    timeAgo.text(timeString);
    timeAgo.attr("datetime", timeString);
    jQuery("time.timeago").timeago();

});