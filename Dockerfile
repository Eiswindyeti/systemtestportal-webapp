FROM alpine:latest

ADD /stp.tar.gz /tmp/

RUN cp /tmp/stp /usr/sbin/ \
    && mkdir /usr/share/stp/ \
    && mkdir /var/stp/ \
    && if [[  -f "./config.ini" ]] ; then mkdir /etc/stp/ && cp "./config.ini" /etc/stp/ ; fi \
    && cp -r /tmp/templates/ /usr/share/stp/templates \
    && cp -r /tmp/static/ /usr/share/stp/static \
    && rm -r /tmp/*

EXPOSE 8080

CMD ["stp", "--basepath=/usr/share/stp/", "--data-dir=/var/stp/"]